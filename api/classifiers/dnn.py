import shutil
import os
import tensorflow as tf
import numpy as np
from sklearn.model_selection import StratifiedKFold, KFold

from api.commons.dataset_ops import Dataset, Datasets


class DeepNeuralNet:

    def __init__(self, layer_size: int, neuron_size: list, model_dir,
                 output_class=2):
        self.output_class = output_class

        self.hidden_layer = layer_size

        if len(neuron_size) != self.hidden_layer:
            raise ValueError('The number of neuron_size must equal with layer_size, expected {}'
                             .format(self.hidden_layer), 'but get {}'.format(len(neuron_size)))
        self.neurons = neuron_size

        self.model_dir = model_dir

        self.neural_net = None

    def _model(self, data):
        model_temp = []

        for i in range(self.hidden_layer):
            if i == 0:
                model_temp.append(
                    tf.layers.dense(data['features'], self.neurons[i], use_bias=True, activation=tf.nn.relu)
                )
            else:
                model_temp.append(
                    tf.layers.dense(model_temp[i - 1], self.neurons[i], use_bias=True, activation=tf.nn.relu)
                )

        output = tf.layers.dense(model_temp[len(model_temp) - 1], self.output_class, use_bias=True)

        return output

    def _model_fn(self, features, labels, mode, params):

        predictions = self._model(features)

        class_predictions = tf.argmax(predictions, axis=1)

        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(
                mode=mode,
                predictions={"cancer": class_predictions})
        loss = tf.losses.sigmoid_cross_entropy(tf.one_hot(labels, 2), predictions)
        optimizer = tf.train.AdamOptimizer(
            learning_rate=params['learning_rate']
        )

        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step()
        )

        eval_metric_ops = {
            'accuracy': tf.metrics.accuracy(
                labels=labels,
                predictions=class_predictions
            ),
            'rsme': tf.metrics.root_mean_squared_error(
                labels=labels,
                predictions=tf.cast(class_predictions, dtype=tf.int32)
            ),
            'sensitivity': tf.metrics.recall(
                labels=tf.cast(labels, tf.float32),
                predictions=class_predictions
            ),
            'precision': tf.metrics.precision(
                labels=tf.cast(labels, tf.float32),
                predictions=class_predictions
            )
        }

        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            train_op=train_op,
            eval_metric_ops=eval_metric_ops
        )

    def train(self, data, epochs, params):
        self.neural_net = tf.estimator.Estimator(model_fn=self._model_fn, params=params, model_dir=self.model_dir)

        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'features': data.train.features},
            y=data.train.labels,
            num_epochs=epochs,
            shuffle=True
        )

        self.neural_net.train(input_fn=train_input_fn, steps=1000)

    def test(self, data, params):

        self.neural_net = tf.estimator.Estimator(model_fn=self._model_fn, params=params, model_dir=self.model_dir)

        test_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'features': data.test.features},
            y=data.test.labels,
            num_epochs=1,
            shuffle=False
        )

        eval_metrics = self.neural_net.evaluate(input_fn=test_input_fn)

        return eval_metrics

    def predict(self, data: np.array):
        predict_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'features': data},
            num_epochs=1,
            shuffle=False
        )

        prediction = self.neural_net.predict(input_fn=predict_input_fn)

        prediction_arr = []
        for i, p in enumerate(prediction):
            if p['cancer'] == 1:
                prediction_arr.append('Breast')
            else:
                prediction_arr.append('Normal')
        return prediction_arr

    def k_fold(self, k, dataset, epochs, params):
        evals = []
        kf = StratifiedKFold(n_splits=k, shuffle=True)
        for train_index, valid_index in kf.split(dataset.features, dataset.labels):
            train_features, valid_features = dataset.features[train_index], dataset.features[valid_index]
            train_labels, valid_labels = dataset.labels[train_index], dataset.labels[valid_index]
            training_data = Dataset(features=train_features, labels=train_labels)
            validation_data = Dataset(features=valid_features, labels=valid_labels)
            data = Datasets(train=training_data, test=validation_data)
            self.train(data, epochs, params)
            eval_metrics = self.test(data, params)
            eval_metrics.pop('loss')
            eval_metrics.pop('global_step')
            for key in eval_metrics:
                eval_metrics[key] = float(eval_metrics[key])
            evals.append(eval_metrics)

        return evals
