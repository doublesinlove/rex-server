import marshal

import pickle

from api.classifiers.dnn import DeepNeuralNet
from api.commons.utils import Utils


class ClassifierOps:

    @staticmethod
    def save_params(container_id, data):
        path = Utils.get_model_dir(container_id)
        with open(path + 'model_params', 'wb') as e_file:
            marshal.dump(data, e_file)

    @staticmethod
    def create_model(hidden_layer, neurons, model_dir, class_num, container_id):
        path = Utils.get_model_dir(container_id)
        network = DeepNeuralNet(hidden_layer, neurons, model_dir, class_num)
        with open(path + 'model_net.o', 'wb') as e_file:
            pickle.dump(network, e_file)
        return network

    @staticmethod
    def run_model_with_k_fold(network: DeepNeuralNet, k, dataset, epoch, model_params, id):
        path = Utils.get_model_dir(id)
        eval_metrics = network.k_fold(k, dataset, epoch, model_params)
        with open(path + 'model_mar.o', 'wb') as e_file:
            pickle.dump(network, e_file)
        return eval_metrics

    @staticmethod
    def predict_data(network: DeepNeuralNet, data):
        predictions = network.predict(data)
        return predictions

    @staticmethod
    def save_eval_metrics(container_id, eval_metrics):
        path = Utils.get_model_dir(container_id)
        with open(path + 'eval_metrics', 'wb') as e_file:
            marshal.dump(eval_metrics, e_file)

    @staticmethod
    def load_model(container_id):
        path = Utils.get_model_dir(container_id)
        with open(path + 'model_params', 'rb') as p_file:
            params = marshal.load(p_file)
        with open(path + 'eval_metrics', 'rb') as e_file:
            eval_metrics = marshal.load(e_file)
        return params, eval_metrics

    @staticmethod
    def load_trainer_model(container_id):
        path = Utils.get_model_dir(container_id)
        with open(path + 'model_mar.o', 'rb') as e_file:
            network = pickle.load(e_file)
        return network

    @staticmethod
    def check_input(data_input, headers):
        for data, head in zip(list(list(data_input.keys())), headers):
            if data != head:
                return False

        return True
