from flask import Flask, session, request
from flask_cors import CORS
from flask_restful import Api, Resource

from api.commons.config import Config
from api.resources.accounts import Accounts
from api.resources.auths import Auths
from api.resources.classify import Classify
from api.resources.containers import Containers
from api.resources.datasets import Datasets
from api.resources.models import Models

app = Flask(__name__)
app.secret_key = Config.SECRET_KEY
CORS(app, origins=['http://127.0.0.1:8080', 'http://localhost:8080'], supports_credentials=True)


"""
    API
    Version         : 0
    Resources       : Accounts, Auth, Classification
                      Containers, Datasets, Errors, Models
    Endpoint rules  :

"""

api = Api(app=app)

BASE_ROUTE = '/api/v0/'


class ChechSession(Resource):

    def get(self):
        return session.get('username')


api.add_resource(ChechSession, BASE_ROUTE + 'session')
api.add_resource(Accounts, BASE_ROUTE + 'accounts')
api.add_resource(Auths, BASE_ROUTE + 'auths')
api.add_resource(Classify, BASE_ROUTE + 'classify/<string:container_code>')
api.add_resource(Containers, BASE_ROUTE + 'containers/<string:container_code>', BASE_ROUTE + 'containers')
api.add_resource(Datasets, BASE_ROUTE + 'datasets/<string:container_code>')
api.add_resource(Models, BASE_ROUTE + 'models/<string:container_code>')
