import json

from flask import request, session
from flask_restful import Resource

from api.commons.db_access import DataAccess
from api.commons.utils import Utils


class Containers(Resource):
    access = DataAccess()

    def get(self, container_code):
        if container_code == 'all':
            username = session['username']
            res = self.access.get_containers(username)
            containers = [x.__dict__ for x in res]
            return json.dumps(containers, indent=4, separators=(',', ': '))
        else:
            res = self.access.get_container(container_code)
            container = res.__dict__
            return json.dumps(container, indent=4, separators=(',', ': '))

    def post(self):
        try:
            data = request.json['data']
            try:
                container_name = data['container_name']
            except KeyError:
                return {'error': 'Property container_name is not defined'}, 400
            try:
                container_desc = data['container_desc']
            except KeyError:
                return {'error': 'Property container_desc is not defined'}, 400
            try:
                container_code = data['container_code']
            except KeyError:
                return {'error': 'Property container_code is not defined'}, 400
            status = self.access.create_container(container_name, container_code, container_desc)
            if 'container_id' in status:
                container_id = status.pop('container_id')
                Utils.create_container_dirs(str(container_id))
                username = session['username']
                result = self.access.get_containers(username)
                containers = [x.__dict__ for x in result]
                status['containers'] = containers
                return json.dumps(status, indent=4, separators=(',', ': ')), 201
            else:
                return json.dumps(status, indent=4, separators=(',', ': ')), 400

        except KeyError:
            return {'error': 'Bad request format'}, 400

    def delete(self, container_code):
        container = self.access.get_container(container_code)
        if container is not None:
            status_deleted = self.access.delete_container(container_code)

            if 'deleted' in status_deleted:
                status_deleted.pop('deleted')
                Utils.delete_container_dirs(container.container_id)
                return json.dumps(status_deleted, indent=4, separators=(',', ': ')), 200
            else:
                return json.dumps(status_deleted, indent=4, separators=(',', ': ')), 200
        else:
            return json.dumps({'error': 'Container not found'}, indent=4, separators=(',', ': ')), 400
