import json
from flask import request
from flask_restful import Resource

from api.commons.db_access import DataAccess
from api.commons.utils import Utils


class Accounts(Resource):

    # Initiating Database Access
    _access = DataAccess()

    def post(self):
        """
        HTTP Method POST Handler
        Creating new User
        :return:
        """

        # JSON schema
        # {'data' : {
        #     'name'    : 'string',
        #     'username': 'string',
        #     'password': 'string'
        #   }
        # }

        # Validating from here
        try:
            data = request.json['data']
            try:
                name = data['name']
            except KeyError:
                return json.dumps({'error': 'Property name is not defined'}), 400
            try:
                username = data['username']
            except KeyError:
                return json.dumps({'error': 'Property username is not defined'}), 400
            try:
                password = data['password']
            except KeyError:
                return json.dumps({'error': 'Property password is not defined'}), 400
            # to here

            # Creating user and storing it in database
            status_created = self._access.create_user(name=name, username=username, password=password)

            # If user_id exist, then it has been created, else failed
            if 'user_id' in status_created:
                user_id = status_created.pop('user_id')
                # Create user directory with user_id as name
                Utils.create_user_dir(user_id)
                # Get necessary user's data
                user = self._access.get_user(username=username).__dict__
                user.pop('password')
                # Write JSON identifier in user directory
                Utils.write_user_json(user, user_id)
                # Send success message to user
                return json.dumps(status_created, indent=4, separators=(',', ': ')), 201
            else:
                # Failed to create user, with error in status_created
                return json.dumps(status_created, indent=4, separators=(',', ': ')), 400

        except KeyError:
            return json.dumps({'error': 'Bad request format'}), 400

    def delete(self):
        """
        HTTP Method DELETE Handler
        Deleting existing User
        :return:
        """

        # JSON schema
        # {'data' : {
        #     'username': 'string',
        #   }
        # }

        # Validating
        try:
            data = request.json['data']
            try:
                username = data['username']
            except KeyError:
                return {'error': 'Property username is not defined'}, 400
            # Getting user from database
            user = self._access.get_user(username)

            # If user it not none, then it exists, else not found.
            if user is not None:
                # Delete user from database
                status_deleted = self._access.delete_user(username=username)

                #
                if 'deleted' in status_deleted:
                    status_deleted.pop('deleted')
                    # Delete user directory and its content
                    Utils.delete_user_dir(user.user_id)
                    # Send success message to user
                    return json.dumps(status_deleted, indent=4, separators=(',', ': ')), 200
                else:
                    # Send error message to user
                    return json.dumps(status_deleted, indent=4, separators=(',', ': ')), 400
            else:
                # Send error message to user
                return json.dumps({'error': 'User not found'}), 400
        except KeyError:
            # Send error message to user
            return json.dumps({'error': 'Bad request format'}), 400
