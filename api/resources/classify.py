from flask import request, json
from flask_restful import Resource
import numpy as np
from api.classifiers.classifier_ops import ClassifierOps
from api.commons.dataset_ops import DatasetOps
from api.commons.db_access import DataAccess


class Classify(Resource):

    _access = DataAccess()

    def get(self, container_code):
	
        container = self._access.get_container(container_code)
        if container is not None:
            container_id = container.container_id
            columns = DatasetOps.get_data_columns(container_id)
            if 'column' in columns:
                return json.dumps(columns)
            else:
                return json.dumps(columns)
        else:
            return json.dumps({'error': 'Container not found'}), 400

    def post(self, container_code):
        container = self._access.get_container(container_code)
        print(container_code)
        if container is not None:
            container_id = container.container_id
            data = request.json['data']
            c = DatasetOps.get_data_columns(container_id)['header']
            if ClassifierOps.check_input(data, c):
                data_input = np.array([list(data.values())], np.float32)
                print(data_input.size)
                network = ClassifierOps.load_trainer_model(container_id)
                preds = network.predict(data_input)
                return preds
            else:
                return json.dumps({'error': 'Not match'})
        else:
            return json.dumps({'error': 'Container not found'}), 400
