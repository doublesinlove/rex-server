import json
import os
import numpy as np
from flask import request
from flask_restful import Resource
from jsonschema import validate, ValidationError

from api.classifiers.classifier_ops import ClassifierOps
from api.classifiers.dnn import DeepNeuralNet
from api.commons.dataset_ops import DatasetOps
from api.commons.db_access import DataAccess
from api.commons.utils import Utils


class Models(Resource):

    _access = DataAccess()

    schema = {
        'type': 'object',
        'properties': {
            'learningRate': {'type': 'number'},
            'epoch': {'type': 'number'},
            'k': {'type': 'number'},
            'hiddenLayer': {'type': 'number'},
            'neurons': {'type': 'array'}
        }
    }

    def get(self, container_code):
        container = self._access.get_container(container_code)
        if container is not None:
            container_id = container.container_id
            try:
                params, eval_metrics = ClassifierOps.load_model(container_id)
            except FileNotFoundError:
                return json.dumps({'error': 'Model not found'},  indent=4, separators=(',', ': ')), 200
            return json.dumps({'status': 'Model successfully loaded',
                               'params': params, 'eval': eval_metrics}, indent=4, separators=(',', ': '))
        else:
            return {'error': 'Container not found'}, 400

    def post(self, container_code):
        container = self._access.get_container(container_code)
        if container is not None:
            container_id = container.container_id
            model_dir = Utils.get_model_dir(container_id)
            try:
                data = request.json['data']
                try:
                    validate(data, self.schema)
                    learning_rate = data['learningRate']
                    epoch = data['epoch']
                    k = data['k']
                    hidden_layer = data['hiddenLayer']
                    neurons = data['neurons']
                    dataset, class_num = DatasetOps.get_dataset(container_id,
                                                                features_dtype=np.float32,
                                                                target_dtype=np.int32)
                    print(dataset.features[0])
                    if os.listdir(model_dir):
                        Utils.delete_model_dirs(container_id)
                    network = ClassifierOps.create_model(hidden_layer, neurons, model_dir, class_num, container_id)
                    model_params = {'learning_rate': learning_rate}
                    eval_metrics = ClassifierOps.run_model_with_k_fold(network, k, dataset, epoch, model_params,
                                                                       container_id)
                    ClassifierOps.save_eval_metrics(container_id, eval_metrics)
                    ClassifierOps.save_params(container_id, data)
                    params, e = ClassifierOps.load_model(container_id)
                    return json.dumps({'status': 'Model has successfully been created',
                                       'eval': eval_metrics, 'params': params}, indent=4, separators=(',', ': '))
                except ValidationError:
                    return json.dumps({'error': 'Bad request format'}), 400
            except KeyError:
                return json.dumps({'error': 'Bad request format'}), 400
        else:
            return json.dumps({'error': 'Container not found'}), 400

    def delete(self, container_code):
        container = self._access.get_container(container_code)
        if container is not None:
            container_id = container.container_id
            Utils.delete_model_dirs(container_id)
            return json.dumps({'status': 'Model successfully deleted'})
        else:
            return json.dumps({'error': 'Container not found'}), 400
