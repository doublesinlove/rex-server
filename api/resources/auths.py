import json
from flask import request, session
from flask_restful import Resource

from api.commons.db_access import DataAccess


class Auths(Resource):

    # Initiating Database Access
    _access = DataAccess()

    def post(self):
        """
        HTTP Method POST Handler
        Logging to server and client
        :return:
        """

        # JSON schema
        # {'data' : {
        #     'username': 'string',
        #     'password': 'string'
        #   }
        # }

        # Validating from here
        try:
            data = request.json['data']
            try:
                username = data['username']
            except KeyError:
                return {'error': 'Property username is not defined'}, 400
            try:
                password = data['password']
            except KeyError:
                return {'error': 'Property password is not defined'}, 400
            # to here

            # Getting user from database
            user = self._access.get_user(username)

            # If user it not none, then it exists, else not found.
            if user is not None:
                # If credentials is true, then set a session in server.
                if username == user.username and password == user.password:
                    # Creating session
                    session['user_id'] = user.user_id
                    session['username'] = user.username
                    username = session['username']
                    result = self._access.get_containers(username)
                    containers = [x.__dict__ for x in result]
                    # Server response
                    res = {
                        'name': user.name,
                        'username': user.username,
                        'containers': containers
                    }
                    # Send response to user
                    # TODO SET COOKIES
                    return json.dumps(res, indent=4, separators=(',', ': ')), 200, {'Set-Cookie': 'name=iqi;  HttpOnly;'}
                else:
                    # Send error message to user
                    return json.dumps({'error': 'Invalid login'}, indent=4, separators=(',', ': ')), 400
            else:
                return json.dumps({'error': 'User not found'}, indent=4, separators=(',', ': ')), 400
        except KeyError:
            # Send error message to user
            return json.dumps({'error': 'Bad request format'}), 400

    def delete(self):
        """
        HTTP Method DELETE Handler
        Logging out from server and client
        :return:
        """
        try:
            data = request.json['data']
            try:
                username = data['username']
                print(session.get('username'))
            except KeyError:
                return json.dumps({'error': 'Property username is not defined'}, indent=4, separators=(',', ': ')), 400
            if session['username'] == username:
                session.pop('user_id')
                session.pop('username')
                status = {
                    'status': 'Successfully logged out'
                }
                return json.dumps(status, indent=4, separators=(',', ': '))
            else:
                return json.dumps({'error': 'User not found'}, indent=4, separators=(',', ': '))
        except KeyError:

            return json.dumps({'error': 'Bad request format'}, indent=4, separators=(',', ': '))
