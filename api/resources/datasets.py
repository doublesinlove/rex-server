import json
import os
from flask import request
from flask_restful import Resource
from werkzeug.utils import secure_filename

from api.commons.dataset_ops import DatasetOps
from api.commons.db_access import DataAccess
from api.commons.utils import Utils


class Datasets(Resource):
    ALLOWED_EXT = {'csv'}
    _access = DataAccess()

    def _allowed_file(self, filename: str):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in self.ALLOWED_EXT

    def get(self, container_code):
        container_id = self._access.get_container(container_code).container_id
        data = DatasetOps.get_data_source(container_id)
        if 'error' not in data:
            return json.dumps({'status': 'success', 'header': data.get('header'), 'dataset': data.get('data')},
                              indent=4, separators=(',', ': ')), 200
        else:
            return json.dumps(data, indent=4, separators=(',', ': ')), 200

    def post(self, container_code):
        file = request.files['file']
        if file and self._allowed_file(file.filename):
            filename = secure_filename(file.filename)
            container_id = self._access.get_container(container_code).container_id
            file.save(os.path.join(Utils.get_dataset_dir(container_id), 'dataset.csv'))
            header, data = DatasetOps.read_and_write_file(container_id, container_code)

            return json.dumps({'status': 'success', 'header': header, 'dataset': data},
                              indent=4, separators=(',', ': ')), 200
        else:
            return {'path': 'ad'}

    def delete(self, container_code):
        container_id = self._access.get_container(container_code).container_id
        status = DatasetOps.delete_dataset(container_id, container_code)
        return json.dumps(status, indent=4, separators=(',', ': '))
