import os
import json
import shutil
from flask import session


class Utils:
    @staticmethod
    def get_session_dir():
        if session['user_id']:
            path = './api/assets/' + session['user_id'] + '/'
            return path
        return None

    @staticmethod
    def get_user_dir(user_id: str):
        path = './api/assets/' + user_id + '/'
        return path

    @staticmethod
    def get_container_dir(container_id: str):
        if session['user_id']:
            path = Utils.get_session_dir() + container_id + '/'
            return path
        return None

    @staticmethod
    def get_dataset_dir(container_id: str):
        if session['user_id']:
            path = Utils.get_container_dir(container_id) + 'dataset/'
            return path
        return None

    @staticmethod
    def get_model_dir(container_id: str):
        if session['user_id']:
            path = Utils.get_container_dir(container_id) + 'model/'
            return path
        return None

    @staticmethod
    def create_user_dir(user_id: str):
        path = './api/assets/' + user_id + '/'
        if not os.path.isdir(path):
            os.makedirs(path)

    @staticmethod
    def create_container_dirs(container_id: str):
        path = Utils.get_session_dir() + container_id + '/'
        model_dir = path + '/model/'
        dataset_dir = path + '/dataset/'
        if not os.path.isdir(model_dir):
            os.makedirs(model_dir)
            os.makedirs(dataset_dir)

    @staticmethod
    def delete_user_dir(user_id: str):
        path = './api/assets/' + user_id + '/'
        if os.path.isdir(path):
            shutil.rmtree(path)

    @staticmethod
    def delete_container_dirs(container_id: str):
        path = Utils.get_session_dir() + container_id + '/'
        if os.path.isdir(path):
            shutil.rmtree(path)

    @staticmethod
    def delete_model_dirs(container_id: str):
        path = Utils.get_model_dir(container_id)
        for the_file in os.listdir(path):
            file_path = os.path.join(path, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)
        if os.path.isdir(path):
            shutil.rmtree(path)
            os.makedirs(path)

    @staticmethod
    def write_user_json(user, user_id):
        with open(Utils.get_user_dir(user_id) + 'user.json', 'w') as f:
            json.dump(user, f, indent=4, separators=(',', ': '))
