import csv
import collections

import os
from copy import deepcopy
import pickle
import numpy as np
import pandas as pd

from api.commons.db_access import DataAccess
from api.commons.utils import Utils

Dataset = collections.namedtuple('Dataset', ['features', 'labels'])
Datasets = collections.namedtuple('Datasets', ['train', 'test'])


class DatasetOps:

    @staticmethod
    def unison_shuffled_copies(a, b):
        assert len(a) == len(b)
        p = np.random.permutation(len(a))
        return a[p], b[p]

    @staticmethod
    def read_and_write_file(container_id, container_code):
        """
        Read dataset file and write
        TODO: Save it to database
        :param container_id:
        """
        # Get directory path
        path = Utils.get_dataset_dir(container_id)
        # Opening file to buffer
        with open(path + 'dataset.csv', 'r') as file:
            # Read with as Pandas DataFrame
            dataset = pd.read_csv(file)
            # Get columns name / header as list
            data_header = list(dataset)
            # Whole dataset as list
            data_source = dataset.as_matrix().tolist()

            # creating data for db
            db_dataset = []
            for data in data_source:
                row = dict(zip(data_header, data))
                db_dataset.append(row)

            # Separating label column
            labels_column = dataset.pop('label')
            # Get all labels in column and sort it descending (A-Z)
            labels = np.sort(labels_column.unique())
            # Enumerate sorted labels
            labels_dict = dict((y, x) for x, y in enumerate(labels))
            # Mapping string labels to enumerated labels
            labels_column = labels_column.map(labels_dict)
            # Normalise dataset features with Features Scaling
            dataset_norm = (dataset - dataset.min()) / (dataset.max() - dataset.min())
            # Convert all DataFrame to ndarray numpy
            features_matrix = dataset_norm.as_matrix()
            labels_matrix = labels_column.as_matrix()
            # Randomize dataset features and labels
            sf_features, sf_labels = DatasetOps.unison_shuffled_copies(features_matrix, labels_matrix)
            # Add to Dataset tuple
            d = Dataset(features=sf_features, labels=sf_labels)

        # Saving data to binary files
        with open(path + 'dataset_tf', 'wb') as file:
            pickle.dump(d, file)
        with open(path + 'labels_dict', 'wb') as file:
            pickle.dump(labels_dict, file)
        with open(path + 'data_header', 'wb') as file:
            pickle.dump(data_header, file)
        with open(path + 'data_source', 'wb') as file:
            pickle.dump(data_source, file)

        # saving data to database
        _access = DataAccess()
        _access.save_dataset(container_code, db_dataset)

        return data_header, data_source

    @staticmethod
    def get_data_source(container_id):
        path = Utils.get_dataset_dir(container_id)
        try:
            with open(path + 'data_header', 'rb') as file:
                data_header = pickle.load(file)
            with open(path + 'data_source', 'rb') as file:
                data_source = pickle.load(file)
            return {'header': data_header, 'data': data_source}
        except FileNotFoundError:
            return {'error': 'Dataset not found'}

    @staticmethod
    def get_data_columns(container_id):
        path = Utils.get_dataset_dir(container_id)
        try:
            with open(path + 'data_header', 'rb') as file:
                data_header = pickle.load(file)
            return {'header': data_header}
        except FileNotFoundError:
            return {'error': 'Dataset not found'}

    @staticmethod
    def delete_dataset(container_id, container_code):
        path = Utils.get_dataset_dir(container_id)
        for the_file in os.listdir(path):
            file_path = os.path.join(path, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)
        _access = DataAccess()
        _access.delete_dataset(container_code)
        return {'status': 'Delete successful'}

    @staticmethod
    def get_dataset(container_id, features_dtype, target_dtype):
        path = Utils.get_dataset_dir(container_id)
        with open(path + 'dataset_tf', 'rb') as file:
            dataset = pickle.load(file)
            features = dataset.features.astype(features_dtype)
            labels = dataset.labels.astype(target_dtype)
            dt = Dataset(features=features, labels=labels)
        with open(path + 'labels_dict', 'rb') as file:
            labels_dict = pickle.load(file)
            class_num = len(labels_dict)
        return dt, class_num
