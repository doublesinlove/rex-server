from bson import DBRef, ObjectId
from flask import session
from pymongo import MongoClient, errors

from api.commons.object_models import UserObject, ContainerObject


class DataAccess:

    def __init__(self,
                 db_name: str = 'REXifier',
                 server: str = 'localhost',
                 port: int = 27017):
        self._client = MongoClient(server, port)
        self._db = self._client[db_name]
        self._user_c = self._db['User']
        self._container_c = self._db['Container']
        self._dataset_c = self._db['Dataset']

    """
        User Operations
        1. [GET]    Get User
        2. [POST]   Create User
        3. [DELETE] Delete User
    """
    def get_user(self, username: str):
        result = self._user_c.find_one({'username': username})
        if result is not None:
            user = UserObject(name=result.get('name'),
                              username=result.get('username'),
                              password=result.get('password'),
                              user_id=str(result.get('_id')))

            return user
        else:
            return result

    def create_user(self, name: str, username: str, password: str):
        self._user_c.create_index('username', unique=True)

        try:
            user_id = self._user_c.insert_one({'name': name, 'username': username, 'password': password}, 'container_').inserted_id
            status = {
                'status': 'User has been created',
                'user_id': str(user_id)
            }
            return status
        except errors.DuplicateKeyError:
            status = {
                'status': 'User can not be created'
            }
            return status

    def delete_user(self, username: str):
        count = self._user_c.delete_one({'username': username}).deleted_count
        if count == 0:
            return {'status': 'Delete unsuccessful'}
        else:
            return {'status': 'Delete successful', 'deleted': True}


    """
        Containers Operations
        1. [POST]   Create a container
        2. [GET]    Get a single container
        3. [DELETE] Delete a single container
        4. [UPDATE] Update a single container
    """
    def create_container(self, container_name, container_code, container_desc):
        self._container_c.create_index('container_code', unique=True)
        try:
            username = self.get_user(session['username']).username
            container_id = self._container_c.insert_one(
                {'user_id': DBRef(collection='User', id=username), 'container_name': container_name,
                 'container_code': container_code, 'container_desc': container_desc},
            ).inserted_id
            status = {
                'status': 'Container has been created',
                'container_id': container_id
            }
            return status
        except errors.DuplicateKeyError:
            status = {
                'status': 'Container can not be created, duplicate name.'
            }
            return status

    def get_container(self, container_code):
        result = self._container_c.find_one({'user_id': DBRef('User', session['username']),
                                            'container_code': container_code})
        if result is not None:
            container = ContainerObject(container_id=str(result.get('_id')),
                                        container_name=result.get('container_name'),
                                        container_desc=result.get('container_desc'),
                                        container_code=result.get('container_code'))

            return container
        else:
            return result

    def get_containers(self, username: str):
        res = self._container_c.find({'user_id': DBRef('User', username)})
        containers = []
        if res is not None:
            for container in res:
                c = ContainerObject(str(container.get('_id')),
                                    container.get('container_name'),
                                    container.get('container_desc'),
                                    container.get('container_code'))
                containers.append(c)
            return containers
        else:
            return containers

    def delete_container(self, container_code):
        count = self._container_c.delete_one({'container_code': container_code}).deleted_count
        if count == 0:
            return {'error': 'Delete unsuccessful'}
        else:
            return {'status': 'Delete successful', 'deleted': True}

    """
        Dataset operations
        1. [POST]   Save a dataset to database
        2. [GET]    Get a dataset from database
        3. [DELETE] Delete a dataset
    """
    def save_dataset(self, container_code, dataset):
        self._dataset_c.create_index('container_code', unique=True)
        try:
            dataset_id = self._dataset_c.insert_one({
                'container_code': DBRef(collection='Container', id=container_code),
                'dataset': dataset
            })
            status = {
                'status': 'Container has been created',
                'dataset_id': dataset_id
            }
            return status
        except errors.DuplicateKeyError:
            status = {
                'status': 'Dataset can not be saved, duplicate data.'
            }
            return status

    def get_dataset(self, container_code):
        res = self._dataset_c.find_one({'container_code': DBRef('Container', container_code)})
        dataset = []
        if res is not None:
            dataset = res.get('dataset')
            return dataset
        else:
            return dataset

    def delete_dataset(self, container_code):
        count = self._dataset_c.delete_one({'container_code': DBRef('Container', container_code)}).deleted_count
        if count == 0:
            return {'error': 'Delete unsuccessful'}
        else:
            return {'status': 'Delete successful', 'deleted': True}
