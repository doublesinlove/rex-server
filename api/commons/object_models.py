class UserObject:
    def __init__(self, name, username, password, user_id=None):
        self.name = name
        self.username = username
        self.password = password
        self.user_id = user_id


class ContainerObject:

    def __init__(self, container_id, container_name, container_desc, container_code):
        self.container_id = container_id
        self.container_name = container_name
        self.container_desc = container_desc
        self.container_code = container_code

