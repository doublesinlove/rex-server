       ЃK"	   2ДжAbrain.Event:21шВh     ЌЭ7z	2ДжA"Ѕб	

-global_step/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@global_step*
dtype0*
_output_shapes
: 

#global_step/Initializer/zeros/ConstConst*
value	B	 R *
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
Ь
global_step/Initializer/zerosFill-global_step/Initializer/zeros/shape_as_tensor#global_step/Initializer/zeros/Const*
T0	*

index_type0*
_class
loc:@global_step*
_output_shapes
: 

global_step
VariableV2*
_class
loc:@global_step*
	container *
shape: *
dtype0	*
_output_shapes
: *
shared_name 
В
global_step/AssignAssignglobal_stepglobal_step/Initializer/zeros*
use_locking(*
T0	*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
j
global_step/readIdentityglobal_step*
_output_shapes
: *
T0	*
_class
loc:@global_step
З
enqueue_input/fifo_queueFIFOQueueV2"/device:CPU:0*
shared_name *
capacityш*
	container *
_output_shapes
: *
component_types
2	*
shapes

: :: 
m
enqueue_input/PlaceholderPlaceholder"/device:CPU:0*
dtype0	*
_output_shapes
:*
shape:
o
enqueue_input/Placeholder_1Placeholder"/device:CPU:0*
dtype0*
_output_shapes
:*
shape:
o
enqueue_input/Placeholder_2Placeholder"/device:CPU:0*
shape:*
dtype0*
_output_shapes
:
ы
$enqueue_input/fifo_queue_EnqueueManyQueueEnqueueManyV2enqueue_input/fifo_queueenqueue_input/Placeholderenqueue_input/Placeholder_1enqueue_input/Placeholder_2"/device:CPU:0*

timeout_msџџџџџџџџџ*
Tcomponents
2	
v
enqueue_input/fifo_queue_CloseQueueCloseV2enqueue_input/fifo_queue"/device:CPU:0*
cancel_pending_enqueues( 
x
 enqueue_input/fifo_queue_Close_1QueueCloseV2enqueue_input/fifo_queue"/device:CPU:0*
cancel_pending_enqueues(
m
enqueue_input/fifo_queue_SizeQueueSizeV2enqueue_input/fifo_queue"/device:CPU:0*
_output_shapes
: 
d
enqueue_input/sub/yConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 
|
enqueue_input/subSubenqueue_input/fifo_queue_Sizeenqueue_input/sub/y"/device:CPU:0*
_output_shapes
: *
T0
h
enqueue_input/Maximum/xConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 
|
enqueue_input/MaximumMaximumenqueue_input/Maximum/xenqueue_input/sub"/device:CPU:0*
T0*
_output_shapes
: 
p
enqueue_input/CastCastenqueue_input/Maximum"/device:CPU:0*
_output_shapes
: *

DstT0*

SrcT0
g
enqueue_input/mul/yConst"/device:CPU:0*
valueB
 *o:*
dtype0*
_output_shapes
: 
q
enqueue_input/mulMulenqueue_input/Castenqueue_input/mul/y"/device:CPU:0*
_output_shapes
: *
T0
х
Menqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full/tagsConst"/device:CPU:0*Y
valuePBN BHenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full*
dtype0*
_output_shapes
: 
ы
Henqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_fullScalarSummaryMenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full/tagsenqueue_input/mul"/device:CPU:0*
_output_shapes
: *
T0
j
fifo_queue_DequeueUpTo/nConst"/device:CPU:0*
value
B :*
dtype0*
_output_shapes
: 
э
fifo_queue_DequeueUpToQueueDequeueUpToV2enqueue_input/fifo_queuefifo_queue_DequeueUpTo/n"/device:CPU:0*E
_output_shapes3
1:џџџџџџџџџ:џџџџџџџџџ:џџџџџџџџџ*
component_types
2	*

timeout_msџџџџџџџџџ

-dense/kernel/Initializer/random_uniform/shapeConst*
valueB"   
   *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
:

+dense/kernel/Initializer/random_uniform/minConst*
valueB
 *я[ёО*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 

+dense/kernel/Initializer/random_uniform/maxConst*
valueB
 *я[ё>*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
х
5dense/kernel/Initializer/random_uniform/RandomUniformRandomUniform-dense/kernel/Initializer/random_uniform/shape*

seed *
T0*
_class
loc:@dense/kernel*
seed2 *
dtype0*
_output_shapes

:

Ю
+dense/kernel/Initializer/random_uniform/subSub+dense/kernel/Initializer/random_uniform/max+dense/kernel/Initializer/random_uniform/min*
T0*
_class
loc:@dense/kernel*
_output_shapes
: 
р
+dense/kernel/Initializer/random_uniform/mulMul5dense/kernel/Initializer/random_uniform/RandomUniform+dense/kernel/Initializer/random_uniform/sub*
T0*
_class
loc:@dense/kernel*
_output_shapes

:

в
'dense/kernel/Initializer/random_uniformAdd+dense/kernel/Initializer/random_uniform/mul+dense/kernel/Initializer/random_uniform/min*
T0*
_class
loc:@dense/kernel*
_output_shapes

:

Ё
dense/kernel
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *
_class
loc:@dense/kernel*
	container *
shape
:

Ч
dense/kernel/AssignAssigndense/kernel'dense/kernel/Initializer/random_uniform*
_output_shapes

:
*
use_locking(*
T0*
_class
loc:@dense/kernel*
validate_shape(
u
dense/kernel/readIdentitydense/kernel*
T0*
_class
loc:@dense/kernel*
_output_shapes

:


,dense/bias/Initializer/zeros/shape_as_tensorConst*
_output_shapes
:*
valueB:
*
_class
loc:@dense/bias*
dtype0

"dense/bias/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ь
dense/bias/Initializer/zerosFill,dense/bias/Initializer/zeros/shape_as_tensor"dense/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/bias*
_output_shapes
:



dense/bias
VariableV2*
_output_shapes
:
*
shared_name *
_class
loc:@dense/bias*
	container *
shape:
*
dtype0
В
dense/bias/AssignAssign
dense/biasdense/bias/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

k
dense/bias/readIdentity
dense/bias*
_class
loc:@dense/bias*
_output_shapes
:
*
T0

dense/MatMulMatMulfifo_queue_DequeueUpTo:1dense/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b( 

dense/BiasAddBiasAdddense/MatMuldense/bias/read*'
_output_shapes
:џџџџџџџџџ
*
T0*
data_formatNHWC
S

dense/ReluReludense/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ

Ѓ
/dense_1/kernel/Initializer/random_uniform/shapeConst*
valueB"
      *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
:

-dense_1/kernel/Initializer/random_uniform/minConst*
valueB
 *БП*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 

-dense_1/kernel/Initializer/random_uniform/maxConst*
valueB
 *Б?*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
ы
7dense_1/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_1/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:
*

seed *
T0*!
_class
loc:@dense_1/kernel*
seed2 
ж
-dense_1/kernel/Initializer/random_uniform/subSub-dense_1/kernel/Initializer/random_uniform/max-dense_1/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes
: 
ш
-dense_1/kernel/Initializer/random_uniform/mulMul7dense_1/kernel/Initializer/random_uniform/RandomUniform-dense_1/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

к
)dense_1/kernel/Initializer/random_uniformAdd-dense_1/kernel/Initializer/random_uniform/mul-dense_1/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

Ѕ
dense_1/kernel
VariableV2*
shared_name *!
_class
loc:@dense_1/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:

Я
dense_1/kernel/AssignAssigndense_1/kernel)dense_1/kernel/Initializer/random_uniform*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:

{
dense_1/kernel/readIdentitydense_1/kernel*
_output_shapes

:
*
T0*!
_class
loc:@dense_1/kernel

.dense_1/bias/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
:

$dense_1/bias/Initializer/zeros/ConstConst*
_output_shapes
: *
valueB
 *    *
_class
loc:@dense_1/bias*
dtype0
д
dense_1/bias/Initializer/zerosFill.dense_1/bias/Initializer/zeros/shape_as_tensor$dense_1/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_1/bias*
_output_shapes
:

dense_1/bias
VariableV2*
_class
loc:@dense_1/bias*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name 
К
dense_1/bias/AssignAssigndense_1/biasdense_1/bias/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias
q
dense_1/bias/readIdentitydense_1/bias*
T0*
_class
loc:@dense_1/bias*
_output_shapes
:

dense_1/MatMulMatMul
dense/Reludense_1/kernel/read*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b( *
T0

dense_1/BiasAddBiasAdddense_1/MatMuldense_1/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ
W
dense_1/ReluReludense_1/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
Ѓ
/dense_2/kernel/Initializer/random_uniform/shapeConst*
valueB"   
   *!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
:

-dense_2/kernel/Initializer/random_uniform/minConst*
valueB
 *БП*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 

-dense_2/kernel/Initializer/random_uniform/maxConst*
valueB
 *Б?*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
ы
7dense_2/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_2/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:
*

seed *
T0*!
_class
loc:@dense_2/kernel*
seed2 
ж
-dense_2/kernel/Initializer/random_uniform/subSub-dense_2/kernel/Initializer/random_uniform/max-dense_2/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes
: 
ш
-dense_2/kernel/Initializer/random_uniform/mulMul7dense_2/kernel/Initializer/random_uniform/RandomUniform-dense_2/kernel/Initializer/random_uniform/sub*
_output_shapes

:
*
T0*!
_class
loc:@dense_2/kernel
к
)dense_2/kernel/Initializer/random_uniformAdd-dense_2/kernel/Initializer/random_uniform/mul-dense_2/kernel/Initializer/random_uniform/min*
_output_shapes

:
*
T0*!
_class
loc:@dense_2/kernel
Ѕ
dense_2/kernel
VariableV2*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_2/kernel
Я
dense_2/kernel/AssignAssigndense_2/kernel)dense_2/kernel/Initializer/random_uniform*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(
{
dense_2/kernel/readIdentitydense_2/kernel*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:


.dense_2/bias/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
:

$dense_2/bias/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
д
dense_2/bias/Initializer/zerosFill.dense_2/bias/Initializer/zeros/shape_as_tensor$dense_2/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_2/bias*
_output_shapes
:


dense_2/bias
VariableV2*
_class
loc:@dense_2/bias*
	container *
shape:
*
dtype0*
_output_shapes
:
*
shared_name 
К
dense_2/bias/AssignAssigndense_2/biasdense_2/bias/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:

q
dense_2/bias/readIdentitydense_2/bias*
T0*
_class
loc:@dense_2/bias*
_output_shapes
:


dense_2/MatMulMatMuldense_1/Reludense_2/kernel/read*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b( *
T0

dense_2/BiasAddBiasAdddense_2/MatMuldense_2/bias/read*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ
*
T0
W
dense_2/ReluReludense_2/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ

Ѓ
/dense_3/kernel/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
valueB"
      *!
_class
loc:@dense_3/kernel

-dense_3/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *ѓ5П*!
_class
loc:@dense_3/kernel

-dense_3/kernel/Initializer/random_uniform/maxConst*
valueB
 *ѓ5?*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
ы
7dense_3/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_3/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:
*

seed *
T0*!
_class
loc:@dense_3/kernel*
seed2 
ж
-dense_3/kernel/Initializer/random_uniform/subSub-dense_3/kernel/Initializer/random_uniform/max-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes
: 
ш
-dense_3/kernel/Initializer/random_uniform/mulMul7dense_3/kernel/Initializer/random_uniform/RandomUniform-dense_3/kernel/Initializer/random_uniform/sub*
_output_shapes

:
*
T0*!
_class
loc:@dense_3/kernel
к
)dense_3/kernel/Initializer/random_uniformAdd-dense_3/kernel/Initializer/random_uniform/mul-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

Ѕ
dense_3/kernel
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_3/kernel*
	container *
shape
:

Я
dense_3/kernel/AssignAssigndense_3/kernel)dense_3/kernel/Initializer/random_uniform*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:

{
dense_3/kernel/readIdentitydense_3/kernel*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:


.dense_3/bias/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:

$dense_3/bias/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
д
dense_3/bias/Initializer/zerosFill.dense_3/bias/Initializer/zeros/shape_as_tensor$dense_3/bias/Initializer/zeros/Const*
_output_shapes
:*
T0*

index_type0*
_class
loc:@dense_3/bias

dense_3/bias
VariableV2*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@dense_3/bias*
	container *
shape:
К
dense_3/bias/AssignAssigndense_3/biasdense_3/bias/Initializer/zeros*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:*
use_locking(
q
dense_3/bias/readIdentitydense_3/bias*
_output_shapes
:*
T0*
_class
loc:@dense_3/bias

dense_3/MatMulMatMuldense_2/Reludense_3/kernel/read*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b( *
T0

dense_3/BiasAddBiasAdddense_3/MatMuldense_3/bias/read*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ*
T0
R
ArgMax/dimensionConst*
value	B :*
dtype0*
_output_shapes
: 

ArgMaxArgMaxdense_3/BiasAddArgMax/dimension*
T0*
output_type0	*#
_output_shapes
:џџџџџџџџџ*

Tidx0
U
one_hot/on_valueConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
V
one_hot/off_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
O
one_hot/depthConst*
dtype0*
_output_shapes
: *
value	B :
А
one_hotOneHotfifo_queue_DequeueUpTo:2one_hot/depthone_hot/on_valueone_hot/off_value*
T0*
TI0*
axisџџџџџџџџџ*'
_output_shapes
:џџџџџџџџџ
~
.sigmoid_cross_entropy_loss/xentropy/zeros_like	ZerosLikedense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
Г
0sigmoid_cross_entropy_loss/xentropy/GreaterEqualGreaterEqualdense_3/BiasAdd.sigmoid_cross_entropy_loss/xentropy/zeros_like*
T0*'
_output_shapes
:џџџџџџџџџ
й
*sigmoid_cross_entropy_loss/xentropy/SelectSelect0sigmoid_cross_entropy_loss/xentropy/GreaterEqualdense_3/BiasAdd.sigmoid_cross_entropy_loss/xentropy/zeros_like*'
_output_shapes
:џџџџџџџџџ*
T0
q
'sigmoid_cross_entropy_loss/xentropy/NegNegdense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
д
,sigmoid_cross_entropy_loss/xentropy/Select_1Select0sigmoid_cross_entropy_loss/xentropy/GreaterEqual'sigmoid_cross_entropy_loss/xentropy/Negdense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
z
'sigmoid_cross_entropy_loss/xentropy/mulMuldense_3/BiasAddone_hot*
T0*'
_output_shapes
:џџџџџџџџџ
Е
'sigmoid_cross_entropy_loss/xentropy/subSub*sigmoid_cross_entropy_loss/xentropy/Select'sigmoid_cross_entropy_loss/xentropy/mul*
T0*'
_output_shapes
:џџџџџџџџџ

'sigmoid_cross_entropy_loss/xentropy/ExpExp,sigmoid_cross_entropy_loss/xentropy/Select_1*'
_output_shapes
:џџџџџџџџџ*
T0

)sigmoid_cross_entropy_loss/xentropy/Log1pLog1p'sigmoid_cross_entropy_loss/xentropy/Exp*
T0*'
_output_shapes
:џџџџџџџџџ
А
#sigmoid_cross_entropy_loss/xentropyAdd'sigmoid_cross_entropy_loss/xentropy/sub)sigmoid_cross_entropy_loss/xentropy/Log1p*'
_output_shapes
:џџџџџџџџџ*
T0
|
7sigmoid_cross_entropy_loss/assert_broadcastable/weightsConst*
dtype0*
_output_shapes
: *
valueB
 *  ?

=sigmoid_cross_entropy_loss/assert_broadcastable/weights/shapeConst*
dtype0*
_output_shapes
: *
valueB 
~
<sigmoid_cross_entropy_loss/assert_broadcastable/weights/rankConst*
value	B : *
dtype0*
_output_shapes
: 

<sigmoid_cross_entropy_loss/assert_broadcastable/values/shapeShape#sigmoid_cross_entropy_loss/xentropy*
T0*
out_type0*
_output_shapes
:
}
;sigmoid_cross_entropy_loss/assert_broadcastable/values/rankConst*
value	B :*
dtype0*
_output_shapes
: 
S
Ksigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_successNoOp
Й
&sigmoid_cross_entropy_loss/ToFloat_1/xConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Є
sigmoid_cross_entropy_loss/MulMul#sigmoid_cross_entropy_loss/xentropy&sigmoid_cross_entropy_loss/ToFloat_1/x*
T0*'
_output_shapes
:џџџџџџџџџ
П
 sigmoid_cross_entropy_loss/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB"       *
dtype0*
_output_shapes
:
Ѕ
sigmoid_cross_entropy_loss/SumSumsigmoid_cross_entropy_loss/Mul sigmoid_cross_entropy_loss/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
С
.sigmoid_cross_entropy_loss/num_present/Equal/yConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 
Ў
,sigmoid_cross_entropy_loss/num_present/EqualEqual&sigmoid_cross_entropy_loss/ToFloat_1/x.sigmoid_cross_entropy_loss/num_present/Equal/y*
T0*
_output_shapes
: 
в
Asigmoid_cross_entropy_loss/num_present/zeros_like/shape_as_tensorConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB 
Ъ
7sigmoid_cross_entropy_loss/num_present/zeros_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 
ш
1sigmoid_cross_entropy_loss/num_present/zeros_likeFillAsigmoid_cross_entropy_loss/num_present/zeros_like/shape_as_tensor7sigmoid_cross_entropy_loss/num_present/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
Ч
6sigmoid_cross_entropy_loss/num_present/ones_like/ShapeConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB 
Щ
6sigmoid_cross_entropy_loss/num_present/ones_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
л
0sigmoid_cross_entropy_loss/num_present/ones_likeFill6sigmoid_cross_entropy_loss/num_present/ones_like/Shape6sigmoid_cross_entropy_loss/num_present/ones_like/Const*
_output_shapes
: *
T0*

index_type0
ы
-sigmoid_cross_entropy_loss/num_present/SelectSelect,sigmoid_cross_entropy_loss/num_present/Equal1sigmoid_cross_entropy_loss/num_present/zeros_like0sigmoid_cross_entropy_loss/num_present/ones_like*
_output_shapes
: *
T0
ь
[sigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/weights/shapeConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB 
ъ
Zsigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/weights/rankConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
value	B : *
dtype0*
_output_shapes
: 

Zsigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/values/shapeShape#sigmoid_cross_entropy_loss/xentropyL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
_output_shapes
:*
T0*
out_type0
щ
Ysigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/values/rankConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
value	B :*
dtype0*
_output_shapes
: 
П
isigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/static_scalar_check_successNoOpL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success
х
Hsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/ShapeShape#sigmoid_cross_entropy_loss/xentropyL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_successj^sigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/static_scalar_check_success*
T0*
out_type0*
_output_shapes
:
Ч
Hsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_successj^sigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Ђ
Bsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_likeFillHsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/ShapeHsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/Const*
T0*

index_type0*'
_output_shapes
:џџџџџџџџџ
ф
8sigmoid_cross_entropy_loss/num_present/broadcast_weightsMul-sigmoid_cross_entropy_loss/num_present/SelectBsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like*
T0*'
_output_shapes
:џџџџџџџџџ
Ы
,sigmoid_cross_entropy_loss/num_present/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
:*
valueB"       
г
&sigmoid_cross_entropy_loss/num_presentSum8sigmoid_cross_entropy_loss/num_present/broadcast_weights,sigmoid_cross_entropy_loss/num_present/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 
Г
"sigmoid_cross_entropy_loss/Const_1ConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
Љ
 sigmoid_cross_entropy_loss/Sum_1Sumsigmoid_cross_entropy_loss/Sum"sigmoid_cross_entropy_loss/Const_1*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
З
$sigmoid_cross_entropy_loss/Greater/yConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 

"sigmoid_cross_entropy_loss/GreaterGreater&sigmoid_cross_entropy_loss/num_present$sigmoid_cross_entropy_loss/Greater/y*
T0*
_output_shapes
: 
Е
"sigmoid_cross_entropy_loss/Equal/yConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB
 *    

 sigmoid_cross_entropy_loss/EqualEqual&sigmoid_cross_entropy_loss/num_present"sigmoid_cross_entropy_loss/Equal/y*
T0*
_output_shapes
: 
Л
*sigmoid_cross_entropy_loss/ones_like/ShapeConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
Н
*sigmoid_cross_entropy_loss/ones_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB
 *  ?
З
$sigmoid_cross_entropy_loss/ones_likeFill*sigmoid_cross_entropy_loss/ones_like/Shape*sigmoid_cross_entropy_loss/ones_like/Const*
T0*

index_type0*
_output_shapes
: 
М
!sigmoid_cross_entropy_loss/SelectSelect sigmoid_cross_entropy_loss/Equal$sigmoid_cross_entropy_loss/ones_like&sigmoid_cross_entropy_loss/num_present*
T0*
_output_shapes
: 

sigmoid_cross_entropy_loss/divRealDiv sigmoid_cross_entropy_loss/Sum_1!sigmoid_cross_entropy_loss/Select*
_output_shapes
: *
T0
Ц
5sigmoid_cross_entropy_loss/zeros_like/shape_as_tensorConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
О
+sigmoid_cross_entropy_loss/zeros_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 
Ф
%sigmoid_cross_entropy_loss/zeros_likeFill5sigmoid_cross_entropy_loss/zeros_like/shape_as_tensor+sigmoid_cross_entropy_loss/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
Ж
 sigmoid_cross_entropy_loss/valueSelect"sigmoid_cross_entropy_loss/Greatersigmoid_cross_entropy_loss/div%sigmoid_cross_entropy_loss/zeros_like*
_output_shapes
: *
T0
R
gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
X
gradients/grad_ys_0Const*
valueB
 *  ?*
dtype0*
_output_shapes
: 
o
gradients/FillFillgradients/Shapegradients/grad_ys_0*
_output_shapes
: *
T0*

index_type0

Jgradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB 

@gradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

:gradients/sigmoid_cross_entropy_loss/value_grad/zeros_likeFillJgradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/shape_as_tensor@gradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
б
6gradients/sigmoid_cross_entropy_loss/value_grad/SelectSelect"sigmoid_cross_entropy_loss/Greatergradients/Fill:gradients/sigmoid_cross_entropy_loss/value_grad/zeros_like*
_output_shapes
: *
T0
г
8gradients/sigmoid_cross_entropy_loss/value_grad/Select_1Select"sigmoid_cross_entropy_loss/Greater:gradients/sigmoid_cross_entropy_loss/value_grad/zeros_likegradients/Fill*
T0*
_output_shapes
: 
М
@gradients/sigmoid_cross_entropy_loss/value_grad/tuple/group_depsNoOp7^gradients/sigmoid_cross_entropy_loss/value_grad/Select9^gradients/sigmoid_cross_entropy_loss/value_grad/Select_1
Л
Hgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependencyIdentity6gradients/sigmoid_cross_entropy_loss/value_grad/SelectA^gradients/sigmoid_cross_entropy_loss/value_grad/tuple/group_deps*
T0*I
_class?
=;loc:@gradients/sigmoid_cross_entropy_loss/value_grad/Select*
_output_shapes
: 
С
Jgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependency_1Identity8gradients/sigmoid_cross_entropy_loss/value_grad/Select_1A^gradients/sigmoid_cross_entropy_loss/value_grad/tuple/group_deps*
T0*K
_classA
?=loc:@gradients/sigmoid_cross_entropy_loss/value_grad/Select_1*
_output_shapes
: 
v
3gradients/sigmoid_cross_entropy_loss/div_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
x
5gradients/sigmoid_cross_entropy_loss/div_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 

Cgradients/sigmoid_cross_entropy_loss/div_grad/BroadcastGradientArgsBroadcastGradientArgs3gradients/sigmoid_cross_entropy_loss/div_grad/Shape5gradients/sigmoid_cross_entropy_loss/div_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ
Ю
5gradients/sigmoid_cross_entropy_loss/div_grad/RealDivRealDivHgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependency!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
є
1gradients/sigmoid_cross_entropy_loss/div_grad/SumSum5gradients/sigmoid_cross_entropy_loss/div_grad/RealDivCgradients/sigmoid_cross_entropy_loss/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
з
5gradients/sigmoid_cross_entropy_loss/div_grad/ReshapeReshape1gradients/sigmoid_cross_entropy_loss/div_grad/Sum3gradients/sigmoid_cross_entropy_loss/div_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
{
1gradients/sigmoid_cross_entropy_loss/div_grad/NegNeg sigmoid_cross_entropy_loss/Sum_1*
_output_shapes
: *
T0
Й
7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_1RealDiv1gradients/sigmoid_cross_entropy_loss/div_grad/Neg!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
П
7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_2RealDiv7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_1!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
м
1gradients/sigmoid_cross_entropy_loss/div_grad/mulMulHgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependency7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_2*
_output_shapes
: *
T0
є
3gradients/sigmoid_cross_entropy_loss/div_grad/Sum_1Sum1gradients/sigmoid_cross_entropy_loss/div_grad/mulEgradients/sigmoid_cross_entropy_loss/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
н
7gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1Reshape3gradients/sigmoid_cross_entropy_loss/div_grad/Sum_15gradients/sigmoid_cross_entropy_loss/div_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
И
>gradients/sigmoid_cross_entropy_loss/div_grad/tuple/group_depsNoOp6^gradients/sigmoid_cross_entropy_loss/div_grad/Reshape8^gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1
Е
Fgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependencyIdentity5gradients/sigmoid_cross_entropy_loss/div_grad/Reshape?^gradients/sigmoid_cross_entropy_loss/div_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/sigmoid_cross_entropy_loss/div_grad/Reshape*
_output_shapes
: 
Л
Hgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency_1Identity7gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1?^gradients/sigmoid_cross_entropy_loss/div_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1*
_output_shapes
: 

=gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Reshape/shapeConst*
valueB *
dtype0*
_output_shapes
: 
ј
7gradients/sigmoid_cross_entropy_loss/Sum_1_grad/ReshapeReshapeFgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency=gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes
: 

>gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Tile/multiplesConst*
valueB *
dtype0*
_output_shapes
: 
ш
4gradients/sigmoid_cross_entropy_loss/Sum_1_grad/TileTile7gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Reshape>gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Tile/multiples*

Tmultiples0*
T0*
_output_shapes
: 

Kgradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 

Agradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    

;gradients/sigmoid_cross_entropy_loss/Select_grad/zeros_likeFillKgradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/shape_as_tensorAgradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 

7gradients/sigmoid_cross_entropy_loss/Select_grad/SelectSelect sigmoid_cross_entropy_loss/EqualHgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency_1;gradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like*
_output_shapes
: *
T0

9gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1Select sigmoid_cross_entropy_loss/Equal;gradients/sigmoid_cross_entropy_loss/Select_grad/zeros_likeHgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency_1*
T0*
_output_shapes
: 
П
Agradients/sigmoid_cross_entropy_loss/Select_grad/tuple/group_depsNoOp8^gradients/sigmoid_cross_entropy_loss/Select_grad/Select:^gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1
П
Igradients/sigmoid_cross_entropy_loss/Select_grad/tuple/control_dependencyIdentity7gradients/sigmoid_cross_entropy_loss/Select_grad/SelectB^gradients/sigmoid_cross_entropy_loss/Select_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/sigmoid_cross_entropy_loss/Select_grad/Select*
_output_shapes
: 
Х
Kgradients/sigmoid_cross_entropy_loss/Select_grad/tuple/control_dependency_1Identity9gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1B^gradients/sigmoid_cross_entropy_loss/Select_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1*
_output_shapes
: 

;gradients/sigmoid_cross_entropy_loss/Sum_grad/Reshape/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
ъ
5gradients/sigmoid_cross_entropy_loss/Sum_grad/ReshapeReshape4gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Tile;gradients/sigmoid_cross_entropy_loss/Sum_grad/Reshape/shape*
_output_shapes

:*
T0*
Tshape0

3gradients/sigmoid_cross_entropy_loss/Sum_grad/ShapeShapesigmoid_cross_entropy_loss/Mul*
T0*
out_type0*
_output_shapes
:
ъ
2gradients/sigmoid_cross_entropy_loss/Sum_grad/TileTile5gradients/sigmoid_cross_entropy_loss/Sum_grad/Reshape3gradients/sigmoid_cross_entropy_loss/Sum_grad/Shape*

Tmultiples0*
T0*'
_output_shapes
:џџџџџџџџџ

3gradients/sigmoid_cross_entropy_loss/Mul_grad/ShapeShape#sigmoid_cross_entropy_loss/xentropy*
T0*
out_type0*
_output_shapes
:
x
5gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 

Cgradients/sigmoid_cross_entropy_loss/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs3gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape5gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ
Ц
1gradients/sigmoid_cross_entropy_loss/Mul_grad/mulMul2gradients/sigmoid_cross_entropy_loss/Sum_grad/Tile&sigmoid_cross_entropy_loss/ToFloat_1/x*
T0*'
_output_shapes
:џџџџџџџџџ
№
1gradients/sigmoid_cross_entropy_loss/Mul_grad/SumSum1gradients/sigmoid_cross_entropy_loss/Mul_grad/mulCgradients/sigmoid_cross_entropy_loss/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
ш
5gradients/sigmoid_cross_entropy_loss/Mul_grad/ReshapeReshape1gradients/sigmoid_cross_entropy_loss/Mul_grad/Sum3gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
Х
3gradients/sigmoid_cross_entropy_loss/Mul_grad/mul_1Mul#sigmoid_cross_entropy_loss/xentropy2gradients/sigmoid_cross_entropy_loss/Sum_grad/Tile*
T0*'
_output_shapes
:џџџџџџџџџ
і
3gradients/sigmoid_cross_entropy_loss/Mul_grad/Sum_1Sum3gradients/sigmoid_cross_entropy_loss/Mul_grad/mul_1Egradients/sigmoid_cross_entropy_loss/Mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
н
7gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1Reshape3gradients/sigmoid_cross_entropy_loss/Mul_grad/Sum_15gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
И
>gradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/group_depsNoOp6^gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape8^gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1
Ц
Fgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependencyIdentity5gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape?^gradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape*'
_output_shapes
:џџџџџџџџџ
Л
Hgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependency_1Identity7gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1?^gradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1*
_output_shapes
: 

Cgradients/sigmoid_cross_entropy_loss/num_present_grad/Reshape/shapeConst*
valueB"      *
dtype0*
_output_shapes
:

=gradients/sigmoid_cross_entropy_loss/num_present_grad/ReshapeReshapeKgradients/sigmoid_cross_entropy_loss/Select_grad/tuple/control_dependency_1Cgradients/sigmoid_cross_entropy_loss/num_present_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes

:
Г
;gradients/sigmoid_cross_entropy_loss/num_present_grad/ShapeShape8sigmoid_cross_entropy_loss/num_present/broadcast_weights*
T0*
out_type0*
_output_shapes
:

:gradients/sigmoid_cross_entropy_loss/num_present_grad/TileTile=gradients/sigmoid_cross_entropy_loss/num_present_grad/Reshape;gradients/sigmoid_cross_entropy_loss/num_present_grad/Shape*'
_output_shapes
:џџџџџџџџџ*

Tmultiples0*
T0

Mgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ShapeConst*
dtype0*
_output_shapes
: *
valueB 
б
Ogradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape_1ShapeBsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like*
T0*
out_type0*
_output_shapes
:
г
]gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/BroadcastGradientArgsBroadcastGradientArgsMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ShapeOgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape_1*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ*
T0

Kgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mulMul:gradients/sigmoid_cross_entropy_loss/num_present_grad/TileBsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like*
T0*'
_output_shapes
:џџџџџџџџџ
О
Kgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/SumSumKgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mul]gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Ѕ
Ogradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ReshapeReshapeKgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/SumMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ё
Mgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mul_1Mul-sigmoid_cross_entropy_loss/num_present/Select:gradients/sigmoid_cross_entropy_loss/num_present_grad/Tile*
T0*'
_output_shapes
:џџџџџџџџџ
Ф
Mgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Sum_1SumMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mul_1_gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
М
Qgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1ReshapeMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Sum_1Ogradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ

Xgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/group_depsNoOpP^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ReshapeR^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1

`gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/control_dependencyIdentityOgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ReshapeY^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/group_deps*
T0*b
_classX
VTloc:@gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape*
_output_shapes
: 
Д
bgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/control_dependency_1IdentityQgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1Y^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*d
_classZ
XVloc:@gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1
Ј
Wgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like_grad/ConstConst*
dtype0*
_output_shapes
:*
valueB"       
з
Ugradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like_grad/SumSumbgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/control_dependency_1Wgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like_grad/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 

8gradients/sigmoid_cross_entropy_loss/xentropy_grad/ShapeShape'sigmoid_cross_entropy_loss/xentropy/sub*
T0*
out_type0*
_output_shapes
:
Ѓ
:gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape_1Shape)sigmoid_cross_entropy_loss/xentropy/Log1p*
T0*
out_type0*
_output_shapes
:

Hgradients/sigmoid_cross_entropy_loss/xentropy_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape:gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape_1*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ*
T0

6gradients/sigmoid_cross_entropy_loss/xentropy_grad/SumSumFgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependencyHgradients/sigmoid_cross_entropy_loss/xentropy_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ї
:gradients/sigmoid_cross_entropy_loss/xentropy_grad/ReshapeReshape6gradients/sigmoid_cross_entropy_loss/xentropy_grad/Sum8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape*'
_output_shapes
:џџџџџџџџџ*
T0*
Tshape0

8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Sum_1SumFgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependencyJgradients/sigmoid_cross_entropy_loss/xentropy_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
§
<gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1Reshape8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Sum_1:gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
Ч
Cgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/group_depsNoOp;^gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape=^gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1
к
Kgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependencyIdentity:gradients/sigmoid_cross_entropy_loss/xentropy_grad/ReshapeD^gradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*M
_classC
A?loc:@gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape
р
Mgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependency_1Identity<gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1D^gradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1*'
_output_shapes
:џџџџџџџџџ
І
<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ShapeShape*sigmoid_cross_entropy_loss/xentropy/Select*
T0*
out_type0*
_output_shapes
:
Ѕ
>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape_1Shape'sigmoid_cross_entropy_loss/xentropy/mul*
_output_shapes
:*
T0*
out_type0
 
Lgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/BroadcastGradientArgsBroadcastGradientArgs<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ

:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/SumSumKgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependencyLgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0

>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ReshapeReshape:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Sum<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
 
<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Sum_1SumKgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependencyNgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Ђ
:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/NegNeg<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Sum_1*
T0*
_output_shapes
:

@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1Reshape:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Neg>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape_1*'
_output_shapes
:џџџџџџџџџ*
T0*
Tshape0
г
Ggradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/group_depsNoOp?^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ReshapeA^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1
ъ
Ogradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependencyIdentity>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ReshapeH^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape*'
_output_shapes
:џџџџџџџџџ
№
Qgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency_1Identity@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1H^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1*'
_output_shapes
:џџџџџџџџџ
г
>gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/add/xConstN^gradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependency_1*
valueB
 *  ?*
dtype0*
_output_shapes
: 
о
<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/addAdd>gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/add/x'sigmoid_cross_entropy_loss/xentropy/Exp*'
_output_shapes
:џџџџџџџџџ*
T0
С
Cgradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/Reciprocal
Reciprocal<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/add*
T0*'
_output_shapes
:џџџџџџџџџ

<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/mulMulMgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependency_1Cgradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/Reciprocal*
T0*'
_output_shapes
:џџџџџџџџџ

Dgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/zeros_like	ZerosLikedense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
Х
@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/SelectSelect0sigmoid_cross_entropy_loss/xentropy/GreaterEqualOgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependencyDgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/zeros_like*'
_output_shapes
:џџџџџџџџџ*
T0
Ч
Bgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1Select0sigmoid_cross_entropy_loss/xentropy/GreaterEqualDgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/zeros_likeOgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency*
T0*'
_output_shapes
:џџџџџџџџџ
к
Jgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/group_depsNoOpA^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/SelectC^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1
є
Rgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/control_dependencyIdentity@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/SelectK^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select*'
_output_shapes
:џџџџџџџџџ
њ
Tgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/control_dependency_1IdentityBgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1K^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*U
_classK
IGloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1

<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ShapeShapedense_3/BiasAdd*
T0*
out_type0*
_output_shapes
:

>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape_1Shapeone_hot*
T0*
out_type0*
_output_shapes
:
 
Lgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/BroadcastGradientArgsBroadcastGradientArgs<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape_1*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ*
T0
Я
:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mulMulQgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency_1one_hot*'
_output_shapes
:џџџџџџџџџ*
T0

:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/SumSum:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mulLgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0

>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ReshapeReshape:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Sum<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape*'
_output_shapes
:џџџџџџџџџ*
T0*
Tshape0
й
<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mul_1Muldense_3/BiasAddQgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency_1*
T0*'
_output_shapes
:џџџџџџџџџ

<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Sum_1Sum<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mul_1Ngradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 

@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1Reshape<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Sum_1>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
г
Ggradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/group_depsNoOp?^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ReshapeA^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1
ъ
Ogradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/control_dependencyIdentity>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ReshapeH^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*Q
_classG
ECloc:@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape
№
Qgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/control_dependency_1Identity@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1H^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1*'
_output_shapes
:џџџџџџџџџ
к
:gradients/sigmoid_cross_entropy_loss/xentropy/Exp_grad/mulMul<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/mul'sigmoid_cross_entropy_loss/xentropy/Exp*
T0*'
_output_shapes
:џџџџџџџџџ
Ў
Fgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/zeros_like	ZerosLike'sigmoid_cross_entropy_loss/xentropy/Neg*
T0*'
_output_shapes
:џџџџџџџџџ
Д
Bgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/SelectSelect0sigmoid_cross_entropy_loss/xentropy/GreaterEqual:gradients/sigmoid_cross_entropy_loss/xentropy/Exp_grad/mulFgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/zeros_like*
T0*'
_output_shapes
:џџџџџџџџџ
Ж
Dgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1Select0sigmoid_cross_entropy_loss/xentropy/GreaterEqualFgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/zeros_like:gradients/sigmoid_cross_entropy_loss/xentropy/Exp_grad/mul*
T0*'
_output_shapes
:џџџџџџџџџ
р
Lgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/group_depsNoOpC^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/SelectE^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1
ќ
Tgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependencyIdentityBgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/SelectM^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select*'
_output_shapes
:џџџџџџџџџ

Vgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependency_1IdentityDgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1M^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/group_deps*
T0*W
_classM
KIloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1*'
_output_shapes
:џџџџџџџџџ
Щ
:gradients/sigmoid_cross_entropy_loss/xentropy/Neg_grad/NegNegTgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependency*
T0*'
_output_shapes
:џџџџџџџџџ
п
gradients/AddNAddNRgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/control_dependencyOgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/control_dependencyVgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependency_1:gradients/sigmoid_cross_entropy_loss/xentropy/Neg_grad/Neg*
N*'
_output_shapes
:џџџџџџџџџ*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select

*gradients/dense_3/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN*
T0*
data_formatNHWC*
_output_shapes
:
u
/gradients/dense_3/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN+^gradients/dense_3/BiasAdd_grad/BiasAddGrad

7gradients/dense_3/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN0^gradients/dense_3/BiasAdd_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select*'
_output_shapes
:џџџџџџџџџ

9gradients/dense_3/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_3/BiasAdd_grad/BiasAddGrad0^gradients/dense_3/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_3/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
д
$gradients/dense_3/MatMul_grad/MatMulMatMul7gradients/dense_3/BiasAdd_grad/tuple/control_dependencydense_3/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b(
Ц
&gradients/dense_3/MatMul_grad/MatMul_1MatMuldense_2/Relu7gradients/dense_3/BiasAdd_grad/tuple/control_dependency*
T0*
_output_shapes

:
*
transpose_a(*
transpose_b( 

.gradients/dense_3/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_3/MatMul_grad/MatMul'^gradients/dense_3/MatMul_grad/MatMul_1

6gradients/dense_3/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_3/MatMul_grad/MatMul/^gradients/dense_3/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_3/MatMul_grad/MatMul*'
_output_shapes
:џџџџџџџџџ


8gradients/dense_3/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_3/MatMul_grad/MatMul_1/^gradients/dense_3/MatMul_grad/tuple/group_deps*
_output_shapes

:
*
T0*9
_class/
-+loc:@gradients/dense_3/MatMul_grad/MatMul_1
Ј
$gradients/dense_2/Relu_grad/ReluGradReluGrad6gradients/dense_3/MatMul_grad/tuple/control_dependencydense_2/Relu*'
_output_shapes
:џџџџџџџџџ
*
T0

*gradients/dense_2/BiasAdd_grad/BiasAddGradBiasAddGrad$gradients/dense_2/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:


/gradients/dense_2/BiasAdd_grad/tuple/group_depsNoOp%^gradients/dense_2/Relu_grad/ReluGrad+^gradients/dense_2/BiasAdd_grad/BiasAddGrad

7gradients/dense_2/BiasAdd_grad/tuple/control_dependencyIdentity$gradients/dense_2/Relu_grad/ReluGrad0^gradients/dense_2/BiasAdd_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_2/Relu_grad/ReluGrad*'
_output_shapes
:џџџџџџџџџ


9gradients/dense_2/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_2/BiasAdd_grad/BiasAddGrad0^gradients/dense_2/BiasAdd_grad/tuple/group_deps*
_output_shapes
:
*
T0*=
_class3
1/loc:@gradients/dense_2/BiasAdd_grad/BiasAddGrad
д
$gradients/dense_2/MatMul_grad/MatMulMatMul7gradients/dense_2/BiasAdd_grad/tuple/control_dependencydense_2/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b(
Ц
&gradients/dense_2/MatMul_grad/MatMul_1MatMuldense_1/Relu7gradients/dense_2/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
_output_shapes

:
*
transpose_a(

.gradients/dense_2/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_2/MatMul_grad/MatMul'^gradients/dense_2/MatMul_grad/MatMul_1

6gradients/dense_2/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_2/MatMul_grad/MatMul/^gradients/dense_2/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_2/MatMul_grad/MatMul*'
_output_shapes
:џџџџџџџџџ

8gradients/dense_2/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_2/MatMul_grad/MatMul_1/^gradients/dense_2/MatMul_grad/tuple/group_deps*
_output_shapes

:
*
T0*9
_class/
-+loc:@gradients/dense_2/MatMul_grad/MatMul_1
Ј
$gradients/dense_1/Relu_grad/ReluGradReluGrad6gradients/dense_2/MatMul_grad/tuple/control_dependencydense_1/Relu*'
_output_shapes
:џџџџџџџџџ*
T0

*gradients/dense_1/BiasAdd_grad/BiasAddGradBiasAddGrad$gradients/dense_1/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:

/gradients/dense_1/BiasAdd_grad/tuple/group_depsNoOp%^gradients/dense_1/Relu_grad/ReluGrad+^gradients/dense_1/BiasAdd_grad/BiasAddGrad

7gradients/dense_1/BiasAdd_grad/tuple/control_dependencyIdentity$gradients/dense_1/Relu_grad/ReluGrad0^gradients/dense_1/BiasAdd_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_1/Relu_grad/ReluGrad*'
_output_shapes
:џџџџџџџџџ

9gradients/dense_1/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_1/BiasAdd_grad/BiasAddGrad0^gradients/dense_1/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_1/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
д
$gradients/dense_1/MatMul_grad/MatMulMatMul7gradients/dense_1/BiasAdd_grad/tuple/control_dependencydense_1/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b(
Ф
&gradients/dense_1/MatMul_grad/MatMul_1MatMul
dense/Relu7gradients/dense_1/BiasAdd_grad/tuple/control_dependency*
T0*
_output_shapes

:
*
transpose_a(*
transpose_b( 

.gradients/dense_1/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_1/MatMul_grad/MatMul'^gradients/dense_1/MatMul_grad/MatMul_1

6gradients/dense_1/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_1/MatMul_grad/MatMul/^gradients/dense_1/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_1/MatMul_grad/MatMul*'
_output_shapes
:џџџџџџџџџ


8gradients/dense_1/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_1/MatMul_grad/MatMul_1/^gradients/dense_1/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_1/MatMul_grad/MatMul_1*
_output_shapes

:

Є
"gradients/dense/Relu_grad/ReluGradReluGrad6gradients/dense_1/MatMul_grad/tuple/control_dependency
dense/Relu*
T0*'
_output_shapes
:џџџџџџџџџ


(gradients/dense/BiasAdd_grad/BiasAddGradBiasAddGrad"gradients/dense/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:


-gradients/dense/BiasAdd_grad/tuple/group_depsNoOp#^gradients/dense/Relu_grad/ReluGrad)^gradients/dense/BiasAdd_grad/BiasAddGrad
ў
5gradients/dense/BiasAdd_grad/tuple/control_dependencyIdentity"gradients/dense/Relu_grad/ReluGrad.^gradients/dense/BiasAdd_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense/Relu_grad/ReluGrad*'
_output_shapes
:џџџџџџџџџ

џ
7gradients/dense/BiasAdd_grad/tuple/control_dependency_1Identity(gradients/dense/BiasAdd_grad/BiasAddGrad.^gradients/dense/BiasAdd_grad/tuple/group_deps*
_output_shapes
:
*
T0*;
_class1
/-loc:@gradients/dense/BiasAdd_grad/BiasAddGrad
Ю
"gradients/dense/MatMul_grad/MatMulMatMul5gradients/dense/BiasAdd_grad/tuple/control_dependencydense/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b(
Ю
$gradients/dense/MatMul_grad/MatMul_1MatMulfifo_queue_DequeueUpTo:15gradients/dense/BiasAdd_grad/tuple/control_dependency*
T0*
_output_shapes

:
*
transpose_a(*
transpose_b( 

,gradients/dense/MatMul_grad/tuple/group_depsNoOp#^gradients/dense/MatMul_grad/MatMul%^gradients/dense/MatMul_grad/MatMul_1
ќ
4gradients/dense/MatMul_grad/tuple/control_dependencyIdentity"gradients/dense/MatMul_grad/MatMul-^gradients/dense/MatMul_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense/MatMul_grad/MatMul*'
_output_shapes
:џџџџџџџџџ
љ
6gradients/dense/MatMul_grad/tuple/control_dependency_1Identity$gradients/dense/MatMul_grad/MatMul_1-^gradients/dense/MatMul_grad/tuple/group_deps*
_output_shapes

:
*
T0*7
_class-
+)loc:@gradients/dense/MatMul_grad/MatMul_1
}
beta1_power/initial_valueConst*
valueB
 *fff?*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 

beta1_power
VariableV2*
_class
loc:@dense/bias*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name 
­
beta1_power/AssignAssignbeta1_powerbeta1_power/initial_value*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: 
i
beta1_power/readIdentitybeta1_power*
T0*
_class
loc:@dense/bias*
_output_shapes
: 
}
beta2_power/initial_valueConst*
dtype0*
_output_shapes
: *
valueB
 *wО?*
_class
loc:@dense/bias

beta2_power
VariableV2*
dtype0*
_output_shapes
: *
shared_name *
_class
loc:@dense/bias*
	container *
shape: 
­
beta2_power/AssignAssignbeta2_powerbeta2_power/initial_value*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: 
i
beta2_power/readIdentitybeta2_power*
T0*
_class
loc:@dense/bias*
_output_shapes
: 
Ѕ
3dense/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"   
   *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
:

)dense/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
ч
#dense/kernel/Adam/Initializer/zerosFill3dense/kernel/Adam/Initializer/zeros/shape_as_tensor)dense/kernel/Adam/Initializer/zeros/Const*
_output_shapes

:
*
T0*

index_type0*
_class
loc:@dense/kernel
І
dense/kernel/Adam
VariableV2*
_class
loc:@dense/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name 
Э
dense/kernel/Adam/AssignAssigndense/kernel/Adam#dense/kernel/Adam/Initializer/zeros*
T0*
_class
loc:@dense/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(

dense/kernel/Adam/readIdentitydense/kernel/Adam*
T0*
_class
loc:@dense/kernel*
_output_shapes

:

Ї
5dense/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"   
   *
_class
loc:@dense/kernel

+dense/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
э
%dense/kernel/Adam_1/Initializer/zerosFill5dense/kernel/Adam_1/Initializer/zeros/shape_as_tensor+dense/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/kernel*
_output_shapes

:

Ј
dense/kernel/Adam_1
VariableV2*
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *
_class
loc:@dense/kernel*
	container 
г
dense/kernel/Adam_1/AssignAssigndense/kernel/Adam_1%dense/kernel/Adam_1/Initializer/zeros*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*
_class
loc:@dense/kernel

dense/kernel/Adam_1/readIdentitydense/kernel/Adam_1*
_output_shapes

:
*
T0*
_class
loc:@dense/kernel

1dense/bias/Adam/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:
*
_class
loc:@dense/bias

'dense/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
л
!dense/bias/Adam/Initializer/zerosFill1dense/bias/Adam/Initializer/zeros/shape_as_tensor'dense/bias/Adam/Initializer/zeros/Const*
_output_shapes
:
*
T0*

index_type0*
_class
loc:@dense/bias

dense/bias/Adam
VariableV2*
shape:
*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense/bias*
	container 
С
dense/bias/Adam/AssignAssigndense/bias/Adam!dense/bias/Adam/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

u
dense/bias/Adam/readIdentitydense/bias/Adam*
T0*
_class
loc:@dense/bias*
_output_shapes
:


3dense/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense/bias*
dtype0*
_output_shapes
:

)dense/bias/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
с
#dense/bias/Adam_1/Initializer/zerosFill3dense/bias/Adam_1/Initializer/zeros/shape_as_tensor)dense/bias/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/bias*
_output_shapes
:


dense/bias/Adam_1
VariableV2*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense/bias*
	container *
shape:

Ч
dense/bias/Adam_1/AssignAssigndense/bias/Adam_1#dense/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

y
dense/bias/Adam_1/readIdentitydense/bias/Adam_1*
T0*
_class
loc:@dense/bias*
_output_shapes
:

Љ
5dense_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
:

+dense_1/kernel/Adam/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *!
_class
loc:@dense_1/kernel
я
%dense_1/kernel/Adam/Initializer/zerosFill5dense_1/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_1/kernel/Adam/Initializer/zeros/Const*
_output_shapes

:
*
T0*

index_type0*!
_class
loc:@dense_1/kernel
Њ
dense_1/kernel/Adam
VariableV2*
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_1/kernel*
	container 
е
dense_1/kernel/Adam/AssignAssigndense_1/kernel/Adam%dense_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:


dense_1/kernel/Adam/readIdentitydense_1/kernel/Adam*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

Ћ
7dense_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
:

-dense_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
ѕ
'dense_1/kernel/Adam_1/Initializer/zerosFill7dense_1/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_1/kernel/Adam_1/Initializer/zeros/Const*
_output_shapes

:
*
T0*

index_type0*!
_class
loc:@dense_1/kernel
Ќ
dense_1/kernel/Adam_1
VariableV2*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_1/kernel
л
dense_1/kernel/Adam_1/AssignAssigndense_1/kernel/Adam_1'dense_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:


dense_1/kernel/Adam_1/readIdentitydense_1/kernel/Adam_1*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:


3dense_1/bias/Adam/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:*
_class
loc:@dense_1/bias

)dense_1/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
у
#dense_1/bias/Adam/Initializer/zerosFill3dense_1/bias/Adam/Initializer/zeros/shape_as_tensor)dense_1/bias/Adam/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_1/bias*
_output_shapes
:

dense_1/bias/Adam
VariableV2*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@dense_1/bias*
	container *
shape:
Щ
dense_1/bias/Adam/AssignAssigndense_1/bias/Adam#dense_1/bias/Adam/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias
{
dense_1/bias/Adam/readIdentitydense_1/bias/Adam*
_output_shapes
:*
T0*
_class
loc:@dense_1/bias
 
5dense_1/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:*
_class
loc:@dense_1/bias

+dense_1/bias/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
щ
%dense_1/bias/Adam_1/Initializer/zerosFill5dense_1/bias/Adam_1/Initializer/zeros/shape_as_tensor+dense_1/bias/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_1/bias*
_output_shapes
:
 
dense_1/bias/Adam_1
VariableV2*
_class
loc:@dense_1/bias*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name 
Я
dense_1/bias/Adam_1/AssignAssigndense_1/bias/Adam_1%dense_1/bias/Adam_1/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias

dense_1/bias/Adam_1/readIdentitydense_1/bias/Adam_1*
_output_shapes
:*
T0*
_class
loc:@dense_1/bias
Љ
5dense_2/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"   
   *!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
:

+dense_2/kernel/Adam/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *!
_class
loc:@dense_2/kernel
я
%dense_2/kernel/Adam/Initializer/zerosFill5dense_2/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_2/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Њ
dense_2/kernel/Adam
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_2/kernel*
	container *
shape
:

е
dense_2/kernel/Adam/AssignAssigndense_2/kernel/Adam%dense_2/kernel/Adam/Initializer/zeros*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(

dense_2/kernel/Adam/readIdentitydense_2/kernel/Adam*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Ћ
7dense_2/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"   
   *!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
:

-dense_2/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
ѕ
'dense_2/kernel/Adam_1/Initializer/zerosFill7dense_2/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_2/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Ќ
dense_2/kernel/Adam_1
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_2/kernel*
	container *
shape
:

л
dense_2/kernel/Adam_1/AssignAssigndense_2/kernel/Adam_1'dense_2/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:


dense_2/kernel/Adam_1/readIdentitydense_2/kernel/Adam_1*
_output_shapes

:
*
T0*!
_class
loc:@dense_2/kernel

3dense_2/bias/Adam/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
:

)dense_2/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
у
#dense_2/bias/Adam/Initializer/zerosFill3dense_2/bias/Adam/Initializer/zeros/shape_as_tensor)dense_2/bias/Adam/Initializer/zeros/Const*
_output_shapes
:
*
T0*

index_type0*
_class
loc:@dense_2/bias

dense_2/bias/Adam
VariableV2*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense_2/bias*
	container *
shape:

Щ
dense_2/bias/Adam/AssignAssigndense_2/bias/Adam#dense_2/bias/Adam/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:

{
dense_2/bias/Adam/readIdentitydense_2/bias/Adam*
_output_shapes
:
*
T0*
_class
loc:@dense_2/bias
 
5dense_2/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:
*
_class
loc:@dense_2/bias

+dense_2/bias/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
щ
%dense_2/bias/Adam_1/Initializer/zerosFill5dense_2/bias/Adam_1/Initializer/zeros/shape_as_tensor+dense_2/bias/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_2/bias*
_output_shapes
:

 
dense_2/bias/Adam_1
VariableV2*
_class
loc:@dense_2/bias*
	container *
shape:
*
dtype0*
_output_shapes
:
*
shared_name 
Я
dense_2/bias/Adam_1/AssignAssigndense_2/bias/Adam_1%dense_2/bias/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:
*
use_locking(

dense_2/bias/Adam_1/readIdentitydense_2/bias/Adam_1*
T0*
_class
loc:@dense_2/bias*
_output_shapes
:

Љ
5dense_3/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
:

+dense_3/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
я
%dense_3/kernel/Adam/Initializer/zerosFill5dense_3/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_3/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

Њ
dense_3/kernel/Adam
VariableV2*
shared_name *!
_class
loc:@dense_3/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:

е
dense_3/kernel/Adam/AssignAssigndense_3/kernel/Adam%dense_3/kernel/Adam/Initializer/zeros*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(

dense_3/kernel/Adam/readIdentitydense_3/kernel/Adam*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

Ћ
7dense_3/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
:

-dense_3/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
ѕ
'dense_3/kernel/Adam_1/Initializer/zerosFill7dense_3/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_3/kernel/Adam_1/Initializer/zeros/Const*
_output_shapes

:
*
T0*

index_type0*!
_class
loc:@dense_3/kernel
Ќ
dense_3/kernel/Adam_1
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_3/kernel*
	container *
shape
:

л
dense_3/kernel/Adam_1/AssignAssigndense_3/kernel/Adam_1'dense_3/kernel/Adam_1/Initializer/zeros*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(

dense_3/kernel/Adam_1/readIdentitydense_3/kernel/Adam_1*
_output_shapes

:
*
T0*!
_class
loc:@dense_3/kernel

3dense_3/bias/Adam/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:

)dense_3/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
у
#dense_3/bias/Adam/Initializer/zerosFill3dense_3/bias/Adam/Initializer/zeros/shape_as_tensor)dense_3/bias/Adam/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_3/bias*
_output_shapes
:

dense_3/bias/Adam
VariableV2*
shared_name *
_class
loc:@dense_3/bias*
	container *
shape:*
dtype0*
_output_shapes
:
Щ
dense_3/bias/Adam/AssignAssigndense_3/bias/Adam#dense_3/bias/Adam/Initializer/zeros*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:*
use_locking(
{
dense_3/bias/Adam/readIdentitydense_3/bias/Adam*
T0*
_class
loc:@dense_3/bias*
_output_shapes
:
 
5dense_3/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:

+dense_3/bias/Adam_1/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *
_class
loc:@dense_3/bias
щ
%dense_3/bias/Adam_1/Initializer/zerosFill5dense_3/bias/Adam_1/Initializer/zeros/shape_as_tensor+dense_3/bias/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_3/bias*
_output_shapes
:
 
dense_3/bias/Adam_1
VariableV2*
shared_name *
_class
loc:@dense_3/bias*
	container *
shape:*
dtype0*
_output_shapes
:
Я
dense_3/bias/Adam_1/AssignAssigndense_3/bias/Adam_1%dense_3/bias/Adam_1/Initializer/zeros*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:*
use_locking(

dense_3/bias/Adam_1/readIdentitydense_3/bias/Adam_1*
T0*
_class
loc:@dense_3/bias*
_output_shapes
:
W
Adam/learning_rateConst*
valueB
 *
з#<*
dtype0*
_output_shapes
: 
O

Adam/beta1Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
O

Adam/beta2Const*
dtype0*
_output_shapes
: *
valueB
 *wО?
Q
Adam/epsilonConst*
valueB
 *wЬ+2*
dtype0*
_output_shapes
: 
ь
"Adam/update_dense/kernel/ApplyAdam	ApplyAdamdense/kerneldense/kernel/Adamdense/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon6gradients/dense/MatMul_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense/kernel*
use_nesterov( *
_output_shapes

:
*
use_locking( 
п
 Adam/update_dense/bias/ApplyAdam	ApplyAdam
dense/biasdense/bias/Adamdense/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon7gradients/dense/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense/bias*
use_nesterov( *
_output_shapes
:
*
use_locking( 
ј
$Adam/update_dense_1/kernel/ApplyAdam	ApplyAdamdense_1/kerneldense_1/kernel/Adamdense_1/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*!
_class
loc:@dense_1/kernel*
use_nesterov( *
_output_shapes

:

ы
"Adam/update_dense_1/bias/ApplyAdam	ApplyAdamdense_1/biasdense_1/bias/Adamdense_1/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_1/BiasAdd_grad/tuple/control_dependency_1*
use_nesterov( *
_output_shapes
:*
use_locking( *
T0*
_class
loc:@dense_1/bias
ј
$Adam/update_dense_2/kernel/ApplyAdam	ApplyAdamdense_2/kerneldense_2/kernel/Adamdense_2/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_2/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*!
_class
loc:@dense_2/kernel*
use_nesterov( *
_output_shapes

:

ы
"Adam/update_dense_2/bias/ApplyAdam	ApplyAdamdense_2/biasdense_2/bias/Adamdense_2/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_2/BiasAdd_grad/tuple/control_dependency_1*
use_nesterov( *
_output_shapes
:
*
use_locking( *
T0*
_class
loc:@dense_2/bias
ј
$Adam/update_dense_3/kernel/ApplyAdam	ApplyAdamdense_3/kerneldense_3/kernel/Adamdense_3/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_3/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*!
_class
loc:@dense_3/kernel*
use_nesterov( *
_output_shapes

:

ы
"Adam/update_dense_3/bias/ApplyAdam	ApplyAdamdense_3/biasdense_3/bias/Adamdense_3/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_3/BiasAdd_grad/tuple/control_dependency_1*
use_nesterov( *
_output_shapes
:*
use_locking( *
T0*
_class
loc:@dense_3/bias

Adam/mulMulbeta1_power/read
Adam/beta1#^Adam/update_dense/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam*
T0*
_class
loc:@dense/bias*
_output_shapes
: 

Adam/AssignAssignbeta1_powerAdam/mul*
use_locking( *
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: 


Adam/mul_1Mulbeta2_power/read
Adam/beta2#^Adam/update_dense/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam*
T0*
_class
loc:@dense/bias*
_output_shapes
: 

Adam/Assign_1Assignbeta2_power
Adam/mul_1*
validate_shape(*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@dense/bias
н
Adam/updateNoOp#^Adam/update_dense/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam^Adam/Assign^Adam/Assign_1
z

Adam/valueConst^Adam/update*
value	B	 R*
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
~
Adam	AssignAddglobal_step
Adam/value*
use_locking( *
T0	*
_class
loc:@global_step*
_output_shapes
: 
Q
CastCastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0
\
EqualEqualCastfifo_queue_DequeueUpTo:2*#
_output_shapes
:џџџџџџџџџ*
T0
S
ToFloatCastEqual*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0

0accuracy/total/Initializer/zeros/shape_as_tensorConst*
valueB *!
_class
loc:@accuracy/total*
dtype0*
_output_shapes
: 

&accuracy/total/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@accuracy/total*
dtype0*
_output_shapes
: 
и
 accuracy/total/Initializer/zerosFill0accuracy/total/Initializer/zeros/shape_as_tensor&accuracy/total/Initializer/zeros/Const*
_output_shapes
: *
T0*

index_type0*!
_class
loc:@accuracy/total

accuracy/total
VariableV2*
dtype0*
_output_shapes
: *
shared_name *!
_class
loc:@accuracy/total*
	container *
shape: 
О
accuracy/total/AssignAssignaccuracy/total accuracy/total/Initializer/zeros*
T0*!
_class
loc:@accuracy/total*
validate_shape(*
_output_shapes
: *
use_locking(
s
accuracy/total/readIdentityaccuracy/total*
T0*!
_class
loc:@accuracy/total*
_output_shapes
: 

0accuracy/count/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB *!
_class
loc:@accuracy/count

&accuracy/count/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@accuracy/count*
dtype0*
_output_shapes
: 
и
 accuracy/count/Initializer/zerosFill0accuracy/count/Initializer/zeros/shape_as_tensor&accuracy/count/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@accuracy/count*
_output_shapes
: 

accuracy/count
VariableV2*
shared_name *!
_class
loc:@accuracy/count*
	container *
shape: *
dtype0*
_output_shapes
: 
О
accuracy/count/AssignAssignaccuracy/count accuracy/count/Initializer/zeros*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*!
_class
loc:@accuracy/count
s
accuracy/count/readIdentityaccuracy/count*
_output_shapes
: *
T0*!
_class
loc:@accuracy/count
O
accuracy/SizeSizeToFloat*
_output_shapes
: *
T0*
out_type0
Y
accuracy/ToFloat_1Castaccuracy/Size*

SrcT0*
_output_shapes
: *

DstT0
X
accuracy/ConstConst*
valueB: *
dtype0*
_output_shapes
:
j
accuracy/SumSumToFloataccuracy/Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0

accuracy/AssignAdd	AssignAddaccuracy/totalaccuracy/Sum*
use_locking( *
T0*!
_class
loc:@accuracy/total*
_output_shapes
: 
І
accuracy/AssignAdd_1	AssignAddaccuracy/countaccuracy/ToFloat_1^ToFloat*
use_locking( *
T0*!
_class
loc:@accuracy/count*
_output_shapes
: 
f
accuracy/truedivRealDivaccuracy/total/readaccuracy/count/read*
_output_shapes
: *
T0
f
#accuracy/zeros_like/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
^
accuracy/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    

accuracy/zeros_likeFill#accuracy/zeros_like/shape_as_tensoraccuracy/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
f
accuracy/GreaterGreateraccuracy/count/readaccuracy/zeros_like*
_output_shapes
: *
T0
r
accuracy/valueSelectaccuracy/Greateraccuracy/truedivaccuracy/zeros_like*
T0*
_output_shapes
: 
h
accuracy/truediv_1RealDivaccuracy/AssignAddaccuracy/AssignAdd_1*
T0*
_output_shapes
: 
h
%accuracy/zeros_like_1/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
`
accuracy/zeros_like_1/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

accuracy/zeros_like_1Fill%accuracy/zeros_like_1/shape_as_tensoraccuracy/zeros_like_1/Const*
T0*

index_type0*
_output_shapes
: 
k
accuracy/Greater_1Greateraccuracy/AssignAdd_1accuracy/zeros_like_1*
_output_shapes
: *
T0
|
accuracy/update_opSelectaccuracy/Greater_1accuracy/truediv_1accuracy/zeros_like_1*
T0*
_output_shapes
: 
S
Cast_1CastArgMax*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0	
Z
subSubfifo_queue_DequeueUpTo:2Cast_1*
T0*#
_output_shapes
:џџџџџџџџџ
C
SquareSquaresub*
T0*#
_output_shapes
:џџџџџџџџџ
l
root_mean_squared_error/ToFloatCastSquare*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0
Д
?root_mean_squared_error/total/Initializer/zeros/shape_as_tensorConst*
valueB *0
_class&
$"loc:@root_mean_squared_error/total*
dtype0*
_output_shapes
: 
Ќ
5root_mean_squared_error/total/Initializer/zeros/ConstConst*
valueB
 *    *0
_class&
$"loc:@root_mean_squared_error/total*
dtype0*
_output_shapes
: 

/root_mean_squared_error/total/Initializer/zerosFill?root_mean_squared_error/total/Initializer/zeros/shape_as_tensor5root_mean_squared_error/total/Initializer/zeros/Const*
T0*

index_type0*0
_class&
$"loc:@root_mean_squared_error/total*
_output_shapes
: 
Г
root_mean_squared_error/total
VariableV2*0
_class&
$"loc:@root_mean_squared_error/total*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name 
њ
$root_mean_squared_error/total/AssignAssignroot_mean_squared_error/total/root_mean_squared_error/total/Initializer/zeros*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*0
_class&
$"loc:@root_mean_squared_error/total
 
"root_mean_squared_error/total/readIdentityroot_mean_squared_error/total*
T0*0
_class&
$"loc:@root_mean_squared_error/total*
_output_shapes
: 
Д
?root_mean_squared_error/count/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB *0
_class&
$"loc:@root_mean_squared_error/count
Ќ
5root_mean_squared_error/count/Initializer/zeros/ConstConst*
valueB
 *    *0
_class&
$"loc:@root_mean_squared_error/count*
dtype0*
_output_shapes
: 

/root_mean_squared_error/count/Initializer/zerosFill?root_mean_squared_error/count/Initializer/zeros/shape_as_tensor5root_mean_squared_error/count/Initializer/zeros/Const*
_output_shapes
: *
T0*

index_type0*0
_class&
$"loc:@root_mean_squared_error/count
Г
root_mean_squared_error/count
VariableV2*0
_class&
$"loc:@root_mean_squared_error/count*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name 
њ
$root_mean_squared_error/count/AssignAssignroot_mean_squared_error/count/root_mean_squared_error/count/Initializer/zeros*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*0
_class&
$"loc:@root_mean_squared_error/count
 
"root_mean_squared_error/count/readIdentityroot_mean_squared_error/count*
T0*0
_class&
$"loc:@root_mean_squared_error/count*
_output_shapes
: 
v
root_mean_squared_error/SizeSizeroot_mean_squared_error/ToFloat*
T0*
out_type0*
_output_shapes
: 
w
!root_mean_squared_error/ToFloat_1Castroot_mean_squared_error/Size*

SrcT0*
_output_shapes
: *

DstT0
g
root_mean_squared_error/ConstConst*
valueB: *
dtype0*
_output_shapes
:
 
root_mean_squared_error/SumSumroot_mean_squared_error/ToFloatroot_mean_squared_error/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 
а
!root_mean_squared_error/AssignAdd	AssignAddroot_mean_squared_error/totalroot_mean_squared_error/Sum*
use_locking( *
T0*0
_class&
$"loc:@root_mean_squared_error/total*
_output_shapes
: 
њ
#root_mean_squared_error/AssignAdd_1	AssignAddroot_mean_squared_error/count!root_mean_squared_error/ToFloat_1 ^root_mean_squared_error/ToFloat*
use_locking( *
T0*0
_class&
$"loc:@root_mean_squared_error/count*
_output_shapes
: 

root_mean_squared_error/truedivRealDiv"root_mean_squared_error/total/read"root_mean_squared_error/count/read*
_output_shapes
: *
T0
u
2root_mean_squared_error/zeros_like/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
m
(root_mean_squared_error/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    
Л
"root_mean_squared_error/zeros_likeFill2root_mean_squared_error/zeros_like/shape_as_tensor(root_mean_squared_error/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 

root_mean_squared_error/GreaterGreater"root_mean_squared_error/count/read"root_mean_squared_error/zeros_like*
T0*
_output_shapes
: 
Ў
root_mean_squared_error/valueSelectroot_mean_squared_error/Greaterroot_mean_squared_error/truediv"root_mean_squared_error/zeros_like*
T0*
_output_shapes
: 

!root_mean_squared_error/truediv_1RealDiv!root_mean_squared_error/AssignAdd#root_mean_squared_error/AssignAdd_1*
T0*
_output_shapes
: 
w
4root_mean_squared_error/zeros_like_1/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
o
*root_mean_squared_error/zeros_like_1/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
С
$root_mean_squared_error/zeros_like_1Fill4root_mean_squared_error/zeros_like_1/shape_as_tensor*root_mean_squared_error/zeros_like_1/Const*
_output_shapes
: *
T0*

index_type0

!root_mean_squared_error/Greater_1Greater#root_mean_squared_error/AssignAdd_1$root_mean_squared_error/zeros_like_1*
T0*
_output_shapes
: 
И
!root_mean_squared_error/update_opSelect!root_mean_squared_error/Greater_1!root_mean_squared_error/truediv_1$root_mean_squared_error/zeros_like_1*
T0*
_output_shapes
: 
L
SqrtSqrtroot_mean_squared_error/value*
T0*
_output_shapes
: 
R
Sqrt_1Sqrt!root_mean_squared_error/update_op*
T0*
_output_shapes
: 
e
Cast_2Castfifo_queue_DequeueUpTo:2*

SrcT0*#
_output_shapes
:џџџџџџџџџ*

DstT0
X
recall/CastCastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0

Z
recall/Cast_1CastCast_2*

SrcT0*#
_output_shapes
:џџџџџџџџџ*

DstT0

_
recall/true_positives/Equal/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

recall/true_positives/EqualEqualrecall/Cast_1recall/true_positives/Equal/y*
T0
*#
_output_shapes
:џџџџџџџџџ
a
recall/true_positives/Equal_1/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

recall/true_positives/Equal_1Equalrecall/Castrecall/true_positives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

 recall/true_positives/LogicalAnd
LogicalAndrecall/true_positives/Equalrecall/true_positives/Equal_1*#
_output_shapes
:џџџџџџџџџ
L
Drecall/true_positives/assert_type/statically_determined_correct_typeNoOp
А
=recall/true_positives/count/Initializer/zeros/shape_as_tensorConst*
valueB *.
_class$
" loc:@recall/true_positives/count*
dtype0*
_output_shapes
: 
Ј
3recall/true_positives/count/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *.
_class$
" loc:@recall/true_positives/count

-recall/true_positives/count/Initializer/zerosFill=recall/true_positives/count/Initializer/zeros/shape_as_tensor3recall/true_positives/count/Initializer/zeros/Const*
T0*

index_type0*.
_class$
" loc:@recall/true_positives/count*
_output_shapes
: 
Џ
recall/true_positives/count
VariableV2*
dtype0*
_output_shapes
: *
shared_name *.
_class$
" loc:@recall/true_positives/count*
	container *
shape: 
ђ
"recall/true_positives/count/AssignAssignrecall/true_positives/count-recall/true_positives/count/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@recall/true_positives/count*
validate_shape(*
_output_shapes
: 

 recall/true_positives/count/readIdentityrecall/true_positives/count*
T0*.
_class$
" loc:@recall/true_positives/count*
_output_shapes
: 

recall/true_positives/ToFloatCast recall/true_positives/LogicalAnd*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0
m
recall/true_positives/IdentityIdentity recall/true_positives/count/read*
T0*
_output_shapes
: 
e
recall/true_positives/ConstConst*
valueB: *
dtype0*
_output_shapes
:

recall/true_positives/SumSumrecall/true_positives/ToFloatrecall/true_positives/Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
Ш
recall/true_positives/AssignAdd	AssignAddrecall/true_positives/countrecall/true_positives/Sum*
use_locking( *
T0*.
_class$
" loc:@recall/true_positives/count*
_output_shapes
: 
`
recall/false_negatives/Equal/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

recall/false_negatives/EqualEqualrecall/Cast_1recall/false_negatives/Equal/y*#
_output_shapes
:џџџџџџџџџ*
T0

b
 recall/false_negatives/Equal_1/yConst*
value	B
 Z *
dtype0
*
_output_shapes
: 

recall/false_negatives/Equal_1Equalrecall/Cast recall/false_negatives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

!recall/false_negatives/LogicalAnd
LogicalAndrecall/false_negatives/Equalrecall/false_negatives/Equal_1*#
_output_shapes
:џџџџџџџџџ
M
Erecall/false_negatives/assert_type/statically_determined_correct_typeNoOp
В
>recall/false_negatives/count/Initializer/zeros/shape_as_tensorConst*
valueB */
_class%
#!loc:@recall/false_negatives/count*
dtype0*
_output_shapes
: 
Њ
4recall/false_negatives/count/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    */
_class%
#!loc:@recall/false_negatives/count

.recall/false_negatives/count/Initializer/zerosFill>recall/false_negatives/count/Initializer/zeros/shape_as_tensor4recall/false_negatives/count/Initializer/zeros/Const*
T0*

index_type0*/
_class%
#!loc:@recall/false_negatives/count*
_output_shapes
: 
Б
recall/false_negatives/count
VariableV2*
shared_name */
_class%
#!loc:@recall/false_negatives/count*
	container *
shape: *
dtype0*
_output_shapes
: 
і
#recall/false_negatives/count/AssignAssignrecall/false_negatives/count.recall/false_negatives/count/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@recall/false_negatives/count*
validate_shape(*
_output_shapes
: 

!recall/false_negatives/count/readIdentityrecall/false_negatives/count*
_output_shapes
: *
T0*/
_class%
#!loc:@recall/false_negatives/count

recall/false_negatives/ToFloatCast!recall/false_negatives/LogicalAnd*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0
o
recall/false_negatives/IdentityIdentity!recall/false_negatives/count/read*
T0*
_output_shapes
: 
f
recall/false_negatives/ConstConst*
valueB: *
dtype0*
_output_shapes
:

recall/false_negatives/SumSumrecall/false_negatives/ToFloatrecall/false_negatives/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
Ь
 recall/false_negatives/AssignAdd	AssignAddrecall/false_negatives/countrecall/false_negatives/Sum*
_output_shapes
: *
use_locking( *
T0*/
_class%
#!loc:@recall/false_negatives/count
s

recall/addAddrecall/true_positives/Identityrecall/false_negatives/Identity*
T0*
_output_shapes
: 
U
recall/Greater/yConst*
dtype0*
_output_shapes
: *
valueB
 *    
X
recall/GreaterGreater
recall/addrecall/Greater/y*
T0*
_output_shapes
: 
u
recall/add_1Addrecall/true_positives/Identityrecall/false_negatives/Identity*
_output_shapes
: *
T0
d

recall/divRealDivrecall/true_positives/Identityrecall/add_1*
T0*
_output_shapes
: 
S
recall/value/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
c
recall/valueSelectrecall/Greater
recall/divrecall/value/e*
T0*
_output_shapes
: 
w
recall/add_2Addrecall/true_positives/AssignAdd recall/false_negatives/AssignAdd*
T0*
_output_shapes
: 
W
recall/Greater_1/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
^
recall/Greater_1Greaterrecall/add_2recall/Greater_1/y*
T0*
_output_shapes
: 
w
recall/add_3Addrecall/true_positives/AssignAdd recall/false_negatives/AssignAdd*
_output_shapes
: *
T0
g
recall/div_1RealDivrecall/true_positives/AssignAddrecall/add_3*
_output_shapes
: *
T0
W
recall/update_op/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
o
recall/update_opSelectrecall/Greater_1recall/div_1recall/update_op/e*
T0*
_output_shapes
: 
e
Cast_3Castfifo_queue_DequeueUpTo:2*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0
[
precision/CastCastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0

]
precision/Cast_1CastCast_3*

SrcT0*#
_output_shapes
:џџџџџџџџџ*

DstT0

b
 precision/true_positives/Equal/yConst*
dtype0
*
_output_shapes
: *
value	B
 Z

precision/true_positives/EqualEqualprecision/Cast_1 precision/true_positives/Equal/y*
T0
*#
_output_shapes
:џџџџџџџџџ
d
"precision/true_positives/Equal_1/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

 precision/true_positives/Equal_1Equalprecision/Cast"precision/true_positives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

#precision/true_positives/LogicalAnd
LogicalAndprecision/true_positives/Equal precision/true_positives/Equal_1*#
_output_shapes
:џџџџџџџџџ
O
Gprecision/true_positives/assert_type/statically_determined_correct_typeNoOp
Ж
@precision/true_positives/count/Initializer/zeros/shape_as_tensorConst*
valueB *1
_class'
%#loc:@precision/true_positives/count*
dtype0*
_output_shapes
: 
Ў
6precision/true_positives/count/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *1
_class'
%#loc:@precision/true_positives/count

0precision/true_positives/count/Initializer/zerosFill@precision/true_positives/count/Initializer/zeros/shape_as_tensor6precision/true_positives/count/Initializer/zeros/Const*
T0*

index_type0*1
_class'
%#loc:@precision/true_positives/count*
_output_shapes
: 
Е
precision/true_positives/count
VariableV2*
shared_name *1
_class'
%#loc:@precision/true_positives/count*
	container *
shape: *
dtype0*
_output_shapes
: 
ў
%precision/true_positives/count/AssignAssignprecision/true_positives/count0precision/true_positives/count/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@precision/true_positives/count*
validate_shape(*
_output_shapes
: 
Ѓ
#precision/true_positives/count/readIdentityprecision/true_positives/count*
_output_shapes
: *
T0*1
_class'
%#loc:@precision/true_positives/count

 precision/true_positives/ToFloatCast#precision/true_positives/LogicalAnd*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0

s
!precision/true_positives/IdentityIdentity#precision/true_positives/count/read*
T0*
_output_shapes
: 
h
precision/true_positives/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
Ѓ
precision/true_positives/SumSum precision/true_positives/ToFloatprecision/true_positives/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
д
"precision/true_positives/AssignAdd	AssignAddprecision/true_positives/countprecision/true_positives/Sum*
use_locking( *
T0*1
_class'
%#loc:@precision/true_positives/count*
_output_shapes
: 
c
!precision/false_positives/Equal/yConst*
value	B
 Z *
dtype0
*
_output_shapes
: 

precision/false_positives/EqualEqualprecision/Cast_1!precision/false_positives/Equal/y*#
_output_shapes
:џџџџџџџџџ*
T0

e
#precision/false_positives/Equal_1/yConst*
dtype0
*
_output_shapes
: *
value	B
 Z

!precision/false_positives/Equal_1Equalprecision/Cast#precision/false_positives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

$precision/false_positives/LogicalAnd
LogicalAndprecision/false_positives/Equal!precision/false_positives/Equal_1*#
_output_shapes
:џџџџџџџџџ
P
Hprecision/false_positives/assert_type/statically_determined_correct_typeNoOp
И
Aprecision/false_positives/count/Initializer/zeros/shape_as_tensorConst*
valueB *2
_class(
&$loc:@precision/false_positives/count*
dtype0*
_output_shapes
: 
А
7precision/false_positives/count/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *2
_class(
&$loc:@precision/false_positives/count

1precision/false_positives/count/Initializer/zerosFillAprecision/false_positives/count/Initializer/zeros/shape_as_tensor7precision/false_positives/count/Initializer/zeros/Const*
T0*

index_type0*2
_class(
&$loc:@precision/false_positives/count*
_output_shapes
: 
З
precision/false_positives/count
VariableV2*
dtype0*
_output_shapes
: *
shared_name *2
_class(
&$loc:@precision/false_positives/count*
	container *
shape: 

&precision/false_positives/count/AssignAssignprecision/false_positives/count1precision/false_positives/count/Initializer/zeros*
T0*2
_class(
&$loc:@precision/false_positives/count*
validate_shape(*
_output_shapes
: *
use_locking(
І
$precision/false_positives/count/readIdentityprecision/false_positives/count*
T0*2
_class(
&$loc:@precision/false_positives/count*
_output_shapes
: 

!precision/false_positives/ToFloatCast$precision/false_positives/LogicalAnd*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0
u
"precision/false_positives/IdentityIdentity$precision/false_positives/count/read*
T0*
_output_shapes
: 
i
precision/false_positives/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
І
precision/false_positives/SumSum!precision/false_positives/ToFloatprecision/false_positives/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
и
#precision/false_positives/AssignAdd	AssignAddprecision/false_positives/countprecision/false_positives/Sum*
T0*2
_class(
&$loc:@precision/false_positives/count*
_output_shapes
: *
use_locking( 
|
precision/addAdd!precision/true_positives/Identity"precision/false_positives/Identity*
T0*
_output_shapes
: 
X
precision/Greater/yConst*
dtype0*
_output_shapes
: *
valueB
 *    
a
precision/GreaterGreaterprecision/addprecision/Greater/y*
T0*
_output_shapes
: 
~
precision/add_1Add!precision/true_positives/Identity"precision/false_positives/Identity*
T0*
_output_shapes
: 
m
precision/divRealDiv!precision/true_positives/Identityprecision/add_1*
T0*
_output_shapes
: 
V
precision/value/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
o
precision/valueSelectprecision/Greaterprecision/divprecision/value/e*
_output_shapes
: *
T0

precision/add_2Add"precision/true_positives/AssignAdd#precision/false_positives/AssignAdd*
_output_shapes
: *
T0
Z
precision/Greater_1/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
g
precision/Greater_1Greaterprecision/add_2precision/Greater_1/y*
_output_shapes
: *
T0

precision/add_3Add"precision/true_positives/AssignAdd#precision/false_positives/AssignAdd*
T0*
_output_shapes
: 
p
precision/div_1RealDiv"precision/true_positives/AssignAddprecision/add_3*
T0*
_output_shapes
: 
Z
precision/update_op/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
{
precision/update_opSelectprecision/Greater_1precision/div_1precision/update_op/e*
_output_shapes
: *
T0

,mean/total/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@mean/total*
dtype0*
_output_shapes
: 

"mean/total/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@mean/total*
dtype0*
_output_shapes
: 
Ш
mean/total/Initializer/zerosFill,mean/total/Initializer/zeros/shape_as_tensor"mean/total/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@mean/total*
_output_shapes
: 


mean/total
VariableV2*
dtype0*
_output_shapes
: *
shared_name *
_class
loc:@mean/total*
	container *
shape: 
Ў
mean/total/AssignAssign
mean/totalmean/total/Initializer/zeros*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@mean/total
g
mean/total/readIdentity
mean/total*
_output_shapes
: *
T0*
_class
loc:@mean/total

,mean/count/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@mean/count*
dtype0*
_output_shapes
: 

"mean/count/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@mean/count*
dtype0*
_output_shapes
: 
Ш
mean/count/Initializer/zerosFill,mean/count/Initializer/zeros/shape_as_tensor"mean/count/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@mean/count*
_output_shapes
: 


mean/count
VariableV2*
dtype0*
_output_shapes
: *
shared_name *
_class
loc:@mean/count*
	container *
shape: 
Ў
mean/count/AssignAssign
mean/countmean/count/Initializer/zeros*
T0*
_class
loc:@mean/count*
validate_shape(*
_output_shapes
: *
use_locking(
g
mean/count/readIdentity
mean/count*
T0*
_class
loc:@mean/count*
_output_shapes
: 
K
	mean/SizeConst*
value	B :*
dtype0*
_output_shapes
: 
Q
mean/ToFloat_1Cast	mean/Size*

SrcT0*
_output_shapes
: *

DstT0
M

mean/ConstConst*
valueB *
dtype0*
_output_shapes
: 
{
mean/SumSum sigmoid_cross_entropy_loss/value
mean/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 

mean/AssignAdd	AssignAdd
mean/totalmean/Sum*
use_locking( *
T0*
_class
loc:@mean/total*
_output_shapes
: 
Џ
mean/AssignAdd_1	AssignAdd
mean/countmean/ToFloat_1!^sigmoid_cross_entropy_loss/value*
use_locking( *
T0*
_class
loc:@mean/count*
_output_shapes
: 
Z
mean/truedivRealDivmean/total/readmean/count/read*
T0*
_output_shapes
: 
b
mean/zeros_like/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB 
Z
mean/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    

mean/zeros_likeFillmean/zeros_like/shape_as_tensormean/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
Z
mean/GreaterGreatermean/count/readmean/zeros_like*
T0*
_output_shapes
: 
b

mean/valueSelectmean/Greatermean/truedivmean/zeros_like*
T0*
_output_shapes
: 
\
mean/truediv_1RealDivmean/AssignAddmean/AssignAdd_1*
T0*
_output_shapes
: 
d
!mean/zeros_like_1/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
\
mean/zeros_like_1/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

mean/zeros_like_1Fill!mean/zeros_like_1/shape_as_tensormean/zeros_like_1/Const*
T0*

index_type0*
_output_shapes
: 
_
mean/Greater_1Greatermean/AssignAdd_1mean/zeros_like_1*
T0*
_output_shapes
: 
l
mean/update_opSelectmean/Greater_1mean/truediv_1mean/zeros_like_1*
T0*
_output_shapes
: 
j

group_depsNoOp^accuracy/update_op^mean/update_op^precision/update_op^Sqrt_1^recall/update_op

+eval_step/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@eval_step*
dtype0*
_output_shapes
: 

!eval_step/Initializer/zeros/ConstConst*
dtype0	*
_output_shapes
: *
value	B	 R *
_class
loc:@eval_step
Ф
eval_step/Initializer/zerosFill+eval_step/Initializer/zeros/shape_as_tensor!eval_step/Initializer/zeros/Const*
T0	*

index_type0*
_class
loc:@eval_step*
_output_shapes
: 

	eval_step
VariableV2*
_class
loc:@eval_step*
	container *
shape: *
dtype0	*
_output_shapes
: *
shared_name 
Њ
eval_step/AssignAssign	eval_stepeval_step/Initializer/zeros*
use_locking(*
T0	*
_class
loc:@eval_step*
validate_shape(*
_output_shapes
: 
d
eval_step/readIdentity	eval_step*
T0	*
_class
loc:@eval_step*
_output_shapes
: 
Q
AssignAdd/valueConst*
value	B	 R*
dtype0	*
_output_shapes
: 

	AssignAdd	AssignAdd	eval_stepAssignAdd/value*
_output_shapes
: *
use_locking(*
T0	*
_class
loc:@eval_step
U
readIdentity	eval_step^group_deps
^AssignAdd*
T0	*
_output_shapes
: 
;
IdentityIdentityread*
T0	*
_output_shapes
: 
Ч
initNoOp^global_step/Assign^dense/kernel/Assign^dense/bias/Assign^dense_1/kernel/Assign^dense_1/bias/Assign^dense_2/kernel/Assign^dense_2/bias/Assign^dense_3/kernel/Assign^dense_3/bias/Assign^beta1_power/Assign^beta2_power/Assign^dense/kernel/Adam/Assign^dense/kernel/Adam_1/Assign^dense/bias/Adam/Assign^dense/bias/Adam_1/Assign^dense_1/kernel/Adam/Assign^dense_1/kernel/Adam_1/Assign^dense_1/bias/Adam/Assign^dense_1/bias/Adam_1/Assign^dense_2/kernel/Adam/Assign^dense_2/kernel/Adam_1/Assign^dense_2/bias/Adam/Assign^dense_2/bias/Adam_1/Assign^dense_3/kernel/Adam/Assign^dense_3/kernel/Adam_1/Assign^dense_3/bias/Adam/Assign^dense_3/bias/Adam_1/Assign

init_1NoOp
$
group_deps_1NoOp^init^init_1

4report_uninitialized_variables/IsVariableInitializedIsVariableInitializedglobal_step*
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_1IsVariableInitializeddense/kernel*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 

6report_uninitialized_variables/IsVariableInitialized_2IsVariableInitialized
dense/bias*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ї
6report_uninitialized_variables/IsVariableInitialized_3IsVariableInitializeddense_1/kernel*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_4IsVariableInitializeddense_1/bias*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
Ї
6report_uninitialized_variables/IsVariableInitialized_5IsVariableInitializeddense_2/kernel*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_6IsVariableInitializeddense_2/bias*
dtype0*
_output_shapes
: *
_class
loc:@dense_2/bias
Ї
6report_uninitialized_variables/IsVariableInitialized_7IsVariableInitializeddense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_8IsVariableInitializeddense_3/bias*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
 
6report_uninitialized_variables/IsVariableInitialized_9IsVariableInitializedbeta1_power*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ё
7report_uninitialized_variables/IsVariableInitialized_10IsVariableInitializedbeta2_power*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Љ
7report_uninitialized_variables/IsVariableInitialized_11IsVariableInitializeddense/kernel/Adam*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
Ћ
7report_uninitialized_variables/IsVariableInitialized_12IsVariableInitializeddense/kernel/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense/kernel
Ѕ
7report_uninitialized_variables/IsVariableInitialized_13IsVariableInitializeddense/bias/Adam*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ї
7report_uninitialized_variables/IsVariableInitialized_14IsVariableInitializeddense/bias/Adam_1*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
­
7report_uninitialized_variables/IsVariableInitialized_15IsVariableInitializeddense_1/kernel/Adam*
dtype0*
_output_shapes
: *!
_class
loc:@dense_1/kernel
Џ
7report_uninitialized_variables/IsVariableInitialized_16IsVariableInitializeddense_1/kernel/Adam_1*
dtype0*
_output_shapes
: *!
_class
loc:@dense_1/kernel
Љ
7report_uninitialized_variables/IsVariableInitialized_17IsVariableInitializeddense_1/bias/Adam*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
Ћ
7report_uninitialized_variables/IsVariableInitialized_18IsVariableInitializeddense_1/bias/Adam_1*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
­
7report_uninitialized_variables/IsVariableInitialized_19IsVariableInitializeddense_2/kernel/Adam*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Џ
7report_uninitialized_variables/IsVariableInitialized_20IsVariableInitializeddense_2/kernel/Adam_1*
dtype0*
_output_shapes
: *!
_class
loc:@dense_2/kernel
Љ
7report_uninitialized_variables/IsVariableInitialized_21IsVariableInitializeddense_2/bias/Adam*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
Ћ
7report_uninitialized_variables/IsVariableInitialized_22IsVariableInitializeddense_2/bias/Adam_1*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
­
7report_uninitialized_variables/IsVariableInitialized_23IsVariableInitializeddense_3/kernel/Adam*
dtype0*
_output_shapes
: *!
_class
loc:@dense_3/kernel
Џ
7report_uninitialized_variables/IsVariableInitialized_24IsVariableInitializeddense_3/kernel/Adam_1*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Љ
7report_uninitialized_variables/IsVariableInitialized_25IsVariableInitializeddense_3/bias/Adam*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
Ћ
7report_uninitialized_variables/IsVariableInitialized_26IsVariableInitializeddense_3/bias/Adam_1*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
Ј
7report_uninitialized_variables/IsVariableInitialized_27IsVariableInitializedaccuracy/total*!
_class
loc:@accuracy/total*
dtype0*
_output_shapes
: 
Ј
7report_uninitialized_variables/IsVariableInitialized_28IsVariableInitializedaccuracy/count*!
_class
loc:@accuracy/count*
dtype0*
_output_shapes
: 
Ц
7report_uninitialized_variables/IsVariableInitialized_29IsVariableInitializedroot_mean_squared_error/total*0
_class&
$"loc:@root_mean_squared_error/total*
dtype0*
_output_shapes
: 
Ц
7report_uninitialized_variables/IsVariableInitialized_30IsVariableInitializedroot_mean_squared_error/count*
dtype0*
_output_shapes
: *0
_class&
$"loc:@root_mean_squared_error/count
Т
7report_uninitialized_variables/IsVariableInitialized_31IsVariableInitializedrecall/true_positives/count*
dtype0*
_output_shapes
: *.
_class$
" loc:@recall/true_positives/count
Ф
7report_uninitialized_variables/IsVariableInitialized_32IsVariableInitializedrecall/false_negatives/count*/
_class%
#!loc:@recall/false_negatives/count*
dtype0*
_output_shapes
: 
Ш
7report_uninitialized_variables/IsVariableInitialized_33IsVariableInitializedprecision/true_positives/count*1
_class'
%#loc:@precision/true_positives/count*
dtype0*
_output_shapes
: 
Ъ
7report_uninitialized_variables/IsVariableInitialized_34IsVariableInitializedprecision/false_positives/count*2
_class(
&$loc:@precision/false_positives/count*
dtype0*
_output_shapes
: 
 
7report_uninitialized_variables/IsVariableInitialized_35IsVariableInitialized
mean/total*
_class
loc:@mean/total*
dtype0*
_output_shapes
: 
 
7report_uninitialized_variables/IsVariableInitialized_36IsVariableInitialized
mean/count*
_class
loc:@mean/count*
dtype0*
_output_shapes
: 

7report_uninitialized_variables/IsVariableInitialized_37IsVariableInitialized	eval_step*
_class
loc:@eval_step*
dtype0	*
_output_shapes
: 
п
$report_uninitialized_variables/stackPack4report_uninitialized_variables/IsVariableInitialized6report_uninitialized_variables/IsVariableInitialized_16report_uninitialized_variables/IsVariableInitialized_26report_uninitialized_variables/IsVariableInitialized_36report_uninitialized_variables/IsVariableInitialized_46report_uninitialized_variables/IsVariableInitialized_56report_uninitialized_variables/IsVariableInitialized_66report_uninitialized_variables/IsVariableInitialized_76report_uninitialized_variables/IsVariableInitialized_86report_uninitialized_variables/IsVariableInitialized_97report_uninitialized_variables/IsVariableInitialized_107report_uninitialized_variables/IsVariableInitialized_117report_uninitialized_variables/IsVariableInitialized_127report_uninitialized_variables/IsVariableInitialized_137report_uninitialized_variables/IsVariableInitialized_147report_uninitialized_variables/IsVariableInitialized_157report_uninitialized_variables/IsVariableInitialized_167report_uninitialized_variables/IsVariableInitialized_177report_uninitialized_variables/IsVariableInitialized_187report_uninitialized_variables/IsVariableInitialized_197report_uninitialized_variables/IsVariableInitialized_207report_uninitialized_variables/IsVariableInitialized_217report_uninitialized_variables/IsVariableInitialized_227report_uninitialized_variables/IsVariableInitialized_237report_uninitialized_variables/IsVariableInitialized_247report_uninitialized_variables/IsVariableInitialized_257report_uninitialized_variables/IsVariableInitialized_267report_uninitialized_variables/IsVariableInitialized_277report_uninitialized_variables/IsVariableInitialized_287report_uninitialized_variables/IsVariableInitialized_297report_uninitialized_variables/IsVariableInitialized_307report_uninitialized_variables/IsVariableInitialized_317report_uninitialized_variables/IsVariableInitialized_327report_uninitialized_variables/IsVariableInitialized_337report_uninitialized_variables/IsVariableInitialized_347report_uninitialized_variables/IsVariableInitialized_357report_uninitialized_variables/IsVariableInitialized_367report_uninitialized_variables/IsVariableInitialized_37"/device:CPU:0*
T0
*

axis *
N&*
_output_shapes
:&

)report_uninitialized_variables/LogicalNot
LogicalNot$report_uninitialized_variables/stack"/device:CPU:0*
_output_shapes
:&
н
$report_uninitialized_variables/ConstConst"/device:CPU:0*
dtype0*
_output_shapes
:&*ѕ
valueыBш&Bglobal_stepBdense/kernelB
dense/biasBdense_1/kernelBdense_1/biasBdense_2/kernelBdense_2/biasBdense_3/kernelBdense_3/biasBbeta1_powerBbeta2_powerBdense/kernel/AdamBdense/kernel/Adam_1Bdense/bias/AdamBdense/bias/Adam_1Bdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_3/kernel/AdamBdense_3/kernel/Adam_1Bdense_3/bias/AdamBdense_3/bias/Adam_1Baccuracy/totalBaccuracy/countBroot_mean_squared_error/totalBroot_mean_squared_error/countBrecall/true_positives/countBrecall/false_negatives/countBprecision/true_positives/countBprecision/false_positives/countB
mean/totalB
mean/countB	eval_step

1report_uninitialized_variables/boolean_mask/ShapeConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:&

?report_uninitialized_variables/boolean_mask/strided_slice/stackConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Areport_uninitialized_variables/boolean_mask/strided_slice/stack_1Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Areport_uninitialized_variables/boolean_mask/strided_slice/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
ш
9report_uninitialized_variables/boolean_mask/strided_sliceStridedSlice1report_uninitialized_variables/boolean_mask/Shape?report_uninitialized_variables/boolean_mask/strided_slice/stackAreport_uninitialized_variables/boolean_mask/strided_slice/stack_1Areport_uninitialized_variables/boolean_mask/strided_slice/stack_2"/device:CPU:0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
:*
T0*
Index0

Breport_uninitialized_variables/boolean_mask/Prod/reduction_indicesConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

0report_uninitialized_variables/boolean_mask/ProdProd9report_uninitialized_variables/boolean_mask/strided_sliceBreport_uninitialized_variables/boolean_mask/Prod/reduction_indices"/device:CPU:0*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 

3report_uninitialized_variables/boolean_mask/Shape_1Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:&

Areport_uninitialized_variables/boolean_mask/strided_slice_1/stackConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_1Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_2Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:
№
;report_uninitialized_variables/boolean_mask/strided_slice_1StridedSlice3report_uninitialized_variables/boolean_mask/Shape_1Areport_uninitialized_variables/boolean_mask/strided_slice_1/stackCreport_uninitialized_variables/boolean_mask/strided_slice_1/stack_1Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_2"/device:CPU:0*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: 

3report_uninitialized_variables/boolean_mask/Shape_2Const"/device:CPU:0*
valueB:&*
dtype0*
_output_shapes
:

Areport_uninitialized_variables/boolean_mask/strided_slice_2/stackConst"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_1Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_2Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:
№
;report_uninitialized_variables/boolean_mask/strided_slice_2StridedSlice3report_uninitialized_variables/boolean_mask/Shape_2Areport_uninitialized_variables/boolean_mask/strided_slice_2/stackCreport_uninitialized_variables/boolean_mask/strided_slice_2/stack_1Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_2"/device:CPU:0*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
: *
Index0*
T0*
shrink_axis_mask 
О
;report_uninitialized_variables/boolean_mask/concat/values_1Pack0report_uninitialized_variables/boolean_mask/Prod"/device:CPU:0*
T0*

axis *
N*
_output_shapes
:

7report_uninitialized_variables/boolean_mask/concat/axisConst"/device:CPU:0*
dtype0*
_output_shapes
: *
value	B : 
ї
2report_uninitialized_variables/boolean_mask/concatConcatV2;report_uninitialized_variables/boolean_mask/strided_slice_1;report_uninitialized_variables/boolean_mask/concat/values_1;report_uninitialized_variables/boolean_mask/strided_slice_27report_uninitialized_variables/boolean_mask/concat/axis"/device:CPU:0*

Tidx0*
T0*
N*
_output_shapes
:
к
3report_uninitialized_variables/boolean_mask/ReshapeReshape$report_uninitialized_variables/Const2report_uninitialized_variables/boolean_mask/concat"/device:CPU:0*
_output_shapes
:&*
T0*
Tshape0

;report_uninitialized_variables/boolean_mask/Reshape_1/shapeConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:
џџџџџџџџџ
ъ
5report_uninitialized_variables/boolean_mask/Reshape_1Reshape)report_uninitialized_variables/LogicalNot;report_uninitialized_variables/boolean_mask/Reshape_1/shape"/device:CPU:0*
T0
*
Tshape0*
_output_shapes
:&
В
1report_uninitialized_variables/boolean_mask/WhereWhere5report_uninitialized_variables/boolean_mask/Reshape_1"/device:CPU:0*
T0
*'
_output_shapes
:џџџџџџџџџ
Х
3report_uninitialized_variables/boolean_mask/SqueezeSqueeze1report_uninitialized_variables/boolean_mask/Where"/device:CPU:0*
T0	*#
_output_shapes
:џџџџџџџџџ*
squeeze_dims


2report_uninitialized_variables/boolean_mask/GatherGather3report_uninitialized_variables/boolean_mask/Reshape3report_uninitialized_variables/boolean_mask/Squeeze"/device:CPU:0*
Tparams0*
validate_indices(*#
_output_shapes
:џџџџџџџџџ*
Tindices0	
v
$report_uninitialized_resources/ConstConst"/device:CPU:0*
valueB *
dtype0*
_output_shapes
: 
M
concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
М
concatConcatV22report_uninitialized_variables/boolean_mask/Gather$report_uninitialized_resources/Constconcat/axis*
T0*
N*#
_output_shapes
:џџџџџџџџџ*

Tidx0
Ё
6report_uninitialized_variables_1/IsVariableInitializedIsVariableInitializedglobal_step*
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_1IsVariableInitializeddense/kernel*
dtype0*
_output_shapes
: *
_class
loc:@dense/kernel
Ё
8report_uninitialized_variables_1/IsVariableInitialized_2IsVariableInitialized
dense/bias*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Љ
8report_uninitialized_variables_1/IsVariableInitialized_3IsVariableInitializeddense_1/kernel*
dtype0*
_output_shapes
: *!
_class
loc:@dense_1/kernel
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_4IsVariableInitializeddense_1/bias*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
Љ
8report_uninitialized_variables_1/IsVariableInitialized_5IsVariableInitializeddense_2/kernel*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_6IsVariableInitializeddense_2/bias*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
Љ
8report_uninitialized_variables_1/IsVariableInitialized_7IsVariableInitializeddense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_8IsVariableInitializeddense_3/bias*
dtype0*
_output_shapes
: *
_class
loc:@dense_3/bias
Ђ
8report_uninitialized_variables_1/IsVariableInitialized_9IsVariableInitializedbeta1_power*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ѓ
9report_uninitialized_variables_1/IsVariableInitialized_10IsVariableInitializedbeta2_power*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_11IsVariableInitializeddense/kernel/Adam*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
­
9report_uninitialized_variables_1/IsVariableInitialized_12IsVariableInitializeddense/kernel/Adam_1*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
Ї
9report_uninitialized_variables_1/IsVariableInitialized_13IsVariableInitializeddense/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Љ
9report_uninitialized_variables_1/IsVariableInitialized_14IsVariableInitializeddense/bias/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Џ
9report_uninitialized_variables_1/IsVariableInitialized_15IsVariableInitializeddense_1/kernel/Adam*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Б
9report_uninitialized_variables_1/IsVariableInitialized_16IsVariableInitializeddense_1/kernel/Adam_1*
dtype0*
_output_shapes
: *!
_class
loc:@dense_1/kernel
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_17IsVariableInitializeddense_1/bias/Adam*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
­
9report_uninitialized_variables_1/IsVariableInitialized_18IsVariableInitializeddense_1/bias/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense_1/bias
Џ
9report_uninitialized_variables_1/IsVariableInitialized_19IsVariableInitializeddense_2/kernel/Adam*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Б
9report_uninitialized_variables_1/IsVariableInitialized_20IsVariableInitializeddense_2/kernel/Adam_1*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_21IsVariableInitializeddense_2/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense_2/bias
­
9report_uninitialized_variables_1/IsVariableInitialized_22IsVariableInitializeddense_2/bias/Adam_1*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
Џ
9report_uninitialized_variables_1/IsVariableInitialized_23IsVariableInitializeddense_3/kernel/Adam*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Б
9report_uninitialized_variables_1/IsVariableInitialized_24IsVariableInitializeddense_3/kernel/Adam_1*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_25IsVariableInitializeddense_3/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense_3/bias
­
9report_uninitialized_variables_1/IsVariableInitialized_26IsVariableInitializeddense_3/bias/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense_3/bias
Є
&report_uninitialized_variables_1/stackPack6report_uninitialized_variables_1/IsVariableInitialized8report_uninitialized_variables_1/IsVariableInitialized_18report_uninitialized_variables_1/IsVariableInitialized_28report_uninitialized_variables_1/IsVariableInitialized_38report_uninitialized_variables_1/IsVariableInitialized_48report_uninitialized_variables_1/IsVariableInitialized_58report_uninitialized_variables_1/IsVariableInitialized_68report_uninitialized_variables_1/IsVariableInitialized_78report_uninitialized_variables_1/IsVariableInitialized_88report_uninitialized_variables_1/IsVariableInitialized_99report_uninitialized_variables_1/IsVariableInitialized_109report_uninitialized_variables_1/IsVariableInitialized_119report_uninitialized_variables_1/IsVariableInitialized_129report_uninitialized_variables_1/IsVariableInitialized_139report_uninitialized_variables_1/IsVariableInitialized_149report_uninitialized_variables_1/IsVariableInitialized_159report_uninitialized_variables_1/IsVariableInitialized_169report_uninitialized_variables_1/IsVariableInitialized_179report_uninitialized_variables_1/IsVariableInitialized_189report_uninitialized_variables_1/IsVariableInitialized_199report_uninitialized_variables_1/IsVariableInitialized_209report_uninitialized_variables_1/IsVariableInitialized_219report_uninitialized_variables_1/IsVariableInitialized_229report_uninitialized_variables_1/IsVariableInitialized_239report_uninitialized_variables_1/IsVariableInitialized_249report_uninitialized_variables_1/IsVariableInitialized_259report_uninitialized_variables_1/IsVariableInitialized_26"/device:CPU:0*
T0
*

axis *
N*
_output_shapes
:

+report_uninitialized_variables_1/LogicalNot
LogicalNot&report_uninitialized_variables_1/stack"/device:CPU:0*
_output_shapes
:
т
&report_uninitialized_variables_1/ConstConst"/device:CPU:0*ј
valueюBыBglobal_stepBdense/kernelB
dense/biasBdense_1/kernelBdense_1/biasBdense_2/kernelBdense_2/biasBdense_3/kernelBdense_3/biasBbeta1_powerBbeta2_powerBdense/kernel/AdamBdense/kernel/Adam_1Bdense/bias/AdamBdense/bias/Adam_1Bdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_3/kernel/AdamBdense_3/kernel/Adam_1Bdense_3/bias/AdamBdense_3/bias/Adam_1*
dtype0*
_output_shapes
:

3report_uninitialized_variables_1/boolean_mask/ShapeConst"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Areport_uninitialized_variables_1/boolean_mask/strided_slice/stackConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_1Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
ђ
;report_uninitialized_variables_1/boolean_mask/strided_sliceStridedSlice3report_uninitialized_variables_1/boolean_mask/ShapeAreport_uninitialized_variables_1/boolean_mask/strided_slice/stackCreport_uninitialized_variables_1/boolean_mask/strided_slice/stack_1Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_2"/device:CPU:0*
Index0*
T0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
:

Dreport_uninitialized_variables_1/boolean_mask/Prod/reduction_indicesConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

2report_uninitialized_variables_1/boolean_mask/ProdProd;report_uninitialized_variables_1/boolean_mask/strided_sliceDreport_uninitialized_variables_1/boolean_mask/Prod/reduction_indices"/device:CPU:0*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 

5report_uninitialized_variables_1/boolean_mask/Shape_1Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice_1/stackConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_1Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_2Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:
њ
=report_uninitialized_variables_1/boolean_mask/strided_slice_1StridedSlice5report_uninitialized_variables_1/boolean_mask/Shape_1Creport_uninitialized_variables_1/boolean_mask/strided_slice_1/stackEreport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_1Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_2"/device:CPU:0*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask 

5report_uninitialized_variables_1/boolean_mask/Shape_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice_2/stackConst"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_1Const"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
њ
=report_uninitialized_variables_1/boolean_mask/strided_slice_2StridedSlice5report_uninitialized_variables_1/boolean_mask/Shape_2Creport_uninitialized_variables_1/boolean_mask/strided_slice_2/stackEreport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_1Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_2"/device:CPU:0*
Index0*
T0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
_output_shapes
: 
Т
=report_uninitialized_variables_1/boolean_mask/concat/values_1Pack2report_uninitialized_variables_1/boolean_mask/Prod"/device:CPU:0*
T0*

axis *
N*
_output_shapes
:

9report_uninitialized_variables_1/boolean_mask/concat/axisConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 

4report_uninitialized_variables_1/boolean_mask/concatConcatV2=report_uninitialized_variables_1/boolean_mask/strided_slice_1=report_uninitialized_variables_1/boolean_mask/concat/values_1=report_uninitialized_variables_1/boolean_mask/strided_slice_29report_uninitialized_variables_1/boolean_mask/concat/axis"/device:CPU:0*
T0*
N*
_output_shapes
:*

Tidx0
р
5report_uninitialized_variables_1/boolean_mask/ReshapeReshape&report_uninitialized_variables_1/Const4report_uninitialized_variables_1/boolean_mask/concat"/device:CPU:0*
_output_shapes
:*
T0*
Tshape0

=report_uninitialized_variables_1/boolean_mask/Reshape_1/shapeConst"/device:CPU:0*
valueB:
џџџџџџџџџ*
dtype0*
_output_shapes
:
№
7report_uninitialized_variables_1/boolean_mask/Reshape_1Reshape+report_uninitialized_variables_1/LogicalNot=report_uninitialized_variables_1/boolean_mask/Reshape_1/shape"/device:CPU:0*
T0
*
Tshape0*
_output_shapes
:
Ж
3report_uninitialized_variables_1/boolean_mask/WhereWhere7report_uninitialized_variables_1/boolean_mask/Reshape_1"/device:CPU:0*
T0
*'
_output_shapes
:џџџџџџџџџ
Щ
5report_uninitialized_variables_1/boolean_mask/SqueezeSqueeze3report_uninitialized_variables_1/boolean_mask/Where"/device:CPU:0*
T0	*#
_output_shapes
:џџџџџџџџџ*
squeeze_dims


4report_uninitialized_variables_1/boolean_mask/GatherGather5report_uninitialized_variables_1/boolean_mask/Reshape5report_uninitialized_variables_1/boolean_mask/Squeeze"/device:CPU:0*
Tindices0	*
Tparams0*
validate_indices(*#
_output_shapes
:џџџџџџџџџ
у
init_2NoOp^accuracy/total/Assign^accuracy/count/Assign%^root_mean_squared_error/total/Assign%^root_mean_squared_error/count/Assign#^recall/true_positives/count/Assign$^recall/false_negatives/count/Assign&^precision/true_positives/count/Assign'^precision/false_positives/count/Assign^mean/total/Assign^mean/count/Assign^eval_step/Assign

init_all_tablesNoOp

init_3NoOp
8
group_deps_2NoOp^init_2^init_all_tables^init_3

Merge/MergeSummaryMergeSummaryHenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full*
N*
_output_shapes
: 
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 

save/StringJoin/inputs_1Const*<
value3B1 B+_temp_b0772121846c4dffa1ef6f93bcd62529/part*
dtype0*
_output_shapes
: 
u
save/StringJoin
StringJoin
save/Constsave/StringJoin/inputs_1*
N*
_output_shapes
: *
	separator 
Q
save/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
k
save/ShardedFilename/shardConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 

save/ShardedFilenameShardedFilenamesave/StringJoinsave/ShardedFilename/shardsave/num_shards"/device:CPU:0*
_output_shapes
: 
д
save/SaveV2/tensor_namesConst"/device:CPU:0*ј
valueюBыBbeta1_powerBbeta2_powerB
dense/biasBdense/bias/AdamBdense/bias/Adam_1Bdense/kernelBdense/kernel/AdamBdense/kernel/Adam_1Bdense_1/biasBdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_1/kernelBdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_2/biasBdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_2/kernelBdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_3/biasBdense_3/bias/AdamBdense_3/bias/Adam_1Bdense_3/kernelBdense_3/kernel/AdamBdense_3/kernel/Adam_1Bglobal_step*
dtype0*
_output_shapes
:
Ј
save/SaveV2/shape_and_slicesConst"/device:CPU:0*I
value@B>B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:

save/SaveV2SaveV2save/ShardedFilenamesave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesbeta1_powerbeta2_power
dense/biasdense/bias/Adamdense/bias/Adam_1dense/kerneldense/kernel/Adamdense/kernel/Adam_1dense_1/biasdense_1/bias/Adamdense_1/bias/Adam_1dense_1/kerneldense_1/kernel/Adamdense_1/kernel/Adam_1dense_2/biasdense_2/bias/Adamdense_2/bias/Adam_1dense_2/kerneldense_2/kernel/Adamdense_2/kernel/Adam_1dense_3/biasdense_3/bias/Adamdense_3/bias/Adam_1dense_3/kerneldense_3/kernel/Adamdense_3/kernel/Adam_1global_step"/device:CPU:0*)
dtypes
2	
 
save/control_dependencyIdentitysave/ShardedFilename^save/SaveV2"/device:CPU:0*
T0*'
_class
loc:@save/ShardedFilename*
_output_shapes
: 
Ќ
+save/MergeV2Checkpoints/checkpoint_prefixesPacksave/ShardedFilename^save/control_dependency"/device:CPU:0*
T0*

axis *
N*
_output_shapes
:

save/MergeV2CheckpointsMergeV2Checkpoints+save/MergeV2Checkpoints/checkpoint_prefixes
save/Const"/device:CPU:0*
delete_old_dirs(

save/IdentityIdentity
save/Const^save/control_dependency^save/MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 
з
save/RestoreV2/tensor_namesConst"/device:CPU:0*
dtype0*
_output_shapes
:*ј
valueюBыBbeta1_powerBbeta2_powerB
dense/biasBdense/bias/AdamBdense/bias/Adam_1Bdense/kernelBdense/kernel/AdamBdense/kernel/Adam_1Bdense_1/biasBdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_1/kernelBdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_2/biasBdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_2/kernelBdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_3/biasBdense_3/bias/AdamBdense_3/bias/Adam_1Bdense_3/kernelBdense_3/kernel/AdamBdense_3/kernel/Adam_1Bglobal_step
Ћ
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
dtype0*
_output_shapes
:*I
value@B>B B B B B B B B B B B B B B B B B B B B B B B B B B B 
Ђ
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*)
dtypes
2	*
_output_shapesn
l:::::::::::::::::::::::::::

save/AssignAssignbeta1_powersave/RestoreV2*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: 

save/Assign_1Assignbeta2_powersave/RestoreV2:1*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@dense/bias
Ђ
save/Assign_2Assign
dense/biassave/RestoreV2:2*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

Ї
save/Assign_3Assigndense/bias/Adamsave/RestoreV2:3*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

Љ
save/Assign_4Assigndense/bias/Adam_1save/RestoreV2:4*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
Њ
save/Assign_5Assigndense/kernelsave/RestoreV2:5*
use_locking(*
T0*
_class
loc:@dense/kernel*
validate_shape(*
_output_shapes

:

Џ
save/Assign_6Assigndense/kernel/Adamsave/RestoreV2:6*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*
_class
loc:@dense/kernel
Б
save/Assign_7Assigndense/kernel/Adam_1save/RestoreV2:7*
use_locking(*
T0*
_class
loc:@dense/kernel*
validate_shape(*
_output_shapes

:

І
save/Assign_8Assigndense_1/biassave/RestoreV2:8*
T0*
_class
loc:@dense_1/bias*
validate_shape(*
_output_shapes
:*
use_locking(
Ћ
save/Assign_9Assigndense_1/bias/Adamsave/RestoreV2:9*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias
Џ
save/Assign_10Assigndense_1/bias/Adam_1save/RestoreV2:10*
use_locking(*
T0*
_class
loc:@dense_1/bias*
validate_shape(*
_output_shapes
:
А
save/Assign_11Assigndense_1/kernelsave/RestoreV2:11*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(
Е
save/Assign_12Assigndense_1/kernel/Adamsave/RestoreV2:12*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(
З
save/Assign_13Assigndense_1/kernel/Adam_1save/RestoreV2:13*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:

Ј
save/Assign_14Assigndense_2/biassave/RestoreV2:14*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class
loc:@dense_2/bias
­
save/Assign_15Assigndense_2/bias/Adamsave/RestoreV2:15*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class
loc:@dense_2/bias
Џ
save/Assign_16Assigndense_2/bias/Adam_1save/RestoreV2:16*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class
loc:@dense_2/bias
А
save/Assign_17Assigndense_2/kernelsave/RestoreV2:17*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*!
_class
loc:@dense_2/kernel
Е
save/Assign_18Assigndense_2/kernel/Adamsave/RestoreV2:18*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:

З
save/Assign_19Assigndense_2/kernel/Adam_1save/RestoreV2:19*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:

Ј
save/Assign_20Assigndense_3/biassave/RestoreV2:20*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:*
use_locking(
­
save/Assign_21Assigndense_3/bias/Adamsave/RestoreV2:21*
use_locking(*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:
Џ
save/Assign_22Assigndense_3/bias/Adam_1save/RestoreV2:22*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_3/bias
А
save/Assign_23Assigndense_3/kernelsave/RestoreV2:23*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:

Е
save/Assign_24Assigndense_3/kernel/Adamsave/RestoreV2:24*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(
З
save/Assign_25Assigndense_3/kernel/Adam_1save/RestoreV2:25*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:

Ђ
save/Assign_26Assignglobal_stepsave/RestoreV2:26*
use_locking(*
T0	*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
й
save/restore_shardNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8^save/Assign_9^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26
-
save/restore_allNoOp^save/restore_shard"уkd\~     mkН	2ДжAJёЎ

Р, ,
:
Add
x"T
y"T
z"T"
Ttype:
2	
W
AddN
inputs"T*N
sum"T"
Nint(0"!
Ttype:
2	
ю
	ApplyAdam
var"T	
m"T	
v"T
beta1_power"T
beta2_power"T
lr"T

beta1"T

beta2"T
epsilon"T	
grad"T
out"T" 
Ttype:
2	"
use_lockingbool( "
use_nesterovbool( 

ArgMax

input"T
	dimension"Tidx
output"output_type" 
Ttype:
2	"
Tidxtype0:
2	"
output_typetype0	:
2	
x
Assign
ref"T

value"T

output_ref"T"	
Ttype"
validate_shapebool("
use_lockingbool(
s
	AssignAdd
ref"T

value"T

output_ref"T" 
Ttype:
2	"
use_lockingbool( 
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
~
BiasAddGrad
out_backprop"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
8
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
B
Equal
x"T
y"T
z
"
Ttype:
2	

,
Exp
x"T
y"T"
Ttype:

2
Ў
FIFOQueueV2

handle"!
component_types
list(type)(0"
shapeslist(shape)
 ("
capacityintџџџџџџџџџ"
	containerstring "
shared_namestring 
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	

Gather
params"Tparams
indices"Tindices
output"Tparams"
validate_indicesbool("
Tparamstype"
Tindicestype:
2	
=
Greater
x"T
y"T
z
"
Ttype:
2	
B
GreaterEqual
x"T
y"T
z
"
Ttype:
2	
.
Identity

input"T
output"T"	
Ttype
N
IsVariableInitialized
ref"dtype
is_initialized
"
dtypetype
.
Log1p
x"T
y"T"
Ttype:

2
$

LogicalAnd
x

y

z



LogicalNot
x

y

p
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
	2
;
Maximum
x"T
y"T
z"T"
Ttype:

2	
8
MergeSummary
inputs*N
summary"
Nint(0
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
=
Mul
x"T
y"T
z"T"
Ttype:
2	
.
Neg
x"T
y"T"
Ttype:

2	

NoOp

OneHot
indices"TI	
depth
on_value"T
	off_value"T
output"T"
axisintџџџџџџџџџ"	
Ttype"
TItype0	:
2	
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:

Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
B
QueueCloseV2

handle"#
cancel_pending_enqueuesbool( 

QueueDequeueUpToV2

handle
n

components2component_types"!
component_types
list(type)(0"

timeout_msintџџџџџџџџџ
}
QueueEnqueueManyV2

handle

components2Tcomponents"
Tcomponents
list(type)(0"

timeout_msintџџџџџџџџџ
&
QueueSizeV2

handle
size
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
5

Reciprocal
x"T
y"T"
Ttype:

2	
D
Relu
features"T
activations"T"
Ttype:
2	
V
ReluGrad
	gradients"T
features"T
	backprops"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
P
ScalarSummary
tags
values"T
summary"
Ttype:
2	
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
O
Size

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
-
Sqrt
x"T
y"T"
Ttype:

2
1
Square
x"T
y"T"
Ttype:

2	
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
і
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
:
Sub
x"T
y"T
z"T"
Ttype:
2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	
s

VariableV2
ref"dtype"
shapeshape"
dtypetype"
	containerstring "
shared_namestring 
E
Where

input"T	
index	"%
Ttype0
:
2	

&
	ZerosLike
x"T
y"T"	
Ttype*1.6.02v1.6.0-0-gd2e24b6039Ѕб	

-global_step/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB *
_class
loc:@global_step

#global_step/Initializer/zeros/ConstConst*
dtype0	*
_output_shapes
: *
value	B	 R *
_class
loc:@global_step
Ь
global_step/Initializer/zerosFill-global_step/Initializer/zeros/shape_as_tensor#global_step/Initializer/zeros/Const*
T0	*

index_type0*
_class
loc:@global_step*
_output_shapes
: 

global_step
VariableV2*
shared_name *
_class
loc:@global_step*
	container *
shape: *
dtype0	*
_output_shapes
: 
В
global_step/AssignAssignglobal_stepglobal_step/Initializer/zeros*
use_locking(*
T0	*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
j
global_step/readIdentityglobal_step*
T0	*
_class
loc:@global_step*
_output_shapes
: 
З
enqueue_input/fifo_queueFIFOQueueV2"/device:CPU:0*
shared_name *
capacityш*
	container *
_output_shapes
: *
component_types
2	*
shapes

: :: 
m
enqueue_input/PlaceholderPlaceholder"/device:CPU:0*
dtype0	*
_output_shapes
:*
shape:
o
enqueue_input/Placeholder_1Placeholder"/device:CPU:0*
dtype0*
_output_shapes
:*
shape:
o
enqueue_input/Placeholder_2Placeholder"/device:CPU:0*
dtype0*
_output_shapes
:*
shape:
ы
$enqueue_input/fifo_queue_EnqueueManyQueueEnqueueManyV2enqueue_input/fifo_queueenqueue_input/Placeholderenqueue_input/Placeholder_1enqueue_input/Placeholder_2"/device:CPU:0*

timeout_msџџџџџџџџџ*
Tcomponents
2	
v
enqueue_input/fifo_queue_CloseQueueCloseV2enqueue_input/fifo_queue"/device:CPU:0*
cancel_pending_enqueues( 
x
 enqueue_input/fifo_queue_Close_1QueueCloseV2enqueue_input/fifo_queue"/device:CPU:0*
cancel_pending_enqueues(
m
enqueue_input/fifo_queue_SizeQueueSizeV2enqueue_input/fifo_queue"/device:CPU:0*
_output_shapes
: 
d
enqueue_input/sub/yConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 
|
enqueue_input/subSubenqueue_input/fifo_queue_Sizeenqueue_input/sub/y"/device:CPU:0*
T0*
_output_shapes
: 
h
enqueue_input/Maximum/xConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 
|
enqueue_input/MaximumMaximumenqueue_input/Maximum/xenqueue_input/sub"/device:CPU:0*
T0*
_output_shapes
: 
p
enqueue_input/CastCastenqueue_input/Maximum"/device:CPU:0*

SrcT0*
_output_shapes
: *

DstT0
g
enqueue_input/mul/yConst"/device:CPU:0*
valueB
 *o:*
dtype0*
_output_shapes
: 
q
enqueue_input/mulMulenqueue_input/Castenqueue_input/mul/y"/device:CPU:0*
T0*
_output_shapes
: 
х
Menqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full/tagsConst"/device:CPU:0*Y
valuePBN BHenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full*
dtype0*
_output_shapes
: 
ы
Henqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_fullScalarSummaryMenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full/tagsenqueue_input/mul"/device:CPU:0*
T0*
_output_shapes
: 
j
fifo_queue_DequeueUpTo/nConst"/device:CPU:0*
dtype0*
_output_shapes
: *
value
B :
э
fifo_queue_DequeueUpToQueueDequeueUpToV2enqueue_input/fifo_queuefifo_queue_DequeueUpTo/n"/device:CPU:0*

timeout_msџџџџџџџџџ*E
_output_shapes3
1:џџџџџџџџџ:џџџџџџџџџ:џџџџџџџџџ*
component_types
2	

-dense/kernel/Initializer/random_uniform/shapeConst*
valueB"   
   *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
:

+dense/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *я[ёО*
_class
loc:@dense/kernel

+dense/kernel/Initializer/random_uniform/maxConst*
valueB
 *я[ё>*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
х
5dense/kernel/Initializer/random_uniform/RandomUniformRandomUniform-dense/kernel/Initializer/random_uniform/shape*
T0*
_class
loc:@dense/kernel*
seed2 *
dtype0*
_output_shapes

:
*

seed 
Ю
+dense/kernel/Initializer/random_uniform/subSub+dense/kernel/Initializer/random_uniform/max+dense/kernel/Initializer/random_uniform/min*
T0*
_class
loc:@dense/kernel*
_output_shapes
: 
р
+dense/kernel/Initializer/random_uniform/mulMul5dense/kernel/Initializer/random_uniform/RandomUniform+dense/kernel/Initializer/random_uniform/sub*
T0*
_class
loc:@dense/kernel*
_output_shapes

:

в
'dense/kernel/Initializer/random_uniformAdd+dense/kernel/Initializer/random_uniform/mul+dense/kernel/Initializer/random_uniform/min*
T0*
_class
loc:@dense/kernel*
_output_shapes

:

Ё
dense/kernel
VariableV2*
shared_name *
_class
loc:@dense/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:

Ч
dense/kernel/AssignAssigndense/kernel'dense/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*
_class
loc:@dense/kernel
u
dense/kernel/readIdentitydense/kernel*
T0*
_class
loc:@dense/kernel*
_output_shapes

:


,dense/bias/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense/bias*
dtype0*
_output_shapes
:

"dense/bias/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *
_class
loc:@dense/bias
Ь
dense/bias/Initializer/zerosFill,dense/bias/Initializer/zeros/shape_as_tensor"dense/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/bias*
_output_shapes
:



dense/bias
VariableV2*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense/bias*
	container *
shape:

В
dense/bias/AssignAssign
dense/biasdense/bias/Initializer/zeros*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
k
dense/bias/readIdentity
dense/bias*
T0*
_class
loc:@dense/bias*
_output_shapes
:


dense/MatMulMatMulfifo_queue_DequeueUpTo:1dense/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b( 

dense/BiasAddBiasAdddense/MatMuldense/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ

S

dense/ReluReludense/BiasAdd*'
_output_shapes
:џџџџџџџџџ
*
T0
Ѓ
/dense_1/kernel/Initializer/random_uniform/shapeConst*
valueB"
      *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
:

-dense_1/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *БП*!
_class
loc:@dense_1/kernel

-dense_1/kernel/Initializer/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB
 *Б?*!
_class
loc:@dense_1/kernel
ы
7dense_1/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_1/kernel/Initializer/random_uniform/shape*
T0*!
_class
loc:@dense_1/kernel*
seed2 *
dtype0*
_output_shapes

:
*

seed 
ж
-dense_1/kernel/Initializer/random_uniform/subSub-dense_1/kernel/Initializer/random_uniform/max-dense_1/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes
: 
ш
-dense_1/kernel/Initializer/random_uniform/mulMul7dense_1/kernel/Initializer/random_uniform/RandomUniform-dense_1/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

к
)dense_1/kernel/Initializer/random_uniformAdd-dense_1/kernel/Initializer/random_uniform/mul-dense_1/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

Ѕ
dense_1/kernel
VariableV2*
shared_name *!
_class
loc:@dense_1/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:

Я
dense_1/kernel/AssignAssigndense_1/kernel)dense_1/kernel/Initializer/random_uniform*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:

{
dense_1/kernel/readIdentitydense_1/kernel*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:


.dense_1/bias/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
:

$dense_1/bias/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
д
dense_1/bias/Initializer/zerosFill.dense_1/bias/Initializer/zeros/shape_as_tensor$dense_1/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_1/bias*
_output_shapes
:

dense_1/bias
VariableV2*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@dense_1/bias
К
dense_1/bias/AssignAssigndense_1/biasdense_1/bias/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias
q
dense_1/bias/readIdentitydense_1/bias*
T0*
_class
loc:@dense_1/bias*
_output_shapes
:

dense_1/MatMulMatMul
dense/Reludense_1/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b( 

dense_1/BiasAddBiasAdddense_1/MatMuldense_1/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ
W
dense_1/ReluReludense_1/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
Ѓ
/dense_2/kernel/Initializer/random_uniform/shapeConst*
valueB"   
   *!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
:

-dense_2/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *БП*!
_class
loc:@dense_2/kernel

-dense_2/kernel/Initializer/random_uniform/maxConst*
valueB
 *Б?*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
ы
7dense_2/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_2/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:
*

seed *
T0*!
_class
loc:@dense_2/kernel*
seed2 
ж
-dense_2/kernel/Initializer/random_uniform/subSub-dense_2/kernel/Initializer/random_uniform/max-dense_2/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes
: 
ш
-dense_2/kernel/Initializer/random_uniform/mulMul7dense_2/kernel/Initializer/random_uniform/RandomUniform-dense_2/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

к
)dense_2/kernel/Initializer/random_uniformAdd-dense_2/kernel/Initializer/random_uniform/mul-dense_2/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Ѕ
dense_2/kernel
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_2/kernel*
	container *
shape
:

Я
dense_2/kernel/AssignAssigndense_2/kernel)dense_2/kernel/Initializer/random_uniform*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:

{
dense_2/kernel/readIdentitydense_2/kernel*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:


.dense_2/bias/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
:

$dense_2/bias/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
д
dense_2/bias/Initializer/zerosFill.dense_2/bias/Initializer/zeros/shape_as_tensor$dense_2/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_2/bias*
_output_shapes
:


dense_2/bias
VariableV2*
shared_name *
_class
loc:@dense_2/bias*
	container *
shape:
*
dtype0*
_output_shapes
:

К
dense_2/bias/AssignAssigndense_2/biasdense_2/bias/Initializer/zeros*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class
loc:@dense_2/bias
q
dense_2/bias/readIdentitydense_2/bias*
T0*
_class
loc:@dense_2/bias*
_output_shapes
:


dense_2/MatMulMatMuldense_1/Reludense_2/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b( 

dense_2/BiasAddBiasAdddense_2/MatMuldense_2/bias/read*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ
*
T0
W
dense_2/ReluReludense_2/BiasAdd*'
_output_shapes
:џџџџџџџџџ
*
T0
Ѓ
/dense_3/kernel/Initializer/random_uniform/shapeConst*
valueB"
      *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
:

-dense_3/kernel/Initializer/random_uniform/minConst*
valueB
 *ѓ5П*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 

-dense_3/kernel/Initializer/random_uniform/maxConst*
valueB
 *ѓ5?*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
ы
7dense_3/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_3/kernel/Initializer/random_uniform/shape*

seed *
T0*!
_class
loc:@dense_3/kernel*
seed2 *
dtype0*
_output_shapes

:

ж
-dense_3/kernel/Initializer/random_uniform/subSub-dense_3/kernel/Initializer/random_uniform/max-dense_3/kernel/Initializer/random_uniform/min*
_output_shapes
: *
T0*!
_class
loc:@dense_3/kernel
ш
-dense_3/kernel/Initializer/random_uniform/mulMul7dense_3/kernel/Initializer/random_uniform/RandomUniform-dense_3/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

к
)dense_3/kernel/Initializer/random_uniformAdd-dense_3/kernel/Initializer/random_uniform/mul-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

Ѕ
dense_3/kernel
VariableV2*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_3/kernel
Я
dense_3/kernel/AssignAssigndense_3/kernel)dense_3/kernel/Initializer/random_uniform*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:

{
dense_3/kernel/readIdentitydense_3/kernel*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:


.dense_3/bias/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:*
_class
loc:@dense_3/bias

$dense_3/bias/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
д
dense_3/bias/Initializer/zerosFill.dense_3/bias/Initializer/zeros/shape_as_tensor$dense_3/bias/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_3/bias*
_output_shapes
:

dense_3/bias
VariableV2*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@dense_3/bias*
	container *
shape:
К
dense_3/bias/AssignAssigndense_3/biasdense_3/bias/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:
q
dense_3/bias/readIdentitydense_3/bias*
T0*
_class
loc:@dense_3/bias*
_output_shapes
:

dense_3/MatMulMatMuldense_2/Reludense_3/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b( 

dense_3/BiasAddBiasAdddense_3/MatMuldense_3/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:џџџџџџџџџ
R
ArgMax/dimensionConst*
value	B :*
dtype0*
_output_shapes
: 

ArgMaxArgMaxdense_3/BiasAddArgMax/dimension*
T0*
output_type0	*#
_output_shapes
:џџџџџџџџџ*

Tidx0
U
one_hot/on_valueConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
V
one_hot/off_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
O
one_hot/depthConst*
dtype0*
_output_shapes
: *
value	B :
А
one_hotOneHotfifo_queue_DequeueUpTo:2one_hot/depthone_hot/on_valueone_hot/off_value*
T0*
TI0*
axisџџџџџџџџџ*'
_output_shapes
:џџџџџџџџџ
~
.sigmoid_cross_entropy_loss/xentropy/zeros_like	ZerosLikedense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
Г
0sigmoid_cross_entropy_loss/xentropy/GreaterEqualGreaterEqualdense_3/BiasAdd.sigmoid_cross_entropy_loss/xentropy/zeros_like*'
_output_shapes
:џџџџџџџџџ*
T0
й
*sigmoid_cross_entropy_loss/xentropy/SelectSelect0sigmoid_cross_entropy_loss/xentropy/GreaterEqualdense_3/BiasAdd.sigmoid_cross_entropy_loss/xentropy/zeros_like*
T0*'
_output_shapes
:џџџџџџџџџ
q
'sigmoid_cross_entropy_loss/xentropy/NegNegdense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
д
,sigmoid_cross_entropy_loss/xentropy/Select_1Select0sigmoid_cross_entropy_loss/xentropy/GreaterEqual'sigmoid_cross_entropy_loss/xentropy/Negdense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
z
'sigmoid_cross_entropy_loss/xentropy/mulMuldense_3/BiasAddone_hot*
T0*'
_output_shapes
:џџџџџџџџџ
Е
'sigmoid_cross_entropy_loss/xentropy/subSub*sigmoid_cross_entropy_loss/xentropy/Select'sigmoid_cross_entropy_loss/xentropy/mul*
T0*'
_output_shapes
:џџџџџџџџџ

'sigmoid_cross_entropy_loss/xentropy/ExpExp,sigmoid_cross_entropy_loss/xentropy/Select_1*'
_output_shapes
:џџџџџџџџџ*
T0

)sigmoid_cross_entropy_loss/xentropy/Log1pLog1p'sigmoid_cross_entropy_loss/xentropy/Exp*
T0*'
_output_shapes
:џџџџџџџџџ
А
#sigmoid_cross_entropy_loss/xentropyAdd'sigmoid_cross_entropy_loss/xentropy/sub)sigmoid_cross_entropy_loss/xentropy/Log1p*
T0*'
_output_shapes
:џџџџџџџџџ
|
7sigmoid_cross_entropy_loss/assert_broadcastable/weightsConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 

=sigmoid_cross_entropy_loss/assert_broadcastable/weights/shapeConst*
valueB *
dtype0*
_output_shapes
: 
~
<sigmoid_cross_entropy_loss/assert_broadcastable/weights/rankConst*
value	B : *
dtype0*
_output_shapes
: 

<sigmoid_cross_entropy_loss/assert_broadcastable/values/shapeShape#sigmoid_cross_entropy_loss/xentropy*
T0*
out_type0*
_output_shapes
:
}
;sigmoid_cross_entropy_loss/assert_broadcastable/values/rankConst*
value	B :*
dtype0*
_output_shapes
: 
S
Ksigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_successNoOp
Й
&sigmoid_cross_entropy_loss/ToFloat_1/xConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Є
sigmoid_cross_entropy_loss/MulMul#sigmoid_cross_entropy_loss/xentropy&sigmoid_cross_entropy_loss/ToFloat_1/x*
T0*'
_output_shapes
:џџџџџџџџџ
П
 sigmoid_cross_entropy_loss/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
:*
valueB"       
Ѕ
sigmoid_cross_entropy_loss/SumSumsigmoid_cross_entropy_loss/Mul sigmoid_cross_entropy_loss/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
С
.sigmoid_cross_entropy_loss/num_present/Equal/yConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 
Ў
,sigmoid_cross_entropy_loss/num_present/EqualEqual&sigmoid_cross_entropy_loss/ToFloat_1/x.sigmoid_cross_entropy_loss/num_present/Equal/y*
T0*
_output_shapes
: 
в
Asigmoid_cross_entropy_loss/num_present/zeros_like/shape_as_tensorConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
Ъ
7sigmoid_cross_entropy_loss/num_present/zeros_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 
ш
1sigmoid_cross_entropy_loss/num_present/zeros_likeFillAsigmoid_cross_entropy_loss/num_present/zeros_like/shape_as_tensor7sigmoid_cross_entropy_loss/num_present/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
Ч
6sigmoid_cross_entropy_loss/num_present/ones_like/ShapeConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
Щ
6sigmoid_cross_entropy_loss/num_present/ones_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
л
0sigmoid_cross_entropy_loss/num_present/ones_likeFill6sigmoid_cross_entropy_loss/num_present/ones_like/Shape6sigmoid_cross_entropy_loss/num_present/ones_like/Const*
T0*

index_type0*
_output_shapes
: 
ы
-sigmoid_cross_entropy_loss/num_present/SelectSelect,sigmoid_cross_entropy_loss/num_present/Equal1sigmoid_cross_entropy_loss/num_present/zeros_like0sigmoid_cross_entropy_loss/num_present/ones_like*
T0*
_output_shapes
: 
ь
[sigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/weights/shapeConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
ъ
Zsigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/weights/rankConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
value	B : *
dtype0*
_output_shapes
: 

Zsigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/values/shapeShape#sigmoid_cross_entropy_loss/xentropyL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
_output_shapes
:*
T0*
out_type0
щ
Ysigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/values/rankConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
value	B :*
dtype0*
_output_shapes
: 
П
isigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/static_scalar_check_successNoOpL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success
х
Hsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/ShapeShape#sigmoid_cross_entropy_loss/xentropyL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_successj^sigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/static_scalar_check_success*
_output_shapes
:*
T0*
out_type0
Ч
Hsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_successj^sigmoid_cross_entropy_loss/num_present/broadcast_weights/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Ђ
Bsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_likeFillHsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/ShapeHsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like/Const*
T0*

index_type0*'
_output_shapes
:џџџџџџџџџ
ф
8sigmoid_cross_entropy_loss/num_present/broadcast_weightsMul-sigmoid_cross_entropy_loss/num_present/SelectBsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like*'
_output_shapes
:џџџџџџџџџ*
T0
Ы
,sigmoid_cross_entropy_loss/num_present/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB"       *
dtype0*
_output_shapes
:
г
&sigmoid_cross_entropy_loss/num_presentSum8sigmoid_cross_entropy_loss/num_present/broadcast_weights,sigmoid_cross_entropy_loss/num_present/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
Г
"sigmoid_cross_entropy_loss/Const_1ConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
Љ
 sigmoid_cross_entropy_loss/Sum_1Sumsigmoid_cross_entropy_loss/Sum"sigmoid_cross_entropy_loss/Const_1*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
З
$sigmoid_cross_entropy_loss/Greater/yConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB
 *    

"sigmoid_cross_entropy_loss/GreaterGreater&sigmoid_cross_entropy_loss/num_present$sigmoid_cross_entropy_loss/Greater/y*
T0*
_output_shapes
: 
Е
"sigmoid_cross_entropy_loss/Equal/yConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB
 *    

 sigmoid_cross_entropy_loss/EqualEqual&sigmoid_cross_entropy_loss/num_present"sigmoid_cross_entropy_loss/Equal/y*
T0*
_output_shapes
: 
Л
*sigmoid_cross_entropy_loss/ones_like/ShapeConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
dtype0*
_output_shapes
: *
valueB 
Н
*sigmoid_cross_entropy_loss/ones_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *  ?*
dtype0*
_output_shapes
: 
З
$sigmoid_cross_entropy_loss/ones_likeFill*sigmoid_cross_entropy_loss/ones_like/Shape*sigmoid_cross_entropy_loss/ones_like/Const*
_output_shapes
: *
T0*

index_type0
М
!sigmoid_cross_entropy_loss/SelectSelect sigmoid_cross_entropy_loss/Equal$sigmoid_cross_entropy_loss/ones_like&sigmoid_cross_entropy_loss/num_present*
T0*
_output_shapes
: 

sigmoid_cross_entropy_loss/divRealDiv sigmoid_cross_entropy_loss/Sum_1!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
Ц
5sigmoid_cross_entropy_loss/zeros_like/shape_as_tensorConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB *
dtype0*
_output_shapes
: 
О
+sigmoid_cross_entropy_loss/zeros_like/ConstConstL^sigmoid_cross_entropy_loss/assert_broadcastable/static_scalar_check_success*
valueB
 *    *
dtype0*
_output_shapes
: 
Ф
%sigmoid_cross_entropy_loss/zeros_likeFill5sigmoid_cross_entropy_loss/zeros_like/shape_as_tensor+sigmoid_cross_entropy_loss/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
Ж
 sigmoid_cross_entropy_loss/valueSelect"sigmoid_cross_entropy_loss/Greatersigmoid_cross_entropy_loss/div%sigmoid_cross_entropy_loss/zeros_like*
T0*
_output_shapes
: 
R
gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
X
gradients/grad_ys_0Const*
valueB
 *  ?*
dtype0*
_output_shapes
: 
o
gradients/FillFillgradients/Shapegradients/grad_ys_0*
T0*

index_type0*
_output_shapes
: 

Jgradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 

@gradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    

:gradients/sigmoid_cross_entropy_loss/value_grad/zeros_likeFillJgradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/shape_as_tensor@gradients/sigmoid_cross_entropy_loss/value_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
б
6gradients/sigmoid_cross_entropy_loss/value_grad/SelectSelect"sigmoid_cross_entropy_loss/Greatergradients/Fill:gradients/sigmoid_cross_entropy_loss/value_grad/zeros_like*
_output_shapes
: *
T0
г
8gradients/sigmoid_cross_entropy_loss/value_grad/Select_1Select"sigmoid_cross_entropy_loss/Greater:gradients/sigmoid_cross_entropy_loss/value_grad/zeros_likegradients/Fill*
T0*
_output_shapes
: 
М
@gradients/sigmoid_cross_entropy_loss/value_grad/tuple/group_depsNoOp7^gradients/sigmoid_cross_entropy_loss/value_grad/Select9^gradients/sigmoid_cross_entropy_loss/value_grad/Select_1
Л
Hgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependencyIdentity6gradients/sigmoid_cross_entropy_loss/value_grad/SelectA^gradients/sigmoid_cross_entropy_loss/value_grad/tuple/group_deps*
T0*I
_class?
=;loc:@gradients/sigmoid_cross_entropy_loss/value_grad/Select*
_output_shapes
: 
С
Jgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependency_1Identity8gradients/sigmoid_cross_entropy_loss/value_grad/Select_1A^gradients/sigmoid_cross_entropy_loss/value_grad/tuple/group_deps*
T0*K
_classA
?=loc:@gradients/sigmoid_cross_entropy_loss/value_grad/Select_1*
_output_shapes
: 
v
3gradients/sigmoid_cross_entropy_loss/div_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
x
5gradients/sigmoid_cross_entropy_loss/div_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 

Cgradients/sigmoid_cross_entropy_loss/div_grad/BroadcastGradientArgsBroadcastGradientArgs3gradients/sigmoid_cross_entropy_loss/div_grad/Shape5gradients/sigmoid_cross_entropy_loss/div_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ
Ю
5gradients/sigmoid_cross_entropy_loss/div_grad/RealDivRealDivHgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependency!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
є
1gradients/sigmoid_cross_entropy_loss/div_grad/SumSum5gradients/sigmoid_cross_entropy_loss/div_grad/RealDivCgradients/sigmoid_cross_entropy_loss/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
з
5gradients/sigmoid_cross_entropy_loss/div_grad/ReshapeReshape1gradients/sigmoid_cross_entropy_loss/div_grad/Sum3gradients/sigmoid_cross_entropy_loss/div_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
{
1gradients/sigmoid_cross_entropy_loss/div_grad/NegNeg sigmoid_cross_entropy_loss/Sum_1*
T0*
_output_shapes
: 
Й
7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_1RealDiv1gradients/sigmoid_cross_entropy_loss/div_grad/Neg!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
П
7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_2RealDiv7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_1!sigmoid_cross_entropy_loss/Select*
T0*
_output_shapes
: 
м
1gradients/sigmoid_cross_entropy_loss/div_grad/mulMulHgradients/sigmoid_cross_entropy_loss/value_grad/tuple/control_dependency7gradients/sigmoid_cross_entropy_loss/div_grad/RealDiv_2*
T0*
_output_shapes
: 
є
3gradients/sigmoid_cross_entropy_loss/div_grad/Sum_1Sum1gradients/sigmoid_cross_entropy_loss/div_grad/mulEgradients/sigmoid_cross_entropy_loss/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
н
7gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1Reshape3gradients/sigmoid_cross_entropy_loss/div_grad/Sum_15gradients/sigmoid_cross_entropy_loss/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
И
>gradients/sigmoid_cross_entropy_loss/div_grad/tuple/group_depsNoOp6^gradients/sigmoid_cross_entropy_loss/div_grad/Reshape8^gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1
Е
Fgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependencyIdentity5gradients/sigmoid_cross_entropy_loss/div_grad/Reshape?^gradients/sigmoid_cross_entropy_loss/div_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/sigmoid_cross_entropy_loss/div_grad/Reshape*
_output_shapes
: 
Л
Hgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency_1Identity7gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1?^gradients/sigmoid_cross_entropy_loss/div_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/sigmoid_cross_entropy_loss/div_grad/Reshape_1*
_output_shapes
: 

=gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Reshape/shapeConst*
valueB *
dtype0*
_output_shapes
: 
ј
7gradients/sigmoid_cross_entropy_loss/Sum_1_grad/ReshapeReshapeFgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency=gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes
: 

>gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Tile/multiplesConst*
valueB *
dtype0*
_output_shapes
: 
ш
4gradients/sigmoid_cross_entropy_loss/Sum_1_grad/TileTile7gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Reshape>gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Tile/multiples*

Tmultiples0*
T0*
_output_shapes
: 

Kgradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB 

Agradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    

;gradients/sigmoid_cross_entropy_loss/Select_grad/zeros_likeFillKgradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/shape_as_tensorAgradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like/Const*
_output_shapes
: *
T0*

index_type0

7gradients/sigmoid_cross_entropy_loss/Select_grad/SelectSelect sigmoid_cross_entropy_loss/EqualHgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency_1;gradients/sigmoid_cross_entropy_loss/Select_grad/zeros_like*
_output_shapes
: *
T0

9gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1Select sigmoid_cross_entropy_loss/Equal;gradients/sigmoid_cross_entropy_loss/Select_grad/zeros_likeHgradients/sigmoid_cross_entropy_loss/div_grad/tuple/control_dependency_1*
_output_shapes
: *
T0
П
Agradients/sigmoid_cross_entropy_loss/Select_grad/tuple/group_depsNoOp8^gradients/sigmoid_cross_entropy_loss/Select_grad/Select:^gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1
П
Igradients/sigmoid_cross_entropy_loss/Select_grad/tuple/control_dependencyIdentity7gradients/sigmoid_cross_entropy_loss/Select_grad/SelectB^gradients/sigmoid_cross_entropy_loss/Select_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/sigmoid_cross_entropy_loss/Select_grad/Select*
_output_shapes
: 
Х
Kgradients/sigmoid_cross_entropy_loss/Select_grad/tuple/control_dependency_1Identity9gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1B^gradients/sigmoid_cross_entropy_loss/Select_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients/sigmoid_cross_entropy_loss/Select_grad/Select_1*
_output_shapes
: 

;gradients/sigmoid_cross_entropy_loss/Sum_grad/Reshape/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
ъ
5gradients/sigmoid_cross_entropy_loss/Sum_grad/ReshapeReshape4gradients/sigmoid_cross_entropy_loss/Sum_1_grad/Tile;gradients/sigmoid_cross_entropy_loss/Sum_grad/Reshape/shape*
_output_shapes

:*
T0*
Tshape0

3gradients/sigmoid_cross_entropy_loss/Sum_grad/ShapeShapesigmoid_cross_entropy_loss/Mul*
_output_shapes
:*
T0*
out_type0
ъ
2gradients/sigmoid_cross_entropy_loss/Sum_grad/TileTile5gradients/sigmoid_cross_entropy_loss/Sum_grad/Reshape3gradients/sigmoid_cross_entropy_loss/Sum_grad/Shape*

Tmultiples0*
T0*'
_output_shapes
:џџџџџџџџџ

3gradients/sigmoid_cross_entropy_loss/Mul_grad/ShapeShape#sigmoid_cross_entropy_loss/xentropy*
_output_shapes
:*
T0*
out_type0
x
5gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 

Cgradients/sigmoid_cross_entropy_loss/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs3gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape5gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ
Ц
1gradients/sigmoid_cross_entropy_loss/Mul_grad/mulMul2gradients/sigmoid_cross_entropy_loss/Sum_grad/Tile&sigmoid_cross_entropy_loss/ToFloat_1/x*'
_output_shapes
:џџџџџџџџџ*
T0
№
1gradients/sigmoid_cross_entropy_loss/Mul_grad/SumSum1gradients/sigmoid_cross_entropy_loss/Mul_grad/mulCgradients/sigmoid_cross_entropy_loss/Mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ш
5gradients/sigmoid_cross_entropy_loss/Mul_grad/ReshapeReshape1gradients/sigmoid_cross_entropy_loss/Mul_grad/Sum3gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
Х
3gradients/sigmoid_cross_entropy_loss/Mul_grad/mul_1Mul#sigmoid_cross_entropy_loss/xentropy2gradients/sigmoid_cross_entropy_loss/Sum_grad/Tile*
T0*'
_output_shapes
:џџџџџџџџџ
і
3gradients/sigmoid_cross_entropy_loss/Mul_grad/Sum_1Sum3gradients/sigmoid_cross_entropy_loss/Mul_grad/mul_1Egradients/sigmoid_cross_entropy_loss/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
н
7gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1Reshape3gradients/sigmoid_cross_entropy_loss/Mul_grad/Sum_15gradients/sigmoid_cross_entropy_loss/Mul_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
И
>gradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/group_depsNoOp6^gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape8^gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1
Ц
Fgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependencyIdentity5gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape?^gradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape*'
_output_shapes
:џџџџџџџџџ
Л
Hgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependency_1Identity7gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1?^gradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/sigmoid_cross_entropy_loss/Mul_grad/Reshape_1*
_output_shapes
: 

Cgradients/sigmoid_cross_entropy_loss/num_present_grad/Reshape/shapeConst*
valueB"      *
dtype0*
_output_shapes
:

=gradients/sigmoid_cross_entropy_loss/num_present_grad/ReshapeReshapeKgradients/sigmoid_cross_entropy_loss/Select_grad/tuple/control_dependency_1Cgradients/sigmoid_cross_entropy_loss/num_present_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes

:
Г
;gradients/sigmoid_cross_entropy_loss/num_present_grad/ShapeShape8sigmoid_cross_entropy_loss/num_present/broadcast_weights*
T0*
out_type0*
_output_shapes
:

:gradients/sigmoid_cross_entropy_loss/num_present_grad/TileTile=gradients/sigmoid_cross_entropy_loss/num_present_grad/Reshape;gradients/sigmoid_cross_entropy_loss/num_present_grad/Shape*
T0*'
_output_shapes
:џџџџџџџџџ*

Tmultiples0

Mgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
б
Ogradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape_1ShapeBsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like*
_output_shapes
:*
T0*
out_type0
г
]gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/BroadcastGradientArgsBroadcastGradientArgsMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ShapeOgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ

Kgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mulMul:gradients/sigmoid_cross_entropy_loss/num_present_grad/TileBsigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like*
T0*'
_output_shapes
:џџџџџџџџџ
О
Kgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/SumSumKgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mul]gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ѕ
Ogradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ReshapeReshapeKgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/SumMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ё
Mgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mul_1Mul-sigmoid_cross_entropy_loss/num_present/Select:gradients/sigmoid_cross_entropy_loss/num_present_grad/Tile*
T0*'
_output_shapes
:џџџџџџџџџ
Ф
Mgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Sum_1SumMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/mul_1_gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
М
Qgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1ReshapeMgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Sum_1Ogradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ

Xgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/group_depsNoOpP^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ReshapeR^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1

`gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/control_dependencyIdentityOgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/ReshapeY^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/group_deps*
T0*b
_classX
VTloc:@gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape*
_output_shapes
: 
Д
bgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/control_dependency_1IdentityQgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1Y^gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*d
_classZ
XVloc:@gradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/Reshape_1
Ј
Wgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like_grad/ConstConst*
dtype0*
_output_shapes
:*
valueB"       
з
Ugradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like_grad/SumSumbgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights_grad/tuple/control_dependency_1Wgradients/sigmoid_cross_entropy_loss/num_present/broadcast_weights/ones_like_grad/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 

8gradients/sigmoid_cross_entropy_loss/xentropy_grad/ShapeShape'sigmoid_cross_entropy_loss/xentropy/sub*
_output_shapes
:*
T0*
out_type0
Ѓ
:gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape_1Shape)sigmoid_cross_entropy_loss/xentropy/Log1p*
T0*
out_type0*
_output_shapes
:

Hgradients/sigmoid_cross_entropy_loss/xentropy_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape:gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ

6gradients/sigmoid_cross_entropy_loss/xentropy_grad/SumSumFgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependencyHgradients/sigmoid_cross_entropy_loss/xentropy_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ї
:gradients/sigmoid_cross_entropy_loss/xentropy_grad/ReshapeReshape6gradients/sigmoid_cross_entropy_loss/xentropy_grad/Sum8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ

8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Sum_1SumFgradients/sigmoid_cross_entropy_loss/Mul_grad/tuple/control_dependencyJgradients/sigmoid_cross_entropy_loss/xentropy_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
§
<gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1Reshape8gradients/sigmoid_cross_entropy_loss/xentropy_grad/Sum_1:gradients/sigmoid_cross_entropy_loss/xentropy_grad/Shape_1*'
_output_shapes
:џџџџџџџџџ*
T0*
Tshape0
Ч
Cgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/group_depsNoOp;^gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape=^gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1
к
Kgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependencyIdentity:gradients/sigmoid_cross_entropy_loss/xentropy_grad/ReshapeD^gradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape*'
_output_shapes
:џџџџџџџџџ
р
Mgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependency_1Identity<gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1D^gradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients/sigmoid_cross_entropy_loss/xentropy_grad/Reshape_1*'
_output_shapes
:џџџџџџџџџ
І
<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ShapeShape*sigmoid_cross_entropy_loss/xentropy/Select*
_output_shapes
:*
T0*
out_type0
Ѕ
>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape_1Shape'sigmoid_cross_entropy_loss/xentropy/mul*
_output_shapes
:*
T0*
out_type0
 
Lgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/BroadcastGradientArgsBroadcastGradientArgs<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape_1*
T0*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ

:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/SumSumKgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependencyLgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 

>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ReshapeReshape:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Sum<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
 
<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Sum_1SumKgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependencyNgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ђ
:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/NegNeg<gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Sum_1*
T0*
_output_shapes
:

@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1Reshape:gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Neg>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Shape_1*'
_output_shapes
:џџџџџџџџџ*
T0*
Tshape0
г
Ggradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/group_depsNoOp?^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ReshapeA^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1
ъ
Ogradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependencyIdentity>gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/ReshapeH^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape*'
_output_shapes
:џџџџџџџџџ
№
Qgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency_1Identity@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1H^gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/Reshape_1
г
>gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/add/xConstN^gradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependency_1*
valueB
 *  ?*
dtype0*
_output_shapes
: 
о
<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/addAdd>gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/add/x'sigmoid_cross_entropy_loss/xentropy/Exp*
T0*'
_output_shapes
:џџџџџџџџџ
С
Cgradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/Reciprocal
Reciprocal<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/add*
T0*'
_output_shapes
:џџџџџџџџџ

<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/mulMulMgradients/sigmoid_cross_entropy_loss/xentropy_grad/tuple/control_dependency_1Cgradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/Reciprocal*
T0*'
_output_shapes
:џџџџџџџџџ

Dgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/zeros_like	ZerosLikedense_3/BiasAdd*
T0*'
_output_shapes
:џџџџџџџџџ
Х
@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/SelectSelect0sigmoid_cross_entropy_loss/xentropy/GreaterEqualOgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependencyDgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/zeros_like*
T0*'
_output_shapes
:џџџџџџџџџ
Ч
Bgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1Select0sigmoid_cross_entropy_loss/xentropy/GreaterEqualDgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/zeros_likeOgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency*'
_output_shapes
:џџџџџџџџџ*
T0
к
Jgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/group_depsNoOpA^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/SelectC^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1
є
Rgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/control_dependencyIdentity@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/SelectK^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select*'
_output_shapes
:џџџџџџџџџ
њ
Tgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/control_dependency_1IdentityBgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1K^gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select_1*'
_output_shapes
:џџџџџџџџџ

<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ShapeShapedense_3/BiasAdd*
T0*
out_type0*
_output_shapes
:

>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape_1Shapeone_hot*
T0*
out_type0*
_output_shapes
:
 
Lgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/BroadcastGradientArgsBroadcastGradientArgs<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape_1*2
_output_shapes 
:џџџџџџџџџ:џџџџџџџџџ*
T0
Я
:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mulMulQgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency_1one_hot*
T0*'
_output_shapes
:џџџџџџџџџ

:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/SumSum:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mulLgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0

>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ReshapeReshape:gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Sum<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
й
<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mul_1Muldense_3/BiasAddQgradients/sigmoid_cross_entropy_loss/xentropy/sub_grad/tuple/control_dependency_1*'
_output_shapes
:џџџџџџџџџ*
T0

<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Sum_1Sum<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/mul_1Ngradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 

@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1Reshape<gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Sum_1>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:џџџџџџџџџ
г
Ggradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/group_depsNoOp?^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ReshapeA^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1
ъ
Ogradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/control_dependencyIdentity>gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/ReshapeH^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*Q
_classG
ECloc:@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape
№
Qgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/control_dependency_1Identity@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1H^gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/Reshape_1*'
_output_shapes
:џџџџџџџџџ
к
:gradients/sigmoid_cross_entropy_loss/xentropy/Exp_grad/mulMul<gradients/sigmoid_cross_entropy_loss/xentropy/Log1p_grad/mul'sigmoid_cross_entropy_loss/xentropy/Exp*
T0*'
_output_shapes
:џџџџџџџџџ
Ў
Fgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/zeros_like	ZerosLike'sigmoid_cross_entropy_loss/xentropy/Neg*
T0*'
_output_shapes
:џџџџџџџџџ
Д
Bgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/SelectSelect0sigmoid_cross_entropy_loss/xentropy/GreaterEqual:gradients/sigmoid_cross_entropy_loss/xentropy/Exp_grad/mulFgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/zeros_like*
T0*'
_output_shapes
:џџџџџџџџџ
Ж
Dgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1Select0sigmoid_cross_entropy_loss/xentropy/GreaterEqualFgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/zeros_like:gradients/sigmoid_cross_entropy_loss/xentropy/Exp_grad/mul*'
_output_shapes
:џџџџџџџџџ*
T0
р
Lgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/group_depsNoOpC^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/SelectE^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1
ќ
Tgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependencyIdentityBgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/SelectM^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select*'
_output_shapes
:џџџџџџџџџ

Vgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependency_1IdentityDgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1M^gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*W
_classM
KIloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/Select_1
Щ
:gradients/sigmoid_cross_entropy_loss/xentropy/Neg_grad/NegNegTgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependency*
T0*'
_output_shapes
:џџџџџџџџџ
п
gradients/AddNAddNRgradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/tuple/control_dependencyOgradients/sigmoid_cross_entropy_loss/xentropy/mul_grad/tuple/control_dependencyVgradients/sigmoid_cross_entropy_loss/xentropy/Select_1_grad/tuple/control_dependency_1:gradients/sigmoid_cross_entropy_loss/xentropy/Neg_grad/Neg*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select*
N*'
_output_shapes
:џџџџџџџџџ

*gradients/dense_3/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN*
T0*
data_formatNHWC*
_output_shapes
:
u
/gradients/dense_3/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN+^gradients/dense_3/BiasAdd_grad/BiasAddGrad

7gradients/dense_3/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN0^gradients/dense_3/BiasAdd_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*S
_classI
GEloc:@gradients/sigmoid_cross_entropy_loss/xentropy/Select_grad/Select

9gradients/dense_3/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_3/BiasAdd_grad/BiasAddGrad0^gradients/dense_3/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_3/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
д
$gradients/dense_3/MatMul_grad/MatMulMatMul7gradients/dense_3/BiasAdd_grad/tuple/control_dependencydense_3/kernel/read*
transpose_b(*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( 
Ц
&gradients/dense_3/MatMul_grad/MatMul_1MatMuldense_2/Relu7gradients/dense_3/BiasAdd_grad/tuple/control_dependency*
T0*
_output_shapes

:
*
transpose_a(*
transpose_b( 

.gradients/dense_3/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_3/MatMul_grad/MatMul'^gradients/dense_3/MatMul_grad/MatMul_1

6gradients/dense_3/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_3/MatMul_grad/MatMul/^gradients/dense_3/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_3/MatMul_grad/MatMul*'
_output_shapes
:џџџџџџџџџ


8gradients/dense_3/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_3/MatMul_grad/MatMul_1/^gradients/dense_3/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_3/MatMul_grad/MatMul_1*
_output_shapes

:

Ј
$gradients/dense_2/Relu_grad/ReluGradReluGrad6gradients/dense_3/MatMul_grad/tuple/control_dependencydense_2/Relu*
T0*'
_output_shapes
:џџџџџџџџџ


*gradients/dense_2/BiasAdd_grad/BiasAddGradBiasAddGrad$gradients/dense_2/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:


/gradients/dense_2/BiasAdd_grad/tuple/group_depsNoOp%^gradients/dense_2/Relu_grad/ReluGrad+^gradients/dense_2/BiasAdd_grad/BiasAddGrad

7gradients/dense_2/BiasAdd_grad/tuple/control_dependencyIdentity$gradients/dense_2/Relu_grad/ReluGrad0^gradients/dense_2/BiasAdd_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ
*
T0*7
_class-
+)loc:@gradients/dense_2/Relu_grad/ReluGrad

9gradients/dense_2/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_2/BiasAdd_grad/BiasAddGrad0^gradients/dense_2/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_2/BiasAdd_grad/BiasAddGrad*
_output_shapes
:

д
$gradients/dense_2/MatMul_grad/MatMulMatMul7gradients/dense_2/BiasAdd_grad/tuple/control_dependencydense_2/kernel/read*
transpose_b(*
T0*'
_output_shapes
:џџџџџџџџџ*
transpose_a( 
Ц
&gradients/dense_2/MatMul_grad/MatMul_1MatMuldense_1/Relu7gradients/dense_2/BiasAdd_grad/tuple/control_dependency*
T0*
_output_shapes

:
*
transpose_a(*
transpose_b( 

.gradients/dense_2/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_2/MatMul_grad/MatMul'^gradients/dense_2/MatMul_grad/MatMul_1

6gradients/dense_2/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_2/MatMul_grad/MatMul/^gradients/dense_2/MatMul_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*7
_class-
+)loc:@gradients/dense_2/MatMul_grad/MatMul

8gradients/dense_2/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_2/MatMul_grad/MatMul_1/^gradients/dense_2/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_2/MatMul_grad/MatMul_1*
_output_shapes

:

Ј
$gradients/dense_1/Relu_grad/ReluGradReluGrad6gradients/dense_2/MatMul_grad/tuple/control_dependencydense_1/Relu*
T0*'
_output_shapes
:џџџџџџџџџ

*gradients/dense_1/BiasAdd_grad/BiasAddGradBiasAddGrad$gradients/dense_1/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:

/gradients/dense_1/BiasAdd_grad/tuple/group_depsNoOp%^gradients/dense_1/Relu_grad/ReluGrad+^gradients/dense_1/BiasAdd_grad/BiasAddGrad

7gradients/dense_1/BiasAdd_grad/tuple/control_dependencyIdentity$gradients/dense_1/Relu_grad/ReluGrad0^gradients/dense_1/BiasAdd_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_1/Relu_grad/ReluGrad*'
_output_shapes
:џџџџџџџџџ

9gradients/dense_1/BiasAdd_grad/tuple/control_dependency_1Identity*gradients/dense_1/BiasAdd_grad/BiasAddGrad0^gradients/dense_1/BiasAdd_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/dense_1/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
д
$gradients/dense_1/MatMul_grad/MatMulMatMul7gradients/dense_1/BiasAdd_grad/tuple/control_dependencydense_1/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ
*
transpose_a( *
transpose_b(
Ф
&gradients/dense_1/MatMul_grad/MatMul_1MatMul
dense/Relu7gradients/dense_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
_output_shapes

:
*
transpose_a(

.gradients/dense_1/MatMul_grad/tuple/group_depsNoOp%^gradients/dense_1/MatMul_grad/MatMul'^gradients/dense_1/MatMul_grad/MatMul_1

6gradients/dense_1/MatMul_grad/tuple/control_dependencyIdentity$gradients/dense_1/MatMul_grad/MatMul/^gradients/dense_1/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense_1/MatMul_grad/MatMul*'
_output_shapes
:џџџџџџџџџ


8gradients/dense_1/MatMul_grad/tuple/control_dependency_1Identity&gradients/dense_1/MatMul_grad/MatMul_1/^gradients/dense_1/MatMul_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/dense_1/MatMul_grad/MatMul_1*
_output_shapes

:

Є
"gradients/dense/Relu_grad/ReluGradReluGrad6gradients/dense_1/MatMul_grad/tuple/control_dependency
dense/Relu*'
_output_shapes
:џџџџџџџџџ
*
T0

(gradients/dense/BiasAdd_grad/BiasAddGradBiasAddGrad"gradients/dense/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:


-gradients/dense/BiasAdd_grad/tuple/group_depsNoOp#^gradients/dense/Relu_grad/ReluGrad)^gradients/dense/BiasAdd_grad/BiasAddGrad
ў
5gradients/dense/BiasAdd_grad/tuple/control_dependencyIdentity"gradients/dense/Relu_grad/ReluGrad.^gradients/dense/BiasAdd_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/dense/Relu_grad/ReluGrad*'
_output_shapes
:џџџџџџџџџ

џ
7gradients/dense/BiasAdd_grad/tuple/control_dependency_1Identity(gradients/dense/BiasAdd_grad/BiasAddGrad.^gradients/dense/BiasAdd_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/dense/BiasAdd_grad/BiasAddGrad*
_output_shapes
:

Ю
"gradients/dense/MatMul_grad/MatMulMatMul5gradients/dense/BiasAdd_grad/tuple/control_dependencydense/kernel/read*
T0*'
_output_shapes
:џџџџџџџџџ*
transpose_a( *
transpose_b(
Ю
$gradients/dense/MatMul_grad/MatMul_1MatMulfifo_queue_DequeueUpTo:15gradients/dense/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
_output_shapes

:
*
transpose_a(

,gradients/dense/MatMul_grad/tuple/group_depsNoOp#^gradients/dense/MatMul_grad/MatMul%^gradients/dense/MatMul_grad/MatMul_1
ќ
4gradients/dense/MatMul_grad/tuple/control_dependencyIdentity"gradients/dense/MatMul_grad/MatMul-^gradients/dense/MatMul_grad/tuple/group_deps*'
_output_shapes
:џџџџџџџџџ*
T0*5
_class+
)'loc:@gradients/dense/MatMul_grad/MatMul
љ
6gradients/dense/MatMul_grad/tuple/control_dependency_1Identity$gradients/dense/MatMul_grad/MatMul_1-^gradients/dense/MatMul_grad/tuple/group_deps*
T0*7
_class-
+)loc:@gradients/dense/MatMul_grad/MatMul_1*
_output_shapes

:

}
beta1_power/initial_valueConst*
valueB
 *fff?*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 

beta1_power
VariableV2*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name *
_class
loc:@dense/bias
­
beta1_power/AssignAssignbeta1_powerbeta1_power/initial_value*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: 
i
beta1_power/readIdentitybeta1_power*
_output_shapes
: *
T0*
_class
loc:@dense/bias
}
beta2_power/initial_valueConst*
valueB
 *wО?*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 

beta2_power
VariableV2*
shared_name *
_class
loc:@dense/bias*
	container *
shape: *
dtype0*
_output_shapes
: 
­
beta2_power/AssignAssignbeta2_powerbeta2_power/initial_value*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@dense/bias
i
beta2_power/readIdentitybeta2_power*
T0*
_class
loc:@dense/bias*
_output_shapes
: 
Ѕ
3dense/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"   
   *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
:

)dense/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
ч
#dense/kernel/Adam/Initializer/zerosFill3dense/kernel/Adam/Initializer/zeros/shape_as_tensor)dense/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/kernel*
_output_shapes

:

І
dense/kernel/Adam
VariableV2*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *
_class
loc:@dense/kernel
Э
dense/kernel/Adam/AssignAssigndense/kernel/Adam#dense/kernel/Adam/Initializer/zeros*
T0*
_class
loc:@dense/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(

dense/kernel/Adam/readIdentitydense/kernel/Adam*
T0*
_class
loc:@dense/kernel*
_output_shapes

:

Ї
5dense/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"   
   *
_class
loc:@dense/kernel

+dense/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
э
%dense/kernel/Adam_1/Initializer/zerosFill5dense/kernel/Adam_1/Initializer/zeros/shape_as_tensor+dense/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/kernel*
_output_shapes

:

Ј
dense/kernel/Adam_1
VariableV2*
_class
loc:@dense/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name 
г
dense/kernel/Adam_1/AssignAssigndense/kernel/Adam_1%dense/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense/kernel*
validate_shape(*
_output_shapes

:


dense/kernel/Adam_1/readIdentitydense/kernel/Adam_1*
T0*
_class
loc:@dense/kernel*
_output_shapes

:


1dense/bias/Adam/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:
*
_class
loc:@dense/bias

'dense/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
л
!dense/bias/Adam/Initializer/zerosFill1dense/bias/Adam/Initializer/zeros/shape_as_tensor'dense/bias/Adam/Initializer/zeros/Const*
_output_shapes
:
*
T0*

index_type0*
_class
loc:@dense/bias

dense/bias/Adam
VariableV2*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense/bias*
	container *
shape:

С
dense/bias/Adam/AssignAssigndense/bias/Adam!dense/bias/Adam/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

u
dense/bias/Adam/readIdentitydense/bias/Adam*
T0*
_class
loc:@dense/bias*
_output_shapes
:


3dense/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense/bias*
dtype0*
_output_shapes
:

)dense/bias/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
с
#dense/bias/Adam_1/Initializer/zerosFill3dense/bias/Adam_1/Initializer/zeros/shape_as_tensor)dense/bias/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense/bias*
_output_shapes
:


dense/bias/Adam_1
VariableV2*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense/bias*
	container *
shape:

Ч
dense/bias/Adam_1/AssignAssigndense/bias/Adam_1#dense/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

y
dense/bias/Adam_1/readIdentitydense/bias/Adam_1*
T0*
_class
loc:@dense/bias*
_output_shapes
:

Љ
5dense_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
:

+dense_1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
я
%dense_1/kernel/Adam/Initializer/zerosFill5dense_1/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

Њ
dense_1/kernel/Adam
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_1/kernel*
	container *
shape
:

е
dense_1/kernel/Adam/AssignAssigndense_1/kernel/Adam%dense_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:


dense_1/kernel/Adam/readIdentitydense_1/kernel/Adam*
T0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

Ћ
7dense_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
:

-dense_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
ѕ
'dense_1/kernel/Adam_1/Initializer/zerosFill7dense_1/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_1/kernel*
_output_shapes

:

Ќ
dense_1/kernel/Adam_1
VariableV2*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_1/kernel
л
dense_1/kernel/Adam_1/AssignAssigndense_1/kernel/Adam_1'dense_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:


dense_1/kernel/Adam_1/readIdentitydense_1/kernel/Adam_1*
_output_shapes

:
*
T0*!
_class
loc:@dense_1/kernel

3dense_1/bias/Adam/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
:

)dense_1/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
у
#dense_1/bias/Adam/Initializer/zerosFill3dense_1/bias/Adam/Initializer/zeros/shape_as_tensor)dense_1/bias/Adam/Initializer/zeros/Const*
_output_shapes
:*
T0*

index_type0*
_class
loc:@dense_1/bias

dense_1/bias/Adam
VariableV2*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@dense_1/bias*
	container *
shape:
Щ
dense_1/bias/Adam/AssignAssigndense_1/bias/Adam#dense_1/bias/Adam/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias
{
dense_1/bias/Adam/readIdentitydense_1/bias/Adam*
T0*
_class
loc:@dense_1/bias*
_output_shapes
:
 
5dense_1/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
:

+dense_1/bias/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
щ
%dense_1/bias/Adam_1/Initializer/zerosFill5dense_1/bias/Adam_1/Initializer/zeros/shape_as_tensor+dense_1/bias/Adam_1/Initializer/zeros/Const*
_output_shapes
:*
T0*

index_type0*
_class
loc:@dense_1/bias
 
dense_1/bias/Adam_1
VariableV2*
shared_name *
_class
loc:@dense_1/bias*
	container *
shape:*
dtype0*
_output_shapes
:
Я
dense_1/bias/Adam_1/AssignAssigndense_1/bias/Adam_1%dense_1/bias/Adam_1/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias

dense_1/bias/Adam_1/readIdentitydense_1/bias/Adam_1*
T0*
_class
loc:@dense_1/bias*
_output_shapes
:
Љ
5dense_2/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"   
   *!
_class
loc:@dense_2/kernel

+dense_2/kernel/Adam/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *!
_class
loc:@dense_2/kernel
я
%dense_2/kernel/Adam/Initializer/zerosFill5dense_2/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_2/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Њ
dense_2/kernel/Adam
VariableV2*!
_class
loc:@dense_2/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name 
е
dense_2/kernel/Adam/AssignAssigndense_2/kernel/Adam%dense_2/kernel/Adam/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:


dense_2/kernel/Adam/readIdentitydense_2/kernel/Adam*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Ћ
7dense_2/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"   
   *!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
:

-dense_2/kernel/Adam_1/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *!
_class
loc:@dense_2/kernel
ѕ
'dense_2/kernel/Adam_1/Initializer/zerosFill7dense_2/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_2/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_2/kernel*
_output_shapes

:

Ќ
dense_2/kernel/Adam_1
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_2/kernel*
	container *
shape
:

л
dense_2/kernel/Adam_1/AssignAssigndense_2/kernel/Adam_1'dense_2/kernel/Adam_1/Initializer/zeros*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*!
_class
loc:@dense_2/kernel

dense_2/kernel/Adam_1/readIdentitydense_2/kernel/Adam_1*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes

:


3dense_2/bias/Adam/Initializer/zeros/shape_as_tensorConst*
valueB:
*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
:

)dense_2/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
у
#dense_2/bias/Adam/Initializer/zerosFill3dense_2/bias/Adam/Initializer/zeros/shape_as_tensor)dense_2/bias/Adam/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_2/bias*
_output_shapes
:


dense_2/bias/Adam
VariableV2*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense_2/bias*
	container *
shape:

Щ
dense_2/bias/Adam/AssignAssigndense_2/bias/Adam#dense_2/bias/Adam/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:

{
dense_2/bias/Adam/readIdentitydense_2/bias/Adam*
T0*
_class
loc:@dense_2/bias*
_output_shapes
:

 
5dense_2/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:
*
_class
loc:@dense_2/bias

+dense_2/bias/Adam_1/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *
_class
loc:@dense_2/bias
щ
%dense_2/bias/Adam_1/Initializer/zerosFill5dense_2/bias/Adam_1/Initializer/zeros/shape_as_tensor+dense_2/bias/Adam_1/Initializer/zeros/Const*
_output_shapes
:
*
T0*

index_type0*
_class
loc:@dense_2/bias
 
dense_2/bias/Adam_1
VariableV2*
	container *
shape:
*
dtype0*
_output_shapes
:
*
shared_name *
_class
loc:@dense_2/bias
Я
dense_2/bias/Adam_1/AssignAssigndense_2/bias/Adam_1%dense_2/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:


dense_2/bias/Adam_1/readIdentitydense_2/bias/Adam_1*
T0*
_class
loc:@dense_2/bias*
_output_shapes
:

Љ
5dense_3/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"
      *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
:

+dense_3/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
я
%dense_3/kernel/Adam/Initializer/zerosFill5dense_3/kernel/Adam/Initializer/zeros/shape_as_tensor+dense_3/kernel/Adam/Initializer/zeros/Const*
_output_shapes

:
*
T0*

index_type0*!
_class
loc:@dense_3/kernel
Њ
dense_3/kernel/Adam
VariableV2*!
_class
loc:@dense_3/kernel*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name 
е
dense_3/kernel/Adam/AssignAssigndense_3/kernel/Adam%dense_3/kernel/Adam/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:


dense_3/kernel/Adam/readIdentitydense_3/kernel/Adam*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

Ћ
7dense_3/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"
      *!
_class
loc:@dense_3/kernel

-dense_3/kernel/Adam_1/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *!
_class
loc:@dense_3/kernel
ѕ
'dense_3/kernel/Adam_1/Initializer/zerosFill7dense_3/kernel/Adam_1/Initializer/zeros/shape_as_tensor-dense_3/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@dense_3/kernel*
_output_shapes

:

Ќ
dense_3/kernel/Adam_1
VariableV2*
dtype0*
_output_shapes

:
*
shared_name *!
_class
loc:@dense_3/kernel*
	container *
shape
:

л
dense_3/kernel/Adam_1/AssignAssigndense_3/kernel/Adam_1'dense_3/kernel/Adam_1/Initializer/zeros*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(

dense_3/kernel/Adam_1/readIdentitydense_3/kernel/Adam_1*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

:


3dense_3/bias/Adam/Initializer/zeros/shape_as_tensorConst*
valueB:*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:

)dense_3/bias/Adam/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
у
#dense_3/bias/Adam/Initializer/zerosFill3dense_3/bias/Adam/Initializer/zeros/shape_as_tensor)dense_3/bias/Adam/Initializer/zeros/Const*
_output_shapes
:*
T0*

index_type0*
_class
loc:@dense_3/bias

dense_3/bias/Adam
VariableV2*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@dense_3/bias
Щ
dense_3/bias/Adam/AssignAssigndense_3/bias/Adam#dense_3/bias/Adam/Initializer/zeros*
use_locking(*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:
{
dense_3/bias/Adam/readIdentitydense_3/bias/Adam*
_output_shapes
:*
T0*
_class
loc:@dense_3/bias
 
5dense_3/bias/Adam_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB:*
_class
loc:@dense_3/bias

+dense_3/bias/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
щ
%dense_3/bias/Adam_1/Initializer/zerosFill5dense_3/bias/Adam_1/Initializer/zeros/shape_as_tensor+dense_3/bias/Adam_1/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@dense_3/bias*
_output_shapes
:
 
dense_3/bias/Adam_1
VariableV2*
shared_name *
_class
loc:@dense_3/bias*
	container *
shape:*
dtype0*
_output_shapes
:
Я
dense_3/bias/Adam_1/AssignAssigndense_3/bias/Adam_1%dense_3/bias/Adam_1/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_3/bias

dense_3/bias/Adam_1/readIdentitydense_3/bias/Adam_1*
T0*
_class
loc:@dense_3/bias*
_output_shapes
:
W
Adam/learning_rateConst*
valueB
 *
з#<*
dtype0*
_output_shapes
: 
O

Adam/beta1Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
O

Adam/beta2Const*
valueB
 *wО?*
dtype0*
_output_shapes
: 
Q
Adam/epsilonConst*
valueB
 *wЬ+2*
dtype0*
_output_shapes
: 
ь
"Adam/update_dense/kernel/ApplyAdam	ApplyAdamdense/kerneldense/kernel/Adamdense/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon6gradients/dense/MatMul_grad/tuple/control_dependency_1*
use_nesterov( *
_output_shapes

:
*
use_locking( *
T0*
_class
loc:@dense/kernel
п
 Adam/update_dense/bias/ApplyAdam	ApplyAdam
dense/biasdense/bias/Adamdense/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon7gradients/dense/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense/bias*
use_nesterov( *
_output_shapes
:
*
use_locking( 
ј
$Adam/update_dense_1/kernel/ApplyAdam	ApplyAdamdense_1/kerneldense_1/kernel/Adamdense_1/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_1/MatMul_grad/tuple/control_dependency_1*
T0*!
_class
loc:@dense_1/kernel*
use_nesterov( *
_output_shapes

:
*
use_locking( 
ы
"Adam/update_dense_1/bias/ApplyAdam	ApplyAdamdense_1/biasdense_1/bias/Adamdense_1/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*
_class
loc:@dense_1/bias*
use_nesterov( *
_output_shapes
:
ј
$Adam/update_dense_2/kernel/ApplyAdam	ApplyAdamdense_2/kerneldense_2/kernel/Adamdense_2/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_2/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*!
_class
loc:@dense_2/kernel*
use_nesterov( *
_output_shapes

:

ы
"Adam/update_dense_2/bias/ApplyAdam	ApplyAdamdense_2/biasdense_2/bias/Adamdense_2/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_2/BiasAdd_grad/tuple/control_dependency_1*
T0*
_class
loc:@dense_2/bias*
use_nesterov( *
_output_shapes
:
*
use_locking( 
ј
$Adam/update_dense_3/kernel/ApplyAdam	ApplyAdamdense_3/kerneldense_3/kernel/Adamdense_3/kernel/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon8gradients/dense_3/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*!
_class
loc:@dense_3/kernel*
use_nesterov( *
_output_shapes

:

ы
"Adam/update_dense_3/bias/ApplyAdam	ApplyAdamdense_3/biasdense_3/bias/Adamdense_3/bias/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon9gradients/dense_3/BiasAdd_grad/tuple/control_dependency_1*
use_nesterov( *
_output_shapes
:*
use_locking( *
T0*
_class
loc:@dense_3/bias

Adam/mulMulbeta1_power/read
Adam/beta1#^Adam/update_dense/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam*
_output_shapes
: *
T0*
_class
loc:@dense/bias

Adam/AssignAssignbeta1_powerAdam/mul*
validate_shape(*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@dense/bias


Adam/mul_1Mulbeta2_power/read
Adam/beta2#^Adam/update_dense/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam*
T0*
_class
loc:@dense/bias*
_output_shapes
: 

Adam/Assign_1Assignbeta2_power
Adam/mul_1*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: *
use_locking( 
н
Adam/updateNoOp#^Adam/update_dense/kernel/ApplyAdam!^Adam/update_dense/bias/ApplyAdam%^Adam/update_dense_1/kernel/ApplyAdam#^Adam/update_dense_1/bias/ApplyAdam%^Adam/update_dense_2/kernel/ApplyAdam#^Adam/update_dense_2/bias/ApplyAdam%^Adam/update_dense_3/kernel/ApplyAdam#^Adam/update_dense_3/bias/ApplyAdam^Adam/Assign^Adam/Assign_1
z

Adam/valueConst^Adam/update*
value	B	 R*
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
~
Adam	AssignAddglobal_step
Adam/value*
_output_shapes
: *
use_locking( *
T0	*
_class
loc:@global_step
Q
CastCastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0
\
EqualEqualCastfifo_queue_DequeueUpTo:2*
T0*#
_output_shapes
:џџџџџџџџџ
S
ToFloatCastEqual*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0

0accuracy/total/Initializer/zeros/shape_as_tensorConst*
valueB *!
_class
loc:@accuracy/total*
dtype0*
_output_shapes
: 

&accuracy/total/Initializer/zeros/ConstConst*
valueB
 *    *!
_class
loc:@accuracy/total*
dtype0*
_output_shapes
: 
и
 accuracy/total/Initializer/zerosFill0accuracy/total/Initializer/zeros/shape_as_tensor&accuracy/total/Initializer/zeros/Const*
_output_shapes
: *
T0*

index_type0*!
_class
loc:@accuracy/total

accuracy/total
VariableV2*
dtype0*
_output_shapes
: *
shared_name *!
_class
loc:@accuracy/total*
	container *
shape: 
О
accuracy/total/AssignAssignaccuracy/total accuracy/total/Initializer/zeros*
use_locking(*
T0*!
_class
loc:@accuracy/total*
validate_shape(*
_output_shapes
: 
s
accuracy/total/readIdentityaccuracy/total*
T0*!
_class
loc:@accuracy/total*
_output_shapes
: 

0accuracy/count/Initializer/zeros/shape_as_tensorConst*
valueB *!
_class
loc:@accuracy/count*
dtype0*
_output_shapes
: 

&accuracy/count/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *!
_class
loc:@accuracy/count
и
 accuracy/count/Initializer/zerosFill0accuracy/count/Initializer/zeros/shape_as_tensor&accuracy/count/Initializer/zeros/Const*
T0*

index_type0*!
_class
loc:@accuracy/count*
_output_shapes
: 

accuracy/count
VariableV2*
shared_name *!
_class
loc:@accuracy/count*
	container *
shape: *
dtype0*
_output_shapes
: 
О
accuracy/count/AssignAssignaccuracy/count accuracy/count/Initializer/zeros*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*!
_class
loc:@accuracy/count
s
accuracy/count/readIdentityaccuracy/count*
T0*!
_class
loc:@accuracy/count*
_output_shapes
: 
O
accuracy/SizeSizeToFloat*
T0*
out_type0*
_output_shapes
: 
Y
accuracy/ToFloat_1Castaccuracy/Size*

SrcT0*
_output_shapes
: *

DstT0
X
accuracy/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
j
accuracy/SumSumToFloataccuracy/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 

accuracy/AssignAdd	AssignAddaccuracy/totalaccuracy/Sum*
T0*!
_class
loc:@accuracy/total*
_output_shapes
: *
use_locking( 
І
accuracy/AssignAdd_1	AssignAddaccuracy/countaccuracy/ToFloat_1^ToFloat*
T0*!
_class
loc:@accuracy/count*
_output_shapes
: *
use_locking( 
f
accuracy/truedivRealDivaccuracy/total/readaccuracy/count/read*
T0*
_output_shapes
: 
f
#accuracy/zeros_like/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
^
accuracy/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

accuracy/zeros_likeFill#accuracy/zeros_like/shape_as_tensoraccuracy/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
f
accuracy/GreaterGreateraccuracy/count/readaccuracy/zeros_like*
T0*
_output_shapes
: 
r
accuracy/valueSelectaccuracy/Greateraccuracy/truedivaccuracy/zeros_like*
_output_shapes
: *
T0
h
accuracy/truediv_1RealDivaccuracy/AssignAddaccuracy/AssignAdd_1*
T0*
_output_shapes
: 
h
%accuracy/zeros_like_1/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB 
`
accuracy/zeros_like_1/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

accuracy/zeros_like_1Fill%accuracy/zeros_like_1/shape_as_tensoraccuracy/zeros_like_1/Const*
T0*

index_type0*
_output_shapes
: 
k
accuracy/Greater_1Greateraccuracy/AssignAdd_1accuracy/zeros_like_1*
_output_shapes
: *
T0
|
accuracy/update_opSelectaccuracy/Greater_1accuracy/truediv_1accuracy/zeros_like_1*
T0*
_output_shapes
: 
S
Cast_1CastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0
Z
subSubfifo_queue_DequeueUpTo:2Cast_1*
T0*#
_output_shapes
:џџџџџџџџџ
C
SquareSquaresub*
T0*#
_output_shapes
:џџџџџџџџџ
l
root_mean_squared_error/ToFloatCastSquare*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0
Д
?root_mean_squared_error/total/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB *0
_class&
$"loc:@root_mean_squared_error/total
Ќ
5root_mean_squared_error/total/Initializer/zeros/ConstConst*
valueB
 *    *0
_class&
$"loc:@root_mean_squared_error/total*
dtype0*
_output_shapes
: 

/root_mean_squared_error/total/Initializer/zerosFill?root_mean_squared_error/total/Initializer/zeros/shape_as_tensor5root_mean_squared_error/total/Initializer/zeros/Const*
_output_shapes
: *
T0*

index_type0*0
_class&
$"loc:@root_mean_squared_error/total
Г
root_mean_squared_error/total
VariableV2*0
_class&
$"loc:@root_mean_squared_error/total*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name 
њ
$root_mean_squared_error/total/AssignAssignroot_mean_squared_error/total/root_mean_squared_error/total/Initializer/zeros*
use_locking(*
T0*0
_class&
$"loc:@root_mean_squared_error/total*
validate_shape(*
_output_shapes
: 
 
"root_mean_squared_error/total/readIdentityroot_mean_squared_error/total*
T0*0
_class&
$"loc:@root_mean_squared_error/total*
_output_shapes
: 
Д
?root_mean_squared_error/count/Initializer/zeros/shape_as_tensorConst*
valueB *0
_class&
$"loc:@root_mean_squared_error/count*
dtype0*
_output_shapes
: 
Ќ
5root_mean_squared_error/count/Initializer/zeros/ConstConst*
valueB
 *    *0
_class&
$"loc:@root_mean_squared_error/count*
dtype0*
_output_shapes
: 

/root_mean_squared_error/count/Initializer/zerosFill?root_mean_squared_error/count/Initializer/zeros/shape_as_tensor5root_mean_squared_error/count/Initializer/zeros/Const*
T0*

index_type0*0
_class&
$"loc:@root_mean_squared_error/count*
_output_shapes
: 
Г
root_mean_squared_error/count
VariableV2*
dtype0*
_output_shapes
: *
shared_name *0
_class&
$"loc:@root_mean_squared_error/count*
	container *
shape: 
њ
$root_mean_squared_error/count/AssignAssignroot_mean_squared_error/count/root_mean_squared_error/count/Initializer/zeros*
use_locking(*
T0*0
_class&
$"loc:@root_mean_squared_error/count*
validate_shape(*
_output_shapes
: 
 
"root_mean_squared_error/count/readIdentityroot_mean_squared_error/count*
T0*0
_class&
$"loc:@root_mean_squared_error/count*
_output_shapes
: 
v
root_mean_squared_error/SizeSizeroot_mean_squared_error/ToFloat*
T0*
out_type0*
_output_shapes
: 
w
!root_mean_squared_error/ToFloat_1Castroot_mean_squared_error/Size*

SrcT0*
_output_shapes
: *

DstT0
g
root_mean_squared_error/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
 
root_mean_squared_error/SumSumroot_mean_squared_error/ToFloatroot_mean_squared_error/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
а
!root_mean_squared_error/AssignAdd	AssignAddroot_mean_squared_error/totalroot_mean_squared_error/Sum*
use_locking( *
T0*0
_class&
$"loc:@root_mean_squared_error/total*
_output_shapes
: 
њ
#root_mean_squared_error/AssignAdd_1	AssignAddroot_mean_squared_error/count!root_mean_squared_error/ToFloat_1 ^root_mean_squared_error/ToFloat*
_output_shapes
: *
use_locking( *
T0*0
_class&
$"loc:@root_mean_squared_error/count

root_mean_squared_error/truedivRealDiv"root_mean_squared_error/total/read"root_mean_squared_error/count/read*
T0*
_output_shapes
: 
u
2root_mean_squared_error/zeros_like/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB 
m
(root_mean_squared_error/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
Л
"root_mean_squared_error/zeros_likeFill2root_mean_squared_error/zeros_like/shape_as_tensor(root_mean_squared_error/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 

root_mean_squared_error/GreaterGreater"root_mean_squared_error/count/read"root_mean_squared_error/zeros_like*
T0*
_output_shapes
: 
Ў
root_mean_squared_error/valueSelectroot_mean_squared_error/Greaterroot_mean_squared_error/truediv"root_mean_squared_error/zeros_like*
T0*
_output_shapes
: 

!root_mean_squared_error/truediv_1RealDiv!root_mean_squared_error/AssignAdd#root_mean_squared_error/AssignAdd_1*
T0*
_output_shapes
: 
w
4root_mean_squared_error/zeros_like_1/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
o
*root_mean_squared_error/zeros_like_1/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
С
$root_mean_squared_error/zeros_like_1Fill4root_mean_squared_error/zeros_like_1/shape_as_tensor*root_mean_squared_error/zeros_like_1/Const*
_output_shapes
: *
T0*

index_type0

!root_mean_squared_error/Greater_1Greater#root_mean_squared_error/AssignAdd_1$root_mean_squared_error/zeros_like_1*
T0*
_output_shapes
: 
И
!root_mean_squared_error/update_opSelect!root_mean_squared_error/Greater_1!root_mean_squared_error/truediv_1$root_mean_squared_error/zeros_like_1*
T0*
_output_shapes
: 
L
SqrtSqrtroot_mean_squared_error/value*
_output_shapes
: *
T0
R
Sqrt_1Sqrt!root_mean_squared_error/update_op*
T0*
_output_shapes
: 
e
Cast_2Castfifo_queue_DequeueUpTo:2*

SrcT0*#
_output_shapes
:џџџџџџџџџ*

DstT0
X
recall/CastCastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0

Z
recall/Cast_1CastCast_2*#
_output_shapes
:џџџџџџџџџ*

DstT0
*

SrcT0
_
recall/true_positives/Equal/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

recall/true_positives/EqualEqualrecall/Cast_1recall/true_positives/Equal/y*
T0
*#
_output_shapes
:џџџџџџџџџ
a
recall/true_positives/Equal_1/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

recall/true_positives/Equal_1Equalrecall/Castrecall/true_positives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

 recall/true_positives/LogicalAnd
LogicalAndrecall/true_positives/Equalrecall/true_positives/Equal_1*#
_output_shapes
:џџџџџџџџџ
L
Drecall/true_positives/assert_type/statically_determined_correct_typeNoOp
А
=recall/true_positives/count/Initializer/zeros/shape_as_tensorConst*
valueB *.
_class$
" loc:@recall/true_positives/count*
dtype0*
_output_shapes
: 
Ј
3recall/true_positives/count/Initializer/zeros/ConstConst*
valueB
 *    *.
_class$
" loc:@recall/true_positives/count*
dtype0*
_output_shapes
: 

-recall/true_positives/count/Initializer/zerosFill=recall/true_positives/count/Initializer/zeros/shape_as_tensor3recall/true_positives/count/Initializer/zeros/Const*
T0*

index_type0*.
_class$
" loc:@recall/true_positives/count*
_output_shapes
: 
Џ
recall/true_positives/count
VariableV2*
shared_name *.
_class$
" loc:@recall/true_positives/count*
	container *
shape: *
dtype0*
_output_shapes
: 
ђ
"recall/true_positives/count/AssignAssignrecall/true_positives/count-recall/true_positives/count/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@recall/true_positives/count*
validate_shape(*
_output_shapes
: 

 recall/true_positives/count/readIdentityrecall/true_positives/count*
T0*.
_class$
" loc:@recall/true_positives/count*
_output_shapes
: 

recall/true_positives/ToFloatCast recall/true_positives/LogicalAnd*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0

m
recall/true_positives/IdentityIdentity recall/true_positives/count/read*
T0*
_output_shapes
: 
e
recall/true_positives/ConstConst*
dtype0*
_output_shapes
:*
valueB: 

recall/true_positives/SumSumrecall/true_positives/ToFloatrecall/true_positives/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
Ш
recall/true_positives/AssignAdd	AssignAddrecall/true_positives/countrecall/true_positives/Sum*
use_locking( *
T0*.
_class$
" loc:@recall/true_positives/count*
_output_shapes
: 
`
recall/false_negatives/Equal/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

recall/false_negatives/EqualEqualrecall/Cast_1recall/false_negatives/Equal/y*
T0
*#
_output_shapes
:џџџџџџџџџ
b
 recall/false_negatives/Equal_1/yConst*
value	B
 Z *
dtype0
*
_output_shapes
: 

recall/false_negatives/Equal_1Equalrecall/Cast recall/false_negatives/Equal_1/y*#
_output_shapes
:џџџџџџџџџ*
T0


!recall/false_negatives/LogicalAnd
LogicalAndrecall/false_negatives/Equalrecall/false_negatives/Equal_1*#
_output_shapes
:џџџџџџџџџ
M
Erecall/false_negatives/assert_type/statically_determined_correct_typeNoOp
В
>recall/false_negatives/count/Initializer/zeros/shape_as_tensorConst*
valueB */
_class%
#!loc:@recall/false_negatives/count*
dtype0*
_output_shapes
: 
Њ
4recall/false_negatives/count/Initializer/zeros/ConstConst*
valueB
 *    */
_class%
#!loc:@recall/false_negatives/count*
dtype0*
_output_shapes
: 

.recall/false_negatives/count/Initializer/zerosFill>recall/false_negatives/count/Initializer/zeros/shape_as_tensor4recall/false_negatives/count/Initializer/zeros/Const*
T0*

index_type0*/
_class%
#!loc:@recall/false_negatives/count*
_output_shapes
: 
Б
recall/false_negatives/count
VariableV2*
shared_name */
_class%
#!loc:@recall/false_negatives/count*
	container *
shape: *
dtype0*
_output_shapes
: 
і
#recall/false_negatives/count/AssignAssignrecall/false_negatives/count.recall/false_negatives/count/Initializer/zeros*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*/
_class%
#!loc:@recall/false_negatives/count

!recall/false_negatives/count/readIdentityrecall/false_negatives/count*
T0*/
_class%
#!loc:@recall/false_negatives/count*
_output_shapes
: 

recall/false_negatives/ToFloatCast!recall/false_negatives/LogicalAnd*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0

o
recall/false_negatives/IdentityIdentity!recall/false_negatives/count/read*
_output_shapes
: *
T0
f
recall/false_negatives/ConstConst*
valueB: *
dtype0*
_output_shapes
:

recall/false_negatives/SumSumrecall/false_negatives/ToFloatrecall/false_negatives/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
Ь
 recall/false_negatives/AssignAdd	AssignAddrecall/false_negatives/countrecall/false_negatives/Sum*
_output_shapes
: *
use_locking( *
T0*/
_class%
#!loc:@recall/false_negatives/count
s

recall/addAddrecall/true_positives/Identityrecall/false_negatives/Identity*
T0*
_output_shapes
: 
U
recall/Greater/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
X
recall/GreaterGreater
recall/addrecall/Greater/y*
_output_shapes
: *
T0
u
recall/add_1Addrecall/true_positives/Identityrecall/false_negatives/Identity*
_output_shapes
: *
T0
d

recall/divRealDivrecall/true_positives/Identityrecall/add_1*
_output_shapes
: *
T0
S
recall/value/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
c
recall/valueSelectrecall/Greater
recall/divrecall/value/e*
T0*
_output_shapes
: 
w
recall/add_2Addrecall/true_positives/AssignAdd recall/false_negatives/AssignAdd*
T0*
_output_shapes
: 
W
recall/Greater_1/yConst*
dtype0*
_output_shapes
: *
valueB
 *    
^
recall/Greater_1Greaterrecall/add_2recall/Greater_1/y*
T0*
_output_shapes
: 
w
recall/add_3Addrecall/true_positives/AssignAdd recall/false_negatives/AssignAdd*
T0*
_output_shapes
: 
g
recall/div_1RealDivrecall/true_positives/AssignAddrecall/add_3*
T0*
_output_shapes
: 
W
recall/update_op/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
o
recall/update_opSelectrecall/Greater_1recall/div_1recall/update_op/e*
T0*
_output_shapes
: 
e
Cast_3Castfifo_queue_DequeueUpTo:2*#
_output_shapes
:џџџџџџџџџ*

DstT0*

SrcT0
[
precision/CastCastArgMax*

SrcT0	*#
_output_shapes
:џџџџџџџџџ*

DstT0

]
precision/Cast_1CastCast_3*#
_output_shapes
:џџџџџџџџџ*

DstT0
*

SrcT0
b
 precision/true_positives/Equal/yConst*
dtype0
*
_output_shapes
: *
value	B
 Z

precision/true_positives/EqualEqualprecision/Cast_1 precision/true_positives/Equal/y*
T0
*#
_output_shapes
:џџџџџџџџџ
d
"precision/true_positives/Equal_1/yConst*
value	B
 Z*
dtype0
*
_output_shapes
: 

 precision/true_positives/Equal_1Equalprecision/Cast"precision/true_positives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

#precision/true_positives/LogicalAnd
LogicalAndprecision/true_positives/Equal precision/true_positives/Equal_1*#
_output_shapes
:џџџџџџџџџ
O
Gprecision/true_positives/assert_type/statically_determined_correct_typeNoOp
Ж
@precision/true_positives/count/Initializer/zeros/shape_as_tensorConst*
valueB *1
_class'
%#loc:@precision/true_positives/count*
dtype0*
_output_shapes
: 
Ў
6precision/true_positives/count/Initializer/zeros/ConstConst*
valueB
 *    *1
_class'
%#loc:@precision/true_positives/count*
dtype0*
_output_shapes
: 

0precision/true_positives/count/Initializer/zerosFill@precision/true_positives/count/Initializer/zeros/shape_as_tensor6precision/true_positives/count/Initializer/zeros/Const*
T0*

index_type0*1
_class'
%#loc:@precision/true_positives/count*
_output_shapes
: 
Е
precision/true_positives/count
VariableV2*
dtype0*
_output_shapes
: *
shared_name *1
_class'
%#loc:@precision/true_positives/count*
	container *
shape: 
ў
%precision/true_positives/count/AssignAssignprecision/true_positives/count0precision/true_positives/count/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@precision/true_positives/count*
validate_shape(*
_output_shapes
: 
Ѓ
#precision/true_positives/count/readIdentityprecision/true_positives/count*
T0*1
_class'
%#loc:@precision/true_positives/count*
_output_shapes
: 

 precision/true_positives/ToFloatCast#precision/true_positives/LogicalAnd*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0
s
!precision/true_positives/IdentityIdentity#precision/true_positives/count/read*
T0*
_output_shapes
: 
h
precision/true_positives/ConstConst*
valueB: *
dtype0*
_output_shapes
:
Ѓ
precision/true_positives/SumSum precision/true_positives/ToFloatprecision/true_positives/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
д
"precision/true_positives/AssignAdd	AssignAddprecision/true_positives/countprecision/true_positives/Sum*
T0*1
_class'
%#loc:@precision/true_positives/count*
_output_shapes
: *
use_locking( 
c
!precision/false_positives/Equal/yConst*
value	B
 Z *
dtype0
*
_output_shapes
: 

precision/false_positives/EqualEqualprecision/Cast_1!precision/false_positives/Equal/y*
T0
*#
_output_shapes
:џџџџџџџџџ
e
#precision/false_positives/Equal_1/yConst*
dtype0
*
_output_shapes
: *
value	B
 Z

!precision/false_positives/Equal_1Equalprecision/Cast#precision/false_positives/Equal_1/y*
T0
*#
_output_shapes
:џџџџџџџџџ

$precision/false_positives/LogicalAnd
LogicalAndprecision/false_positives/Equal!precision/false_positives/Equal_1*#
_output_shapes
:џџџџџџџџџ
P
Hprecision/false_positives/assert_type/statically_determined_correct_typeNoOp
И
Aprecision/false_positives/count/Initializer/zeros/shape_as_tensorConst*
valueB *2
_class(
&$loc:@precision/false_positives/count*
dtype0*
_output_shapes
: 
А
7precision/false_positives/count/Initializer/zeros/ConstConst*
valueB
 *    *2
_class(
&$loc:@precision/false_positives/count*
dtype0*
_output_shapes
: 

1precision/false_positives/count/Initializer/zerosFillAprecision/false_positives/count/Initializer/zeros/shape_as_tensor7precision/false_positives/count/Initializer/zeros/Const*
T0*

index_type0*2
_class(
&$loc:@precision/false_positives/count*
_output_shapes
: 
З
precision/false_positives/count
VariableV2*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name *2
_class(
&$loc:@precision/false_positives/count

&precision/false_positives/count/AssignAssignprecision/false_positives/count1precision/false_positives/count/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@precision/false_positives/count*
validate_shape(*
_output_shapes
: 
І
$precision/false_positives/count/readIdentityprecision/false_positives/count*
T0*2
_class(
&$loc:@precision/false_positives/count*
_output_shapes
: 

!precision/false_positives/ToFloatCast$precision/false_positives/LogicalAnd*

SrcT0
*#
_output_shapes
:џџџџџџџџџ*

DstT0
u
"precision/false_positives/IdentityIdentity$precision/false_positives/count/read*
T0*
_output_shapes
: 
i
precision/false_positives/ConstConst*
valueB: *
dtype0*
_output_shapes
:
І
precision/false_positives/SumSum!precision/false_positives/ToFloatprecision/false_positives/Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
и
#precision/false_positives/AssignAdd	AssignAddprecision/false_positives/countprecision/false_positives/Sum*
_output_shapes
: *
use_locking( *
T0*2
_class(
&$loc:@precision/false_positives/count
|
precision/addAdd!precision/true_positives/Identity"precision/false_positives/Identity*
T0*
_output_shapes
: 
X
precision/Greater/yConst*
valueB
 *    *
dtype0*
_output_shapes
: 
a
precision/GreaterGreaterprecision/addprecision/Greater/y*
T0*
_output_shapes
: 
~
precision/add_1Add!precision/true_positives/Identity"precision/false_positives/Identity*
T0*
_output_shapes
: 
m
precision/divRealDiv!precision/true_positives/Identityprecision/add_1*
T0*
_output_shapes
: 
V
precision/value/eConst*
dtype0*
_output_shapes
: *
valueB
 *    
o
precision/valueSelectprecision/Greaterprecision/divprecision/value/e*
T0*
_output_shapes
: 

precision/add_2Add"precision/true_positives/AssignAdd#precision/false_positives/AssignAdd*
_output_shapes
: *
T0
Z
precision/Greater_1/yConst*
dtype0*
_output_shapes
: *
valueB
 *    
g
precision/Greater_1Greaterprecision/add_2precision/Greater_1/y*
T0*
_output_shapes
: 

precision/add_3Add"precision/true_positives/AssignAdd#precision/false_positives/AssignAdd*
_output_shapes
: *
T0
p
precision/div_1RealDiv"precision/true_positives/AssignAddprecision/add_3*
T0*
_output_shapes
: 
Z
precision/update_op/eConst*
valueB
 *    *
dtype0*
_output_shapes
: 
{
precision/update_opSelectprecision/Greater_1precision/div_1precision/update_op/e*
_output_shapes
: *
T0

,mean/total/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@mean/total*
dtype0*
_output_shapes
: 

"mean/total/Initializer/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    *
_class
loc:@mean/total
Ш
mean/total/Initializer/zerosFill,mean/total/Initializer/zeros/shape_as_tensor"mean/total/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@mean/total*
_output_shapes
: 


mean/total
VariableV2*
dtype0*
_output_shapes
: *
shared_name *
_class
loc:@mean/total*
	container *
shape: 
Ў
mean/total/AssignAssign
mean/totalmean/total/Initializer/zeros*
use_locking(*
T0*
_class
loc:@mean/total*
validate_shape(*
_output_shapes
: 
g
mean/total/readIdentity
mean/total*
T0*
_class
loc:@mean/total*
_output_shapes
: 

,mean/count/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@mean/count*
dtype0*
_output_shapes
: 

"mean/count/Initializer/zeros/ConstConst*
valueB
 *    *
_class
loc:@mean/count*
dtype0*
_output_shapes
: 
Ш
mean/count/Initializer/zerosFill,mean/count/Initializer/zeros/shape_as_tensor"mean/count/Initializer/zeros/Const*
T0*

index_type0*
_class
loc:@mean/count*
_output_shapes
: 


mean/count
VariableV2*
_class
loc:@mean/count*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name 
Ў
mean/count/AssignAssign
mean/countmean/count/Initializer/zeros*
use_locking(*
T0*
_class
loc:@mean/count*
validate_shape(*
_output_shapes
: 
g
mean/count/readIdentity
mean/count*
T0*
_class
loc:@mean/count*
_output_shapes
: 
K
	mean/SizeConst*
value	B :*
dtype0*
_output_shapes
: 
Q
mean/ToFloat_1Cast	mean/Size*

SrcT0*
_output_shapes
: *

DstT0
M

mean/ConstConst*
valueB *
dtype0*
_output_shapes
: 
{
mean/SumSum sigmoid_cross_entropy_loss/value
mean/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 

mean/AssignAdd	AssignAdd
mean/totalmean/Sum*
use_locking( *
T0*
_class
loc:@mean/total*
_output_shapes
: 
Џ
mean/AssignAdd_1	AssignAdd
mean/countmean/ToFloat_1!^sigmoid_cross_entropy_loss/value*
use_locking( *
T0*
_class
loc:@mean/count*
_output_shapes
: 
Z
mean/truedivRealDivmean/total/readmean/count/read*
T0*
_output_shapes
: 
b
mean/zeros_like/shape_as_tensorConst*
valueB *
dtype0*
_output_shapes
: 
Z
mean/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    

mean/zeros_likeFillmean/zeros_like/shape_as_tensormean/zeros_like/Const*
T0*

index_type0*
_output_shapes
: 
Z
mean/GreaterGreatermean/count/readmean/zeros_like*
T0*
_output_shapes
: 
b

mean/valueSelectmean/Greatermean/truedivmean/zeros_like*
T0*
_output_shapes
: 
\
mean/truediv_1RealDivmean/AssignAddmean/AssignAdd_1*
T0*
_output_shapes
: 
d
!mean/zeros_like_1/shape_as_tensorConst*
dtype0*
_output_shapes
: *
valueB 
\
mean/zeros_like_1/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

mean/zeros_like_1Fill!mean/zeros_like_1/shape_as_tensormean/zeros_like_1/Const*
T0*

index_type0*
_output_shapes
: 
_
mean/Greater_1Greatermean/AssignAdd_1mean/zeros_like_1*
T0*
_output_shapes
: 
l
mean/update_opSelectmean/Greater_1mean/truediv_1mean/zeros_like_1*
T0*
_output_shapes
: 
j

group_depsNoOp^accuracy/update_op^mean/update_op^precision/update_op^Sqrt_1^recall/update_op

+eval_step/Initializer/zeros/shape_as_tensorConst*
valueB *
_class
loc:@eval_step*
dtype0*
_output_shapes
: 

!eval_step/Initializer/zeros/ConstConst*
value	B	 R *
_class
loc:@eval_step*
dtype0	*
_output_shapes
: 
Ф
eval_step/Initializer/zerosFill+eval_step/Initializer/zeros/shape_as_tensor!eval_step/Initializer/zeros/Const*
_output_shapes
: *
T0	*

index_type0*
_class
loc:@eval_step

	eval_step
VariableV2*
shape: *
dtype0	*
_output_shapes
: *
shared_name *
_class
loc:@eval_step*
	container 
Њ
eval_step/AssignAssign	eval_stepeval_step/Initializer/zeros*
T0	*
_class
loc:@eval_step*
validate_shape(*
_output_shapes
: *
use_locking(
d
eval_step/readIdentity	eval_step*
_output_shapes
: *
T0	*
_class
loc:@eval_step
Q
AssignAdd/valueConst*
dtype0	*
_output_shapes
: *
value	B	 R

	AssignAdd	AssignAdd	eval_stepAssignAdd/value*
use_locking(*
T0	*
_class
loc:@eval_step*
_output_shapes
: 
U
readIdentity	eval_step^group_deps
^AssignAdd*
_output_shapes
: *
T0	
;
IdentityIdentityread*
T0	*
_output_shapes
: 
Ч
initNoOp^global_step/Assign^dense/kernel/Assign^dense/bias/Assign^dense_1/kernel/Assign^dense_1/bias/Assign^dense_2/kernel/Assign^dense_2/bias/Assign^dense_3/kernel/Assign^dense_3/bias/Assign^beta1_power/Assign^beta2_power/Assign^dense/kernel/Adam/Assign^dense/kernel/Adam_1/Assign^dense/bias/Adam/Assign^dense/bias/Adam_1/Assign^dense_1/kernel/Adam/Assign^dense_1/kernel/Adam_1/Assign^dense_1/bias/Adam/Assign^dense_1/bias/Adam_1/Assign^dense_2/kernel/Adam/Assign^dense_2/kernel/Adam_1/Assign^dense_2/bias/Adam/Assign^dense_2/bias/Adam_1/Assign^dense_3/kernel/Adam/Assign^dense_3/kernel/Adam_1/Assign^dense_3/bias/Adam/Assign^dense_3/bias/Adam_1/Assign

init_1NoOp
$
group_deps_1NoOp^init^init_1

4report_uninitialized_variables/IsVariableInitializedIsVariableInitializedglobal_step*
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_1IsVariableInitializeddense/kernel*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 

6report_uninitialized_variables/IsVariableInitialized_2IsVariableInitialized
dense/bias*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ї
6report_uninitialized_variables/IsVariableInitialized_3IsVariableInitializeddense_1/kernel*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_4IsVariableInitializeddense_1/bias*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
Ї
6report_uninitialized_variables/IsVariableInitialized_5IsVariableInitializeddense_2/kernel*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_6IsVariableInitializeddense_2/bias*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
Ї
6report_uninitialized_variables/IsVariableInitialized_7IsVariableInitializeddense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Ѓ
6report_uninitialized_variables/IsVariableInitialized_8IsVariableInitializeddense_3/bias*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
 
6report_uninitialized_variables/IsVariableInitialized_9IsVariableInitializedbeta1_power*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ё
7report_uninitialized_variables/IsVariableInitialized_10IsVariableInitializedbeta2_power*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Љ
7report_uninitialized_variables/IsVariableInitialized_11IsVariableInitializeddense/kernel/Adam*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
Ћ
7report_uninitialized_variables/IsVariableInitialized_12IsVariableInitializeddense/kernel/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense/kernel
Ѕ
7report_uninitialized_variables/IsVariableInitialized_13IsVariableInitializeddense/bias/Adam*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ї
7report_uninitialized_variables/IsVariableInitialized_14IsVariableInitializeddense/bias/Adam_1*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
­
7report_uninitialized_variables/IsVariableInitialized_15IsVariableInitializeddense_1/kernel/Adam*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Џ
7report_uninitialized_variables/IsVariableInitialized_16IsVariableInitializeddense_1/kernel/Adam_1*
dtype0*
_output_shapes
: *!
_class
loc:@dense_1/kernel
Љ
7report_uninitialized_variables/IsVariableInitialized_17IsVariableInitializeddense_1/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense_1/bias
Ћ
7report_uninitialized_variables/IsVariableInitialized_18IsVariableInitializeddense_1/bias/Adam_1*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
­
7report_uninitialized_variables/IsVariableInitialized_19IsVariableInitializeddense_2/kernel/Adam*
dtype0*
_output_shapes
: *!
_class
loc:@dense_2/kernel
Џ
7report_uninitialized_variables/IsVariableInitialized_20IsVariableInitializeddense_2/kernel/Adam_1*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Љ
7report_uninitialized_variables/IsVariableInitialized_21IsVariableInitializeddense_2/bias/Adam*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
Ћ
7report_uninitialized_variables/IsVariableInitialized_22IsVariableInitializeddense_2/bias/Adam_1*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
­
7report_uninitialized_variables/IsVariableInitialized_23IsVariableInitializeddense_3/kernel/Adam*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Џ
7report_uninitialized_variables/IsVariableInitialized_24IsVariableInitializeddense_3/kernel/Adam_1*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Љ
7report_uninitialized_variables/IsVariableInitialized_25IsVariableInitializeddense_3/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense_3/bias
Ћ
7report_uninitialized_variables/IsVariableInitialized_26IsVariableInitializeddense_3/bias/Adam_1*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
Ј
7report_uninitialized_variables/IsVariableInitialized_27IsVariableInitializedaccuracy/total*!
_class
loc:@accuracy/total*
dtype0*
_output_shapes
: 
Ј
7report_uninitialized_variables/IsVariableInitialized_28IsVariableInitializedaccuracy/count*!
_class
loc:@accuracy/count*
dtype0*
_output_shapes
: 
Ц
7report_uninitialized_variables/IsVariableInitialized_29IsVariableInitializedroot_mean_squared_error/total*0
_class&
$"loc:@root_mean_squared_error/total*
dtype0*
_output_shapes
: 
Ц
7report_uninitialized_variables/IsVariableInitialized_30IsVariableInitializedroot_mean_squared_error/count*0
_class&
$"loc:@root_mean_squared_error/count*
dtype0*
_output_shapes
: 
Т
7report_uninitialized_variables/IsVariableInitialized_31IsVariableInitializedrecall/true_positives/count*.
_class$
" loc:@recall/true_positives/count*
dtype0*
_output_shapes
: 
Ф
7report_uninitialized_variables/IsVariableInitialized_32IsVariableInitializedrecall/false_negatives/count*/
_class%
#!loc:@recall/false_negatives/count*
dtype0*
_output_shapes
: 
Ш
7report_uninitialized_variables/IsVariableInitialized_33IsVariableInitializedprecision/true_positives/count*1
_class'
%#loc:@precision/true_positives/count*
dtype0*
_output_shapes
: 
Ъ
7report_uninitialized_variables/IsVariableInitialized_34IsVariableInitializedprecision/false_positives/count*2
_class(
&$loc:@precision/false_positives/count*
dtype0*
_output_shapes
: 
 
7report_uninitialized_variables/IsVariableInitialized_35IsVariableInitialized
mean/total*
_class
loc:@mean/total*
dtype0*
_output_shapes
: 
 
7report_uninitialized_variables/IsVariableInitialized_36IsVariableInitialized
mean/count*
_class
loc:@mean/count*
dtype0*
_output_shapes
: 

7report_uninitialized_variables/IsVariableInitialized_37IsVariableInitialized	eval_step*
dtype0	*
_output_shapes
: *
_class
loc:@eval_step
п
$report_uninitialized_variables/stackPack4report_uninitialized_variables/IsVariableInitialized6report_uninitialized_variables/IsVariableInitialized_16report_uninitialized_variables/IsVariableInitialized_26report_uninitialized_variables/IsVariableInitialized_36report_uninitialized_variables/IsVariableInitialized_46report_uninitialized_variables/IsVariableInitialized_56report_uninitialized_variables/IsVariableInitialized_66report_uninitialized_variables/IsVariableInitialized_76report_uninitialized_variables/IsVariableInitialized_86report_uninitialized_variables/IsVariableInitialized_97report_uninitialized_variables/IsVariableInitialized_107report_uninitialized_variables/IsVariableInitialized_117report_uninitialized_variables/IsVariableInitialized_127report_uninitialized_variables/IsVariableInitialized_137report_uninitialized_variables/IsVariableInitialized_147report_uninitialized_variables/IsVariableInitialized_157report_uninitialized_variables/IsVariableInitialized_167report_uninitialized_variables/IsVariableInitialized_177report_uninitialized_variables/IsVariableInitialized_187report_uninitialized_variables/IsVariableInitialized_197report_uninitialized_variables/IsVariableInitialized_207report_uninitialized_variables/IsVariableInitialized_217report_uninitialized_variables/IsVariableInitialized_227report_uninitialized_variables/IsVariableInitialized_237report_uninitialized_variables/IsVariableInitialized_247report_uninitialized_variables/IsVariableInitialized_257report_uninitialized_variables/IsVariableInitialized_267report_uninitialized_variables/IsVariableInitialized_277report_uninitialized_variables/IsVariableInitialized_287report_uninitialized_variables/IsVariableInitialized_297report_uninitialized_variables/IsVariableInitialized_307report_uninitialized_variables/IsVariableInitialized_317report_uninitialized_variables/IsVariableInitialized_327report_uninitialized_variables/IsVariableInitialized_337report_uninitialized_variables/IsVariableInitialized_347report_uninitialized_variables/IsVariableInitialized_357report_uninitialized_variables/IsVariableInitialized_367report_uninitialized_variables/IsVariableInitialized_37"/device:CPU:0*
N&*
_output_shapes
:&*
T0
*

axis 

)report_uninitialized_variables/LogicalNot
LogicalNot$report_uninitialized_variables/stack"/device:CPU:0*
_output_shapes
:&
н
$report_uninitialized_variables/ConstConst"/device:CPU:0*ѕ
valueыBш&Bglobal_stepBdense/kernelB
dense/biasBdense_1/kernelBdense_1/biasBdense_2/kernelBdense_2/biasBdense_3/kernelBdense_3/biasBbeta1_powerBbeta2_powerBdense/kernel/AdamBdense/kernel/Adam_1Bdense/bias/AdamBdense/bias/Adam_1Bdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_3/kernel/AdamBdense_3/kernel/Adam_1Bdense_3/bias/AdamBdense_3/bias/Adam_1Baccuracy/totalBaccuracy/countBroot_mean_squared_error/totalBroot_mean_squared_error/countBrecall/true_positives/countBrecall/false_negatives/countBprecision/true_positives/countBprecision/false_positives/countB
mean/totalB
mean/countB	eval_step*
dtype0*
_output_shapes
:&

1report_uninitialized_variables/boolean_mask/ShapeConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:&

?report_uninitialized_variables/boolean_mask/strided_slice/stackConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Areport_uninitialized_variables/boolean_mask/strided_slice/stack_1Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Areport_uninitialized_variables/boolean_mask/strided_slice/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
ш
9report_uninitialized_variables/boolean_mask/strided_sliceStridedSlice1report_uninitialized_variables/boolean_mask/Shape?report_uninitialized_variables/boolean_mask/strided_slice/stackAreport_uninitialized_variables/boolean_mask/strided_slice/stack_1Areport_uninitialized_variables/boolean_mask/strided_slice/stack_2"/device:CPU:0*
T0*
Index0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
:

Breport_uninitialized_variables/boolean_mask/Prod/reduction_indicesConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

0report_uninitialized_variables/boolean_mask/ProdProd9report_uninitialized_variables/boolean_mask/strided_sliceBreport_uninitialized_variables/boolean_mask/Prod/reduction_indices"/device:CPU:0*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 

3report_uninitialized_variables/boolean_mask/Shape_1Const"/device:CPU:0*
valueB:&*
dtype0*
_output_shapes
:

Areport_uninitialized_variables/boolean_mask/strided_slice_1/stackConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_1Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
№
;report_uninitialized_variables/boolean_mask/strided_slice_1StridedSlice3report_uninitialized_variables/boolean_mask/Shape_1Areport_uninitialized_variables/boolean_mask/strided_slice_1/stackCreport_uninitialized_variables/boolean_mask/strided_slice_1/stack_1Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_2"/device:CPU:0*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask 

3report_uninitialized_variables/boolean_mask/Shape_2Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:&

Areport_uninitialized_variables/boolean_mask/strided_slice_2/stackConst"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_1Const"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_2Const"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:
№
;report_uninitialized_variables/boolean_mask/strided_slice_2StridedSlice3report_uninitialized_variables/boolean_mask/Shape_2Areport_uninitialized_variables/boolean_mask/strided_slice_2/stackCreport_uninitialized_variables/boolean_mask/strided_slice_2/stack_1Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_2"/device:CPU:0*
end_mask*
_output_shapes
: *
Index0*
T0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask 
О
;report_uninitialized_variables/boolean_mask/concat/values_1Pack0report_uninitialized_variables/boolean_mask/Prod"/device:CPU:0*
T0*

axis *
N*
_output_shapes
:

7report_uninitialized_variables/boolean_mask/concat/axisConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 
ї
2report_uninitialized_variables/boolean_mask/concatConcatV2;report_uninitialized_variables/boolean_mask/strided_slice_1;report_uninitialized_variables/boolean_mask/concat/values_1;report_uninitialized_variables/boolean_mask/strided_slice_27report_uninitialized_variables/boolean_mask/concat/axis"/device:CPU:0*
N*
_output_shapes
:*

Tidx0*
T0
к
3report_uninitialized_variables/boolean_mask/ReshapeReshape$report_uninitialized_variables/Const2report_uninitialized_variables/boolean_mask/concat"/device:CPU:0*
T0*
Tshape0*
_output_shapes
:&

;report_uninitialized_variables/boolean_mask/Reshape_1/shapeConst"/device:CPU:0*
valueB:
џџџџџџџџџ*
dtype0*
_output_shapes
:
ъ
5report_uninitialized_variables/boolean_mask/Reshape_1Reshape)report_uninitialized_variables/LogicalNot;report_uninitialized_variables/boolean_mask/Reshape_1/shape"/device:CPU:0*
_output_shapes
:&*
T0
*
Tshape0
В
1report_uninitialized_variables/boolean_mask/WhereWhere5report_uninitialized_variables/boolean_mask/Reshape_1"/device:CPU:0*'
_output_shapes
:џџџџџџџџџ*
T0

Х
3report_uninitialized_variables/boolean_mask/SqueezeSqueeze1report_uninitialized_variables/boolean_mask/Where"/device:CPU:0*
squeeze_dims
*
T0	*#
_output_shapes
:џџџџџџџџџ

2report_uninitialized_variables/boolean_mask/GatherGather3report_uninitialized_variables/boolean_mask/Reshape3report_uninitialized_variables/boolean_mask/Squeeze"/device:CPU:0*
Tindices0	*
Tparams0*
validate_indices(*#
_output_shapes
:џџџџџџџџџ
v
$report_uninitialized_resources/ConstConst"/device:CPU:0*
valueB *
dtype0*
_output_shapes
: 
M
concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
М
concatConcatV22report_uninitialized_variables/boolean_mask/Gather$report_uninitialized_resources/Constconcat/axis*
T0*
N*#
_output_shapes
:џџџџџџџџџ*

Tidx0
Ё
6report_uninitialized_variables_1/IsVariableInitializedIsVariableInitializedglobal_step*
_class
loc:@global_step*
dtype0	*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_1IsVariableInitializeddense/kernel*
dtype0*
_output_shapes
: *
_class
loc:@dense/kernel
Ё
8report_uninitialized_variables_1/IsVariableInitialized_2IsVariableInitialized
dense/bias*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Љ
8report_uninitialized_variables_1/IsVariableInitialized_3IsVariableInitializeddense_1/kernel*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_4IsVariableInitializeddense_1/bias*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
Љ
8report_uninitialized_variables_1/IsVariableInitialized_5IsVariableInitializeddense_2/kernel*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_6IsVariableInitializeddense_2/bias*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
Љ
8report_uninitialized_variables_1/IsVariableInitialized_7IsVariableInitializeddense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Ѕ
8report_uninitialized_variables_1/IsVariableInitialized_8IsVariableInitializeddense_3/bias*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
Ђ
8report_uninitialized_variables_1/IsVariableInitialized_9IsVariableInitializedbeta1_power*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ѓ
9report_uninitialized_variables_1/IsVariableInitialized_10IsVariableInitializedbeta2_power*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_11IsVariableInitializeddense/kernel/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense/kernel
­
9report_uninitialized_variables_1/IsVariableInitialized_12IsVariableInitializeddense/kernel/Adam_1*
_class
loc:@dense/kernel*
dtype0*
_output_shapes
: 
Ї
9report_uninitialized_variables_1/IsVariableInitialized_13IsVariableInitializeddense/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense/bias
Љ
9report_uninitialized_variables_1/IsVariableInitialized_14IsVariableInitializeddense/bias/Adam_1*
_class
loc:@dense/bias*
dtype0*
_output_shapes
: 
Џ
9report_uninitialized_variables_1/IsVariableInitialized_15IsVariableInitializeddense_1/kernel/Adam*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Б
9report_uninitialized_variables_1/IsVariableInitialized_16IsVariableInitializeddense_1/kernel/Adam_1*!
_class
loc:@dense_1/kernel*
dtype0*
_output_shapes
: 
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_17IsVariableInitializeddense_1/bias/Adam*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
­
9report_uninitialized_variables_1/IsVariableInitialized_18IsVariableInitializeddense_1/bias/Adam_1*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
: 
Џ
9report_uninitialized_variables_1/IsVariableInitialized_19IsVariableInitializeddense_2/kernel/Adam*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Б
9report_uninitialized_variables_1/IsVariableInitialized_20IsVariableInitializeddense_2/kernel/Adam_1*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_21IsVariableInitializeddense_2/bias/Adam*
dtype0*
_output_shapes
: *
_class
loc:@dense_2/bias
­
9report_uninitialized_variables_1/IsVariableInitialized_22IsVariableInitializeddense_2/bias/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense_2/bias
Џ
9report_uninitialized_variables_1/IsVariableInitialized_23IsVariableInitializeddense_3/kernel/Adam*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Б
9report_uninitialized_variables_1/IsVariableInitialized_24IsVariableInitializeddense_3/kernel/Adam_1*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
Ћ
9report_uninitialized_variables_1/IsVariableInitialized_25IsVariableInitializeddense_3/bias/Adam*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
: 
­
9report_uninitialized_variables_1/IsVariableInitialized_26IsVariableInitializeddense_3/bias/Adam_1*
dtype0*
_output_shapes
: *
_class
loc:@dense_3/bias
Є
&report_uninitialized_variables_1/stackPack6report_uninitialized_variables_1/IsVariableInitialized8report_uninitialized_variables_1/IsVariableInitialized_18report_uninitialized_variables_1/IsVariableInitialized_28report_uninitialized_variables_1/IsVariableInitialized_38report_uninitialized_variables_1/IsVariableInitialized_48report_uninitialized_variables_1/IsVariableInitialized_58report_uninitialized_variables_1/IsVariableInitialized_68report_uninitialized_variables_1/IsVariableInitialized_78report_uninitialized_variables_1/IsVariableInitialized_88report_uninitialized_variables_1/IsVariableInitialized_99report_uninitialized_variables_1/IsVariableInitialized_109report_uninitialized_variables_1/IsVariableInitialized_119report_uninitialized_variables_1/IsVariableInitialized_129report_uninitialized_variables_1/IsVariableInitialized_139report_uninitialized_variables_1/IsVariableInitialized_149report_uninitialized_variables_1/IsVariableInitialized_159report_uninitialized_variables_1/IsVariableInitialized_169report_uninitialized_variables_1/IsVariableInitialized_179report_uninitialized_variables_1/IsVariableInitialized_189report_uninitialized_variables_1/IsVariableInitialized_199report_uninitialized_variables_1/IsVariableInitialized_209report_uninitialized_variables_1/IsVariableInitialized_219report_uninitialized_variables_1/IsVariableInitialized_229report_uninitialized_variables_1/IsVariableInitialized_239report_uninitialized_variables_1/IsVariableInitialized_249report_uninitialized_variables_1/IsVariableInitialized_259report_uninitialized_variables_1/IsVariableInitialized_26"/device:CPU:0*
T0
*

axis *
N*
_output_shapes
:

+report_uninitialized_variables_1/LogicalNot
LogicalNot&report_uninitialized_variables_1/stack"/device:CPU:0*
_output_shapes
:
т
&report_uninitialized_variables_1/ConstConst"/device:CPU:0*ј
valueюBыBglobal_stepBdense/kernelB
dense/biasBdense_1/kernelBdense_1/biasBdense_2/kernelBdense_2/biasBdense_3/kernelBdense_3/biasBbeta1_powerBbeta2_powerBdense/kernel/AdamBdense/kernel/Adam_1Bdense/bias/AdamBdense/bias/Adam_1Bdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_3/kernel/AdamBdense_3/kernel/Adam_1Bdense_3/bias/AdamBdense_3/bias/Adam_1*
dtype0*
_output_shapes
:

3report_uninitialized_variables_1/boolean_mask/ShapeConst"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Areport_uninitialized_variables_1/boolean_mask/strided_slice/stackConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_1Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
ђ
;report_uninitialized_variables_1/boolean_mask/strided_sliceStridedSlice3report_uninitialized_variables_1/boolean_mask/ShapeAreport_uninitialized_variables_1/boolean_mask/strided_slice/stackCreport_uninitialized_variables_1/boolean_mask/strided_slice/stack_1Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_2"/device:CPU:0*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
:*
Index0*
T0*
shrink_axis_mask 

Dreport_uninitialized_variables_1/boolean_mask/Prod/reduction_indicesConst"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

2report_uninitialized_variables_1/boolean_mask/ProdProd;report_uninitialized_variables_1/boolean_mask/strided_sliceDreport_uninitialized_variables_1/boolean_mask/Prod/reduction_indices"/device:CPU:0*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 

5report_uninitialized_variables_1/boolean_mask/Shape_1Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice_1/stackConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB: 

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_1Const"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
њ
=report_uninitialized_variables_1/boolean_mask/strided_slice_1StridedSlice5report_uninitialized_variables_1/boolean_mask/Shape_1Creport_uninitialized_variables_1/boolean_mask/strided_slice_1/stackEreport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_1Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_2"/device:CPU:0*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask 

5report_uninitialized_variables_1/boolean_mask/Shape_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:

Creport_uninitialized_variables_1/boolean_mask/strided_slice_2/stackConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueB:

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_1Const"/device:CPU:0*
valueB: *
dtype0*
_output_shapes
:

Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_2Const"/device:CPU:0*
valueB:*
dtype0*
_output_shapes
:
њ
=report_uninitialized_variables_1/boolean_mask/strided_slice_2StridedSlice5report_uninitialized_variables_1/boolean_mask/Shape_2Creport_uninitialized_variables_1/boolean_mask/strided_slice_2/stackEreport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_1Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_2"/device:CPU:0*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
_output_shapes
: *
Index0*
T0*
shrink_axis_mask 
Т
=report_uninitialized_variables_1/boolean_mask/concat/values_1Pack2report_uninitialized_variables_1/boolean_mask/Prod"/device:CPU:0*
T0*

axis *
N*
_output_shapes
:

9report_uninitialized_variables_1/boolean_mask/concat/axisConst"/device:CPU:0*
value	B : *
dtype0*
_output_shapes
: 

4report_uninitialized_variables_1/boolean_mask/concatConcatV2=report_uninitialized_variables_1/boolean_mask/strided_slice_1=report_uninitialized_variables_1/boolean_mask/concat/values_1=report_uninitialized_variables_1/boolean_mask/strided_slice_29report_uninitialized_variables_1/boolean_mask/concat/axis"/device:CPU:0*
N*
_output_shapes
:*

Tidx0*
T0
р
5report_uninitialized_variables_1/boolean_mask/ReshapeReshape&report_uninitialized_variables_1/Const4report_uninitialized_variables_1/boolean_mask/concat"/device:CPU:0*
T0*
Tshape0*
_output_shapes
:

=report_uninitialized_variables_1/boolean_mask/Reshape_1/shapeConst"/device:CPU:0*
valueB:
џџџџџџџџџ*
dtype0*
_output_shapes
:
№
7report_uninitialized_variables_1/boolean_mask/Reshape_1Reshape+report_uninitialized_variables_1/LogicalNot=report_uninitialized_variables_1/boolean_mask/Reshape_1/shape"/device:CPU:0*
T0
*
Tshape0*
_output_shapes
:
Ж
3report_uninitialized_variables_1/boolean_mask/WhereWhere7report_uninitialized_variables_1/boolean_mask/Reshape_1"/device:CPU:0*'
_output_shapes
:џџџџџџџџџ*
T0

Щ
5report_uninitialized_variables_1/boolean_mask/SqueezeSqueeze3report_uninitialized_variables_1/boolean_mask/Where"/device:CPU:0*#
_output_shapes
:џџџџџџџџџ*
squeeze_dims
*
T0	

4report_uninitialized_variables_1/boolean_mask/GatherGather5report_uninitialized_variables_1/boolean_mask/Reshape5report_uninitialized_variables_1/boolean_mask/Squeeze"/device:CPU:0*
Tindices0	*
Tparams0*
validate_indices(*#
_output_shapes
:џџџџџџџџџ
у
init_2NoOp^accuracy/total/Assign^accuracy/count/Assign%^root_mean_squared_error/total/Assign%^root_mean_squared_error/count/Assign#^recall/true_positives/count/Assign$^recall/false_negatives/count/Assign&^precision/true_positives/count/Assign'^precision/false_positives/count/Assign^mean/total/Assign^mean/count/Assign^eval_step/Assign

init_all_tablesNoOp

init_3NoOp
8
group_deps_2NoOp^init_2^init_all_tables^init_3

Merge/MergeSummaryMergeSummaryHenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full*
N*
_output_shapes
: 
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 

save/StringJoin/inputs_1Const*<
value3B1 B+_temp_b0772121846c4dffa1ef6f93bcd62529/part*
dtype0*
_output_shapes
: 
u
save/StringJoin
StringJoin
save/Constsave/StringJoin/inputs_1*
N*
_output_shapes
: *
	separator 
Q
save/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
k
save/ShardedFilename/shardConst"/device:CPU:0*
dtype0*
_output_shapes
: *
value	B : 

save/ShardedFilenameShardedFilenamesave/StringJoinsave/ShardedFilename/shardsave/num_shards"/device:CPU:0*
_output_shapes
: 
д
save/SaveV2/tensor_namesConst"/device:CPU:0*ј
valueюBыBbeta1_powerBbeta2_powerB
dense/biasBdense/bias/AdamBdense/bias/Adam_1Bdense/kernelBdense/kernel/AdamBdense/kernel/Adam_1Bdense_1/biasBdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_1/kernelBdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_2/biasBdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_2/kernelBdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_3/biasBdense_3/bias/AdamBdense_3/bias/Adam_1Bdense_3/kernelBdense_3/kernel/AdamBdense_3/kernel/Adam_1Bglobal_step*
dtype0*
_output_shapes
:
Ј
save/SaveV2/shape_and_slicesConst"/device:CPU:0*
dtype0*
_output_shapes
:*I
value@B>B B B B B B B B B B B B B B B B B B B B B B B B B B B 

save/SaveV2SaveV2save/ShardedFilenamesave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesbeta1_powerbeta2_power
dense/biasdense/bias/Adamdense/bias/Adam_1dense/kerneldense/kernel/Adamdense/kernel/Adam_1dense_1/biasdense_1/bias/Adamdense_1/bias/Adam_1dense_1/kerneldense_1/kernel/Adamdense_1/kernel/Adam_1dense_2/biasdense_2/bias/Adamdense_2/bias/Adam_1dense_2/kerneldense_2/kernel/Adamdense_2/kernel/Adam_1dense_3/biasdense_3/bias/Adamdense_3/bias/Adam_1dense_3/kerneldense_3/kernel/Adamdense_3/kernel/Adam_1global_step"/device:CPU:0*)
dtypes
2	
 
save/control_dependencyIdentitysave/ShardedFilename^save/SaveV2"/device:CPU:0*
T0*'
_class
loc:@save/ShardedFilename*
_output_shapes
: 
Ќ
+save/MergeV2Checkpoints/checkpoint_prefixesPacksave/ShardedFilename^save/control_dependency"/device:CPU:0*
N*
_output_shapes
:*
T0*

axis 

save/MergeV2CheckpointsMergeV2Checkpoints+save/MergeV2Checkpoints/checkpoint_prefixes
save/Const"/device:CPU:0*
delete_old_dirs(

save/IdentityIdentity
save/Const^save/control_dependency^save/MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 
з
save/RestoreV2/tensor_namesConst"/device:CPU:0*ј
valueюBыBbeta1_powerBbeta2_powerB
dense/biasBdense/bias/AdamBdense/bias/Adam_1Bdense/kernelBdense/kernel/AdamBdense/kernel/Adam_1Bdense_1/biasBdense_1/bias/AdamBdense_1/bias/Adam_1Bdense_1/kernelBdense_1/kernel/AdamBdense_1/kernel/Adam_1Bdense_2/biasBdense_2/bias/AdamBdense_2/bias/Adam_1Bdense_2/kernelBdense_2/kernel/AdamBdense_2/kernel/Adam_1Bdense_3/biasBdense_3/bias/AdamBdense_3/bias/Adam_1Bdense_3/kernelBdense_3/kernel/AdamBdense_3/kernel/Adam_1Bglobal_step*
dtype0*
_output_shapes
:
Ћ
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
dtype0*
_output_shapes
:*I
value@B>B B B B B B B B B B B B B B B B B B B B B B B B B B B 
Ђ
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*)
dtypes
2	*
_output_shapesn
l:::::::::::::::::::::::::::

save/AssignAssignbeta1_powersave/RestoreV2*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
: 

save/Assign_1Assignbeta2_powersave/RestoreV2:1*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@dense/bias
Ђ
save/Assign_2Assign
dense/biassave/RestoreV2:2*
use_locking(*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:

Ї
save/Assign_3Assigndense/bias/Adamsave/RestoreV2:3*
T0*
_class
loc:@dense/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
Љ
save/Assign_4Assigndense/bias/Adam_1save/RestoreV2:4*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class
loc:@dense/bias
Њ
save/Assign_5Assigndense/kernelsave/RestoreV2:5*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*
_class
loc:@dense/kernel
Џ
save/Assign_6Assigndense/kernel/Adamsave/RestoreV2:6*
use_locking(*
T0*
_class
loc:@dense/kernel*
validate_shape(*
_output_shapes

:

Б
save/Assign_7Assigndense/kernel/Adam_1save/RestoreV2:7*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*
_class
loc:@dense/kernel
І
save/Assign_8Assigndense_1/biassave/RestoreV2:8*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@dense_1/bias
Ћ
save/Assign_9Assigndense_1/bias/Adamsave/RestoreV2:9*
use_locking(*
T0*
_class
loc:@dense_1/bias*
validate_shape(*
_output_shapes
:
Џ
save/Assign_10Assigndense_1/bias/Adam_1save/RestoreV2:10*
use_locking(*
T0*
_class
loc:@dense_1/bias*
validate_shape(*
_output_shapes
:
А
save/Assign_11Assigndense_1/kernelsave/RestoreV2:11*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*!
_class
loc:@dense_1/kernel
Е
save/Assign_12Assigndense_1/kernel/Adamsave/RestoreV2:12*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:

З
save/Assign_13Assigndense_1/kernel/Adam_1save/RestoreV2:13*
use_locking(*
T0*!
_class
loc:@dense_1/kernel*
validate_shape(*
_output_shapes

:

Ј
save/Assign_14Assigndense_2/biassave/RestoreV2:14*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class
loc:@dense_2/bias
­
save/Assign_15Assigndense_2/bias/Adamsave/RestoreV2:15*
use_locking(*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:

Џ
save/Assign_16Assigndense_2/bias/Adam_1save/RestoreV2:16*
T0*
_class
loc:@dense_2/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
А
save/Assign_17Assigndense_2/kernelsave/RestoreV2:17*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:

Е
save/Assign_18Assigndense_2/kernel/Adamsave/RestoreV2:18*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:

З
save/Assign_19Assigndense_2/kernel/Adam_1save/RestoreV2:19*
use_locking(*
T0*!
_class
loc:@dense_2/kernel*
validate_shape(*
_output_shapes

:

Ј
save/Assign_20Assigndense_3/biassave/RestoreV2:20*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:*
use_locking(
­
save/Assign_21Assigndense_3/bias/Adamsave/RestoreV2:21*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:*
use_locking(
Џ
save/Assign_22Assigndense_3/bias/Adam_1save/RestoreV2:22*
use_locking(*
T0*
_class
loc:@dense_3/bias*
validate_shape(*
_output_shapes
:
А
save/Assign_23Assigndense_3/kernelsave/RestoreV2:23*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0*!
_class
loc:@dense_3/kernel
Е
save/Assign_24Assigndense_3/kernel/Adamsave/RestoreV2:24*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:

З
save/Assign_25Assigndense_3/kernel/Adam_1save/RestoreV2:25*
use_locking(*
T0*!
_class
loc:@dense_3/kernel*
validate_shape(*
_output_shapes

:

Ђ
save/Assign_26Assignglobal_stepsave/RestoreV2:26*
use_locking(*
T0	*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
й
save/restore_shardNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8^save/Assign_9^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26
-
save/restore_allNoOp^save/restore_shard""J
savers@>
<
save/Const:0save/Identity:0save/restore_all (5 @F8"
queue_runners

enqueue_input/fifo_queue$enqueue_input/fifo_queue_EnqueueManyenqueue_input/fifo_queue_Close" enqueue_input/fifo_queue_Close_1*"0
losses&
$
"sigmoid_cross_entropy_loss/value:0" 
global_step

global_step:0"
train_op

Adam"&

summary_op

Merge/MergeSummary:0"П
trainable_variablesЇЄ
e
dense/kernel:0dense/kernel/Assigndense/kernel/read:02)dense/kernel/Initializer/random_uniform:0
T
dense/bias:0dense/bias/Assigndense/bias/read:02dense/bias/Initializer/zeros:0
m
dense_1/kernel:0dense_1/kernel/Assigndense_1/kernel/read:02+dense_1/kernel/Initializer/random_uniform:0
\
dense_1/bias:0dense_1/bias/Assigndense_1/bias/read:02 dense_1/bias/Initializer/zeros:0
m
dense_2/kernel:0dense_2/kernel/Assigndense_2/kernel/read:02+dense_2/kernel/Initializer/random_uniform:0
\
dense_2/bias:0dense_2/bias/Assigndense_2/bias/read:02 dense_2/bias/Initializer/zeros:0
m
dense_3/kernel:0dense_3/kernel/Assigndense_3/kernel/read:02+dense_3/kernel/Initializer/random_uniform:0
\
dense_3/bias:0dense_3/bias/Assigndense_3/bias/read:02 dense_3/bias/Initializer/zeros:0"[
	summariesN
L
Jenqueue_input/queue/enqueue_input/fifo_queuefraction_over_0_of_1000_full:0"U
ready_for_local_init_op:
8
6report_uninitialized_variables_1/boolean_mask/Gather:0"
init_op

group_deps_1"
	eval_step

eval_step:0"
metric_variables

accuracy/total:0
accuracy/count:0
root_mean_squared_error/total:0
root_mean_squared_error/count:0
recall/true_positives/count:0
recall/false_negatives/count:0
 precision/true_positives/count:0
!precision/false_positives/count:0
mean/total:0
mean/count:0"Г
local_variables
d
accuracy/total:0accuracy/total/Assignaccuracy/total/read:02"accuracy/total/Initializer/zeros:0
d
accuracy/count:0accuracy/count/Assignaccuracy/count/read:02"accuracy/count/Initializer/zeros:0
 
root_mean_squared_error/total:0$root_mean_squared_error/total/Assign$root_mean_squared_error/total/read:021root_mean_squared_error/total/Initializer/zeros:0
 
root_mean_squared_error/count:0$root_mean_squared_error/count/Assign$root_mean_squared_error/count/read:021root_mean_squared_error/count/Initializer/zeros:0

recall/true_positives/count:0"recall/true_positives/count/Assign"recall/true_positives/count/read:02/recall/true_positives/count/Initializer/zeros:0

recall/false_negatives/count:0#recall/false_negatives/count/Assign#recall/false_negatives/count/read:020recall/false_negatives/count/Initializer/zeros:0
Є
 precision/true_positives/count:0%precision/true_positives/count/Assign%precision/true_positives/count/read:022precision/true_positives/count/Initializer/zeros:0
Ј
!precision/false_positives/count:0&precision/false_positives/count/Assign&precision/false_positives/count/read:023precision/false_positives/count/Initializer/zeros:0
T
mean/total:0mean/total/Assignmean/total/read:02mean/total/Initializer/zeros:0
T
mean/count:0mean/count/Assignmean/count/read:02mean/count/Initializer/zeros:0
P
eval_step:0eval_step/Assigneval_step/read:02eval_step/Initializer/zeros:0"!
local_init_op

group_deps_2"О
	variablesА­
X
global_step:0global_step/Assignglobal_step/read:02global_step/Initializer/zeros:0
e
dense/kernel:0dense/kernel/Assigndense/kernel/read:02)dense/kernel/Initializer/random_uniform:0
T
dense/bias:0dense/bias/Assigndense/bias/read:02dense/bias/Initializer/zeros:0
m
dense_1/kernel:0dense_1/kernel/Assigndense_1/kernel/read:02+dense_1/kernel/Initializer/random_uniform:0
\
dense_1/bias:0dense_1/bias/Assigndense_1/bias/read:02 dense_1/bias/Initializer/zeros:0
m
dense_2/kernel:0dense_2/kernel/Assigndense_2/kernel/read:02+dense_2/kernel/Initializer/random_uniform:0
\
dense_2/bias:0dense_2/bias/Assigndense_2/bias/read:02 dense_2/bias/Initializer/zeros:0
m
dense_3/kernel:0dense_3/kernel/Assigndense_3/kernel/read:02+dense_3/kernel/Initializer/random_uniform:0
\
dense_3/bias:0dense_3/bias/Assigndense_3/bias/read:02 dense_3/bias/Initializer/zeros:0
T
beta1_power:0beta1_power/Assignbeta1_power/read:02beta1_power/initial_value:0
T
beta2_power:0beta2_power/Assignbeta2_power/read:02beta2_power/initial_value:0
p
dense/kernel/Adam:0dense/kernel/Adam/Assigndense/kernel/Adam/read:02%dense/kernel/Adam/Initializer/zeros:0
x
dense/kernel/Adam_1:0dense/kernel/Adam_1/Assigndense/kernel/Adam_1/read:02'dense/kernel/Adam_1/Initializer/zeros:0
h
dense/bias/Adam:0dense/bias/Adam/Assigndense/bias/Adam/read:02#dense/bias/Adam/Initializer/zeros:0
p
dense/bias/Adam_1:0dense/bias/Adam_1/Assigndense/bias/Adam_1/read:02%dense/bias/Adam_1/Initializer/zeros:0
x
dense_1/kernel/Adam:0dense_1/kernel/Adam/Assigndense_1/kernel/Adam/read:02'dense_1/kernel/Adam/Initializer/zeros:0

dense_1/kernel/Adam_1:0dense_1/kernel/Adam_1/Assigndense_1/kernel/Adam_1/read:02)dense_1/kernel/Adam_1/Initializer/zeros:0
p
dense_1/bias/Adam:0dense_1/bias/Adam/Assigndense_1/bias/Adam/read:02%dense_1/bias/Adam/Initializer/zeros:0
x
dense_1/bias/Adam_1:0dense_1/bias/Adam_1/Assigndense_1/bias/Adam_1/read:02'dense_1/bias/Adam_1/Initializer/zeros:0
x
dense_2/kernel/Adam:0dense_2/kernel/Adam/Assigndense_2/kernel/Adam/read:02'dense_2/kernel/Adam/Initializer/zeros:0

dense_2/kernel/Adam_1:0dense_2/kernel/Adam_1/Assigndense_2/kernel/Adam_1/read:02)dense_2/kernel/Adam_1/Initializer/zeros:0
p
dense_2/bias/Adam:0dense_2/bias/Adam/Assigndense_2/bias/Adam/read:02%dense_2/bias/Adam/Initializer/zeros:0
x
dense_2/bias/Adam_1:0dense_2/bias/Adam_1/Assigndense_2/bias/Adam_1/read:02'dense_2/bias/Adam_1/Initializer/zeros:0
x
dense_3/kernel/Adam:0dense_3/kernel/Adam/Assigndense_3/kernel/Adam/read:02'dense_3/kernel/Adam/Initializer/zeros:0

dense_3/kernel/Adam_1:0dense_3/kernel/Adam_1/Assigndense_3/kernel/Adam_1/read:02)dense_3/kernel/Adam_1/Initializer/zeros:0
p
dense_3/bias/Adam:0dense_3/bias/Adam/Assigndense_3/bias/Adam/read:02%dense_3/bias/Adam/Initializer/zeros:0
x
dense_3/bias/Adam_1:0dense_3/bias/Adam_1/Assigndense_3/bias/Adam_1/read:02'dense_3/bias/Adam_1/Initializer/zeros:0"
ready_op


concat:0ћ>№^       иАnМ	Ki2ДжA*Q

accuracyC2?

lossя*?

	precision  ?

rsmeЙ:?

sensitivityЭЬЬ>ЊЩЧ
^       иАnМ	DHП2ДжA*Q

accuracyё№p?

lossЪ>

	precision  ?

rsmeB[x>

sensitivityтсa?їb§^       иАnМ	чu2ДжA!*Q

accuracy--m?

lossO>

	precision  ?

rsmeуе>

sensitivityZZZ?ЉJЄ